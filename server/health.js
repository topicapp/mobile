const http = require("http");

const config = {
  host: "localhost",
  port: 80,
  path: "/health",
  method: "GET",
  timeout: 5000
};

const request = http.request(config, response => {
  console.log(`STATUS: ${response.statusCode}`);
  if (response.statusCode === 200) {
    process.exit(0);
  } else {
    process.exit(1);
  }
});

request.on("error", function (error) {
  console.error("ERROR", error);
  process.exit(1);
});

request.end();
