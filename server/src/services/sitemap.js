const axiosApi = require("./axiosInstance");
const urls = require('../assets/urls.json');

const baseUrl = 'https://api.topicapp.fr/v1';
const siteUrl = 'https://www.topicapp.fr'

const generateSitemap = async () => {
  let sitemap = "";
  sitemap += '<?xml version="1.0" encoding="UTF-8"?>' +
    '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

  // 1. Urls hardcodées
  urls.main.forEach(url => {
    sitemap +=
    '<url>' +
      `<loc>${siteUrl}${url.url}</loc>` +
      `<changefreq>${url.changefreq}</changefreq>` +
      `<priority>${url.priority}</priority>` +
    '</url>';
  })

  // 2. Articles
  const { data: { info: { articles }}} = await axiosApi.get("/articles/list", {
    params: {
      number: 10000
    }
  });
  articles.forEach((article) => {
    sitemap +=
    '<url>' +
      `<loc>${siteUrl}/articles/${article._id}</loc>` +
      '<changefreq>monthly</changefreq>' +
      '<priority>0.7</priority>' +
      /* '<news:news>' +
        '<news:publication>' +
          `<news:name>The Example Times</news:name>` +
          '<news:language>fr</news:language>' +
        '</news:publication>' +
        `<news:publication_date>2008-12-23</news:publication_date>` +
        `<news:title>Companies A, B in Merger Talks</news:title>` +
      '</news:news>' + */
    '</url>';
  });

  // 3. Évènements
  const { data: { info: { events }}} = await axiosApi.get("/events/list", {
    params: {
      number: 10000
    }
  });
  events.forEach((event) => {
    sitemap +=
    '<url>' +
      `<loc>${siteUrl}/evenements/${event._id}</loc>` +
      '<changefreq>monthly</changefreq>' +
      '<priority>0.7</priority>' +
    '</url>';
  });

  // 3. Groupes
  const { data: { info: { groups }}}  = await axiosApi.get("/groups/list", {
    params: {
      number: 5000
    }
  });
  groups.forEach((group) => {
    sitemap +=
    '<url>' +
      `<loc>${siteUrl}/groupes/${group._id}</loc>` +
      '<changefreq>weekly</changefreq>' +
      '<priority>1.0</priority>' +
    '</url>';
  });

  // 4. Utilisateurs
  const { data: { info: { users }}}  = await axiosApi.get("/users/list", {
    params: {
      number: 10000,
      public: true,
      active: true
    }
  });
  users.forEach((user) => {
    sitemap +=
      '<url>' +
        `<loc>${siteUrl}/utilisateurs/${user._id}</loc>` +
        '<changefreq>monthly</changefreq>' +
        '<priority>0.3</priority>' +
      '</url>';
  });

  // 5. Schools
  const { data: { info: { schools }}}  = await axiosApi.get("/schools/list", {
    params: {
      number: 10000,
      active: true
    }
  });
  schools.forEach((school) => {
    sitemap +=
      '<url>' +
        `<loc>${siteUrl}/ecoles/${school._id}</loc>` +
        '<changefreq>weekly</changefreq>' +
        '<priority>0.3</priority>' +
      '</url>';
  });


  // 6. Departements
  const { data: { info: { departments }}}  = await axiosApi.get("/departments/list", {
    params: {
      number: 500
    }
  });
  departments.forEach((dep) => {
    sitemap +=
      '<url>' +
        `<loc>${siteUrl}/departements/${dep._id}</loc>` +
        '<changefreq>weekly</changefreq>' +
        '<priority>0.3</priority>' +
      '</url>';
  });

  sitemap += '</urlset>';

  console.log(sitemap)

  return sitemap;
}

module.exports = generateSitemap;
