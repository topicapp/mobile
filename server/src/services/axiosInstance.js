const axios = require("axios").default;

const customUserAgent = "TopicRedirection";
const baseURL = process.env.API_URL || "https://api.topicapp.fr/v1/";

const instance = axios.create({
  baseURL,
  headers: {
    "User-Agent": customUserAgent
  }
});

module.exports = instance;
