const axiosApi = require("./axiosInstance");

const getArticleInfos = async articleId => {
  const { data } = await axiosApi.get("/articles/info", {
    params: { articleId }
  });
  const article = data.info.articles[0];
  const imageUrl = article.image.image
    ? `https://cdn.topicapp.fr/file/get/${article.image.image}`
    : null;
  const infos = {
    type: "article",
    description: article.summary,
    author: article.authors[0].displayName,
    keywords: article.tags.map(tag => tag.displayName).join(", "),
    title: `${article.title} par ${article.group.displayName}`,
    imageUrl,
    createdAt: article.date
  };
  return infos;
};

const getEventInfos = async eventId => {
  const { data } = await axiosApi.get("/events/info", {
    params: { eventId }
  });
  const event = data.info.events[0];
  const imageUrl = event.image.image
    ? `https://cdn.topicapp.fr/file/get/${event.image.image}`
    : null;
  const infos = {
    type: "article",
    description: event.summary,
    author: event.authors[0].displayName,
    keywords: event.tags.map(tag => tag.displayName).join(", "),
    title: `${event.title} par ${event.group.displayName}`,
    imageUrl,
    createdAt: event.duration.start
  };
  return infos;
};

const getGroupInfos = async groupId => {
  const { data } = await axiosApi.get("/groups/info", {
    params: { groupId }
  });
  const group = data.info.groups[0];
  const imageUrl =
    group.avatar.image && group.avatar.image.image
      ? `https://cdn.topicapp.fr/file/get/${group.avatar.image.image}`
      : null;
  const infos = {
    type: "profile",
    description: group.summary,
    title: group.name,
    imageUrl
  };
  return infos;
};

const getUserInfos = async userId => {
  const { data } = await axiosApi.get("/users/info", {
    params: { userId }
  });
  const user = data.info.users[0];
  const infos = {
    type: "profile",
    title: user.info.username,
    first: (user.data && user.data.firstName) || null,
    last: (user.data && user.data.lastName) || null
  };
  return infos;
};

const getSchoolInfos = async schoolId => {
  const { data } = await axiosApi.get("/schools/info", {
    params: { schoolId }
  });
  const school = data.info.schools[0];
  const imageUrl =
    school.image && school.image.image
      ? `https://cdn.topicapp.fr/file/get/${school.image.image}`
      : null;
  const infos = {
    type: "profile",
    title: school.name,
    imageUrl
  };
  return infos;
};

const getDepartmentInfos = async departmentId => {
  const { data } = await axiosApi.get("/departments/info", {
    params: { departmentId }
  });
  const department = data.info.departments[0];
  const infos = {
    type: "profile",
    title: department.name
  };
  return infos;
};

module.exports = {
  getArticleInfos,
  getEventInfos,
  getGroupInfos,
  getUserInfos,
  getSchoolInfos,
  getDepartmentInfos
};
