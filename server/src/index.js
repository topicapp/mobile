const express = require('express');
const path = require('path');
const app = express();
const port = process.env.PORT || 80;
const logger = require('./logger');
const generateSitemap = require('./services/sitemap');
const env = process.env.NODE_ENV;

const {
  getArticleInfos,
  getEventInfos,
  getGroupInfos,
  getUserInfos,
  getSchoolInfos,
  getDepartmentInfos,
} = require('./services/getInfos');

app.set('trust proxy', true);
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

app.use(express.static(path.join(__dirname, '../../web-build/')));

app.get('/health', (req, res) => {
  res.status(200).end();
});

app.get('/.well-known/apple-app-site-association', (req, res) => {
  res.type('json').sendFile(path.join(__dirname, './assets/apple-app-site-association.json'));
});

app.get('/robots.txt', (req, res) => {
  if (env === 'production') {
    res.type('txt').sendFile(path.join(__dirname, './assets/robots.txt'));
  } else {
    res.type('txt').sendFile(path.join(__dirname, './assets/robots-dev.txt'));
  }
});

app.get('/sitemap.xml', (req, res) => {
  if (env === 'production') {
    generateSitemap().then(sitemap => res.type('xml').send(sitemap));
  } else {
    res.status(404).send('Not found (sitemap disabled on dev)')
  }
})

app.get('/articles/:id', (req, res) => {
  const { id } = req.params;
  getArticleInfos(id)
    .then((infos) => {
      res.status(200).render('index', {
        headerFile: './headers/content',
        item: infos,
      });
    })
    .catch(() => {
      res.status(200).render('index', {
        headerFile: './headers/other',
      });
    });
});

app.get('/evenements/:id', (req, res) => {
  const { id } = req.params;
  getEventInfos(id)
    .then((infos) => {
      res.status(200).render('index', {
        headerFile: './headers/content',
        item: infos,
      });
    })
    .catch(() => {
      res.status(200).render('index', {
        headerFile: './headers/other',
      });
    });
});

app.get('/groupes/:id', (req, res) => {
  const { id } = req.params;
  getGroupInfos(id)
    .then((infos) => {
      res.status(200).render('index', {
        headerFile: './headers/content',
        item: infos,
      });
    })
    .catch(() => {
      res.status(200).render('index', {
        headerFile: './headers/other',
      });
    });
});

app.get('/utilisateurs/:id', (req, res) => {
  const { id } = req.params;
  getUserInfos(id)
    .then((infos) => {
      res.status(200).render('index', {
        headerFile: './headers/content',
        item: infos,
      });
    })
    .catch(() => {
      res.status(200).render('index', {
        headerFile: './headers/other',
      });
    });
});

app.get('/ecoles/:id', (req, res) => {
  const { id } = req.params;
  getSchoolInfos(id)
    .then((infos) => {
      res.status(200).render('index', {
        headerFile: './headers/content',
        item: infos,
      });
    })
    .catch(() => {
      res.status(200).render('index', {
        headerFile: './headers/other',
      });
    });
});

app.get('/departements/:id', (req, res) => {
  const { id } = req.params;
  getDepartmentInfos(id)
    .then((infos) => {
      res.status(200).render('index', {
        headerFile: './headers/content',
        item: infos,
      });
    })
    .catch(() => {
      res.status(200).render('index', {
        headerFile: './headers/other',
      });
    });
});

app.get('/static/*', (req, res) => {
  res.status(404).send('Not Found');
});
app.get('/.well-known/*', (req, res) => {
  res.status(404).send('Not Found');
});
app.get('/fonts/*', (req, res) => {
  res.status(404).send('Not Found');
});

app.get('*', (req, res) => {
  res.status(200).render('index', {
    headerFile: './headers/other',
  });
});

app.listen(port, () => logger.info(`Listening on port ${port}`));
