const { createLogger, format, transports } = require("winston");
const { combine, timestamp, printf } = format;

const myFormat = printf(({ level, message, timestamp }) => {
  return `${timestamp} [${level}] ${message}`;
});

const getLevel = () => {
  switch (process.env.NODE_ENV) {
    case "production":
      return "info";
    default:
      return "debug";
  }
};

const logger = createLogger({
  level: getLevel(),
  format: combine(timestamp({ format: "YYYY-MM-DD HH:mm:ss" }), myFormat),
  transports: [new transports.Console()]
});

if (process.env.NODE_ENV === "test") {
  logger.silent = true;
}

module.exports = logger;
