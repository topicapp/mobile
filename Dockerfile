# BUILD
FROM node:16-alpine

WORKDIR /data

COPY package*.json ./
COPY patches patches

RUN apk update && apk add git
RUN echo "unsafe-perm" > .npmrc
RUN npm install && npm install --save react-native-web react-native-linear-gradient expo @expo/vector-icons
# TEMP : Need to get babel for web working properly at some point

COPY . .

RUN npm cache clean --force && npm install -g expo-cli
RUN cp index.web.js index.js
RUN expo build:web

RUN cp web-build/index.html server/src/views/index.ejs
RUN rm web-build/index.html
RUN sed -i 's|<meta name="header-inject"/>|<%- include(headerFile) %>|' server/src/views/index.ejs

# SERVER
WORKDIR /data/server

ENV NODE_ENV=production
ENV NODE_PORT=80

# RUN npm install
# If you are building your code for production
RUN npm ci --only=production

# RUN
FROM node:16-alpine
WORKDIR /data/server
COPY --from=0 /data/server /data/server
COPY --from=0 /data/web-build /data/web-build

EXPOSE 80

HEALTHCHECK --interval=10s --timeout=5s --start-period=15s \
  CMD [ "node", "health.js" ]

CMD [ "npm", "start" ]
