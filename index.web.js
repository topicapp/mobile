/* eslint-disable */
// TEMP: We currently use expo on web, but not on mobile, so you have to install expo manually to be able to start the web dev server (npm install --save expo)
// Also, do not commit the package.json, adding expo breaks the android and ios build...
// (yes, it's a terrible hack)
import { registerRootComponent } from 'expo';
import { Platform, AppRegistry } from 'react-native';

import App from './App';

// registerRootComponent calls AppRegistry.registerComponent('main', () => App);
// It also ensures that whether you load the app in the Expo client or in a native build,
// the environment is set up appropriately
// AppRegistry.registerComponent('main', () => App);
registerRootComponent(App);
