import React from 'react';
import { ImageStyle, Linking, StyleProp, useWindowDimensions, View } from 'react-native';
import { Button, Divider, IconButton, Text, useTheme } from 'react-native-paper';
import { connect } from 'react-redux';

import getStyles from '@styles/global';
import { PreferencesState, State, Account, Image } from '@ts/types';
import { getImageUrl } from '@utils';

import AutoHeightImage from './AutoHeightImage';
import { PlatformTouchable } from './PlatformComponents';

type ImageSize = 'small' | 'medium' | 'large' | 'extralarge' | 'full';
type Props = {
  navigation: any; // TODO: Find a better type
  image: Image;
  width?: number;
  maxHeight?: number;
  minHeight?: number;
  size?: ImageSize;
  imageStyle?: StyleProp<ImageStyle>;
};

const ContentImage: React.FC<Props> = ({
  navigation,
  image,
  width,
  maxHeight = 400,
  minHeight = 150,
  size = 'full',
  imageStyle,
}) => {
  const theme = useTheme();
  const styles = getStyles(theme);
  const dimensions = useWindowDimensions();

  return (
    <View>
      <PlatformTouchable
        onPress={() =>
          navigation.push('Root', {
            screen: 'Main',
            params: {
              screen: 'Display',
              params: {
                screen: 'Image',
                params: {
                  screen: 'Display',
                  params: { image: image.image },
                },
              },
            },
          })
        }
      >
        <AutoHeightImage
          source={{ uri: getImageUrl({ image, size }) || '' }}
          width={width || dimensions.width}
          maxHeight={maxHeight}
          style={[styles.image, { minHeight }, imageStyle]}
        />
      </PlatformTouchable>
    </View>
  );
};

const mapStateToProps = (state: State) => {
  const { preferences, account } = state;
  return { preferences, account };
};

export default connect(mapStateToProps)(ContentImage);
