import { Formik } from 'formik';
import React, { createRef } from 'react';
import {
  Linking,
  Platform,
  View,
  ActivityIndicator,
  TextInput as RNTextInput,
  Image,
} from 'react-native';
import DeviceInfo from 'react-native-device-info';
import { Button, Divider, List, RadioButton, Text, useTheme } from 'react-native-paper';
import WebView from 'react-native-webview';
import { connect } from 'react-redux';
import * as Yup from 'yup';

import {
  ErrorMessage,
  FormTextInput,
  Illustration,
  SettingRadio,
  SettingSection,
  SettingToggle,
  SettingTooltip,
} from '@components';
import { Config } from '@constants';
import { sendFeedback } from '@redux/actions/api/feedback';
import { fetchEmail } from '@redux/actions/data/account';
import { LinkingRequestState, State } from '@ts/types';
import { Errors, Alert, trackEvent, handleUrl } from '@utils';

import Modal from './Modal';
import getStyles from './styles/MainFeedbackStylesheet';

type Props = {
  visible: boolean;
  loggedIn: boolean;
  type: string;
  accountId?: string | null;
  accountEmail?: string | null;
  state: LinkingRequestState;
  setVisible: (val: boolean) => void;
};

const HelpModal: React.FC<Props> = ({
  visible,
  type,
  setVisible,
  loggedIn,
  accountId,
  accountEmail,
  state,
}) => {
  const theme = useTheme();
  const { colors } = theme;
  const styles = getStyles(theme);
  const messageInput = createRef<RNTextInput>();
  const emailInput = createRef<RNTextInput>();

  const [submitted, setSubmitted] = React.useState(false);

  if (!visible && submitted) {
    setSubmitted(false);
  }

  const FeedbackSchema = Yup.object().shape({
    message: Yup.string().required('Vous devez spécifier un message'),
    email: loggedIn ? Yup.string() : Yup.string().matches(
      /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.[a-zA-Z]{2,13})+$/,
      'Email invalide',
    ).required(),
  });

  React.useEffect(() => {
    fetchEmail();
  }, [null]);

  return (
    <Modal visible={visible} setVisible={setVisible}>
      {submitted ? (
        <View>
          <View style={[styles.centerIllustrationContainer, { marginTop: 40 }]}>
            <Illustration name="auth-register-success" height={100}/>
            <Text style={[styles.title, { fontSize: 24 }]}>Demande envoyée</Text>
            <Text style={styles.subtitle}>Vous recevrez une réponse par email d&apos;ici peu</Text>
          </View>
          <View style={[styles.container, { marginTop: 80 }]}>
            <Button
              mode="outlined"
              uppercase={Platform.OS !== 'ios'}
              onPress={() => {
                setSubmitted(false);
                setVisible(false);
              }}
              style={{ flex: 1 }}
            >
              Fermer
            </Button>
          </View>
        </View>
      ) : (
        <Formik
          initialValues={{ message: '', email: '' }}
          validationSchema={FeedbackSchema}
          onSubmit={({ message, email }) => {
            const brand = DeviceInfo.getBrand();
            const deviceId = DeviceInfo.getDeviceId();
            const product = DeviceInfo.getProductSync();
            const systemVersion = DeviceInfo.getSystemVersion();
            const topicVersion = DeviceInfo.getReadableVersion();
            const apiLevel = Platform.OS === 'android' ? DeviceInfo.getApiLevelSync() : 'unk';
            const userAgent = DeviceInfo.getUserAgentSync();
            sendFeedback({
              type: `Aide ${type}`,
              message,
              user: {
                id: accountId,
                email: loggedIn ? accountEmail : email,
                loggedIn,
              },
              metadata: {
                device: `${brand}-${product}-${deviceId}`,
                app: `${topicVersion}`,
                os: `${Platform.OS}-${systemVersion}-api${apiLevel}`,
                extra: `UA: ${userAgent}`,
              },
            })
              .then(() => setSubmitted(true))
              .catch((error) => {
                Errors.showPopup({
                  type: 'axios',
                  what: "l'envoi de la question",
                  error,
                });
              });
          }}
        >
          {({ handleChange, handleBlur, handleSubmit, values, errors, touched }) => (
            <View>
              <View style={[styles.container, styles.centerIllustrationContainer]}>
                <Text style={styles.title}>Demande d&apos;aide</Text>
                <Text style={styles.subtitle}>Si vous souhaitez créer un groupe, vous pouvez consulter notre guide en <Text style={styles.link} onPress={() => handleUrl('https://www.topicapp.fr/static/documents/guide.pdf')}>cliquant ici</Text></Text>
              </View>
              <Divider />
              <View style={[styles.container, { marginBottom: 20 }]}>
                <FormTextInput
                  ref={messageInput}
                  label="Message"
                  value={values.message}
                  touched={touched.message}
                  error={errors.message}
                  onChangeText={handleChange('message')}
                  multiline
                  onBlur={handleBlur('message')}
                  style={styles.textInput}
                  numberOfLines={6}
                />
                {loggedIn ? (
                  <View style={styles.container}>
                    <Text>Votre email sera transmis à l&apos;équipe afin que vous puissiez recevoir une réponse</Text>
                  </View>
                ) : (
                  <FormTextInput
                    ref={emailInput}
                    label="Adresse email"
                    value={values.email}
                    touched={touched.email}
                    error={errors.email}
                    onChangeText={handleChange('email')}
                    onBlur={handleBlur('email')}
                    style={styles.textInput}
                  />
                )}
              </View>
              <View style={styles.container}>
                <Button
                  mode={Platform.OS !== 'ios' ? 'contained' : 'outlined'}
                  uppercase={Platform.OS !== 'ios'}
                  onPress={() => handleSubmit()}
                  style={{ flex: 1 }}
                  loading={state.feedback?.loading}
                  disabled={state.feedback?.loading}
                >
                  Envoyer
                </Button>
              </View>
            </View>
          )}
        </Formik>
      )}
    </Modal>
  );
};

const mapStateToProps = (state: State) => {
  const { account, linking } = state;
  return {
    loggedIn: account.loggedIn,
    accountId: account.accountInfo?.accountId,
    accountEmail: account.accountInfo?.email,
    state: linking.state,
  };
};

export default connect(mapStateToProps)(HelpModal);
