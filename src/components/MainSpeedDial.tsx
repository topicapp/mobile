import { useNavigation } from '@react-navigation/core';
import React from 'react';
import { FAB, useTheme } from 'react-native-paper';

import { updateArticleCreationData } from '@redux/actions/contentData/articles';
import { updateEventCreationData } from '@redux/actions/contentData/events';
import getGlobalStyles from '@styles/global';

const MainSpeedDial = () => {
  const [fabOpen, setFabOpen] = React.useState(false);
  const theme = useTheme();
  const styles = getGlobalStyles(theme);
  const navigation = useNavigation();

  return (
    <FAB.Group
      open={fabOpen}
      visible
      style={styles.bottomRightSpeedDial}
      onStateChange={() => setFabOpen(!fabOpen)}
      icon={fabOpen ? 'close' : 'plus'}
      actions={[
        {
          icon: 'newspaper',
          label: 'Article',
          onPress: () => {
            updateArticleCreationData({ editing: false });
            navigation.navigate('Main', {
              screen: 'Add',
              params: { screen: 'Article', params: { screen: 'Add' } },
            });
          },
        },
        {
          icon: 'calendar',
          label: 'Évènement',
          onPress: () => {
            updateEventCreationData({ editing: false });
            navigation.navigate('Main', {
              screen: 'Add',
              params: { screen: 'Event', params: { screen: 'Add' } },
            });
          },
        },
        {
          icon: 'account-group',
          label: 'Groupe',
          onPress: () => {
            navigation.navigate('Main', {
              screen: 'Add',
              params: {
                screen: 'Group',
                params: {
                  screen: 'Add',
                },
              },
            });
          },
        },
      ]}
    />
  );
};

export default MainSpeedDial;
