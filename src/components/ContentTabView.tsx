import { useIsFocused, useNavigation } from '@react-navigation/core';
import { key } from 'localforage';
import _ from 'lodash';
import React from 'react';
import { View, ActivityIndicator, FlatList } from 'react-native';
import { Divider, List, useTheme, Text } from 'react-native-paper';
import { connect } from 'react-redux';

import { AddCommentModal, CommentInlineCard, Illustration, ReportModal } from '@components';
import { clearArticles, searchArticles } from '@redux/actions/api/articles';
import { clearComments, updateComments } from '@redux/actions/api/comments';
import { clearEvents, searchEvents } from '@redux/actions/api/events';
import { clearGroups, searchGroups } from '@redux/actions/api/groups';
import { commentAdd, commentReport } from '@redux/actions/apiActions/comments';
import {
  ArticlePreload,
  EventPreload,
  ArticleRequestState,
  EventRequestState,
  State,
  GroupPreload,
  GroupRequestState,
  Group,
  Account,
  Comment,
  CommentRequestState,
  Content as ContentType,
} from '@ts/types';

import { InlineCard } from './Cards';
import CustomTabView from './CustomTabView';
import ErrorMessage from './ErrorMessage';
import { CategoryTitle } from './Typography';
import ArticleCard from './cards/Article';
import EventCard from './cards/Event';
import getStyles from './styles/CommentStylesheet';

type ContentTabViewProps = {
  searchParams: {
    authors?: string[];
    groups?: string[];
    group?: string;
    tags?: string[];
    schools?: string[];
    departments?: string[];
  };
  types?: ('articles' | 'events' | 'groups' | 'comments')[];
  parentId?: string;
  showHeader?: boolean;
  showSpinner?: boolean;
  header?: string;
  maxCards?: number;
  articles: ArticlePreload[];
  events: EventPreload[];
  groups: (GroupPreload | Group)[];
  articlesState: ArticleRequestState;
  eventsState: EventRequestState;
  groupsState: GroupRequestState;
  comments: Comment[];
  commentsState: CommentRequestState;
  account: Account;
};

const ContentTabView: React.FC<ContentTabViewProps> = React.memo(
  ({
    searchParams,
    articles,
    events,
    groups,
    comments,
    articlesState,
    eventsState,
    groupsState,
    commentsState,
    parentId,
    types = ['articles', 'events'],
    showHeader = true,
    showSpinner = true,
    header,
    maxCards = 0,
    account,
  }) => {
    const theme = useTheme();
    const { colors } = theme;
    const styles = getStyles(theme);
    const navigation = useNavigation();
    const [isCommentModalVisible, setCommentModalVisible] = React.useState(false);
    const [isCommentReportModalVisible, setCommentReportModalVisible] = React.useState(false);
    const [focusedComment, setFocusedComment] = React.useState<string | null>(null);
    const [replyingToComment, setReplyingToComment] = React.useState<string | null>(null);
    const replyingToPublisher = comments.find((c) => c._id === replyingToComment)?.publisher;

    const isFocused = useIsFocused();

    React.useEffect(() => {
      if (isFocused) {
        if (types.includes('articles')) {
          clearArticles(false, true, false, false, false);
          searchArticles('initial', '', searchParams, false);
        }
        if (types.includes('events')) {
          clearEvents(false, true);
          searchEvents('initial', '', searchParams, false, false, 'desc');
        }
        if (types.includes('groups')) {
          clearGroups(false, true, false);
          searchGroups('initial', '', searchParams, false);
        }
        if (types.includes('comments') && parentId) {
          clearComments(true, false);
          updateComments('initial', { parentId });
        }
      }
      // Use JSON.stringify sparingly with deep equality checks
      // Make sure the data that you want to stringify is somewhat small
      // otherwise it will hurt performance
    }, [JSON.stringify(searchParams), isFocused]);

    const pages = [];

    if (articles.length && types.includes('articles')) {
      pages.push({
        key: 'articles',
        title: 'Articles',
        component: (
          <View style={{ flex: 1 }}>
            {articlesState.search?.error && (
              <ErrorMessage
                type="axios"
                strings={{
                  what: 'le chargement des articles',
                  contentPlural: 'les articles',
                }}
                error={articlesState.search?.error}
                retry={() => searchArticles('initial', '', searchParams, false)}
              />
            )}
            {articlesState.search?.loading.initial && showSpinner && (
              <View style={styles.container}>
                <ActivityIndicator size="large" color={colors.primary} />
              </View>
            )}
            {articlesState.search?.success && (
              <FlatList
                scrollEnabled={false}
                data={maxCards ? articles.slice(0, maxCards) : articles}
                keyExtractor={(i) => i._id}
                ListFooterComponent={
                  articlesState.search.loading.initial && showSpinner ? (
                    <View style={[styles.container, { height: 50 }]}>
                      <ActivityIndicator size="large" color={colors.primary} />
                    </View>
                  ) : null
                }
                renderItem={({ item }: { item: ArticlePreload }) => (
                  <ArticleCard
                    article={item}
                    unread
                    navigate={() =>
                      navigation.navigate('Root', {
                        screen: 'Main',
                        params: {
                          screen: 'Display',
                          params: {
                            screen: 'Article',
                            params: {
                              screen: 'Display',
                              params: {
                                id: item._id,
                                title: item.title,
                              },
                            },
                          },
                        },
                      })
                    }
                  />
                )}
              />
            )}
          </View>
        ),
      });
    }

    if (events.length && types.includes('events')) {
      pages.push({
        key: 'events',
        title: 'Évènements',
        component: (
          <View style={{ flex: 1 }}>
            {eventsState.search?.error && (
              <ErrorMessage
                type="axios"
                strings={{
                  what: 'le chargement des évènements',
                  contentPlural: 'les évènements',
                }}
                error={eventsState.search?.error}
                retry={() => searchEvents('initial', '', searchParams, false, false, 'desc')}
              />
            )}
            {eventsState.search?.loading.initial && showSpinner && (
              <View style={styles.container}>
                <ActivityIndicator size="large" color={colors.primary} />
              </View>
            )}
            {eventsState.search?.success && (
              <FlatList
                scrollEnabled={false}
                data={maxCards ? events.slice(0, maxCards) : events}
                keyExtractor={(i) => i._id}
                ListFooterComponent={
                  eventsState.search.loading.initial && showSpinner ? (
                    <View style={[styles.container, { height: 50 }]}>
                      <ActivityIndicator size="large" color={colors.primary} />
                    </View>
                  ) : null
                }
                renderItem={({ item }: { item: EventPreload }) => (
                  <EventCard
                    event={item}
                    navigate={() =>
                      navigation.navigate('Root', {
                        screen: 'Main',
                        params: {
                          screen: 'Display',
                          params: {
                            screen: 'Event',
                            params: {
                              screen: 'Display',
                              params: {
                                id: item._id,
                                title: item.title,
                              },
                            },
                          },
                        },
                      })
                    }
                  />
                )}
              />
            )}
          </View>
        ),
      });
    }

    if (types.includes('comments') && parentId) {
      pages.push({
        key: 'comments',
        title: 'Discussion',
        component: (
          <FlatList
            data={comments}
            ListFooterComponent={
              <View>
                <Divider />
                <View style={[styles.container, { height: 50 }]}>
                  {commentsState.list.loading.next && showSpinner ? (
                    <ActivityIndicator size="large" color={colors.primary} />
                  ) : null}
                </View>
              </View>
            }
            ListEmptyComponent={() =>
              commentsState.list.success ? (
                <View style={styles.contentContainer}>
                  <View style={styles.centerIllustrationContainer}>
                    <Illustration name="comment-empty" height={200} width={200} />
                    <Text>Aucun message</Text>
                  </View>
                </View>
              ) : null
            }
            ListHeaderComponent={
              <View>
                {commentsState.list.error ? (
                  <ErrorMessage
                    type="axios"
                    strings={{
                      what: 'la récupération des messages',
                      contentPlural: 'des messages',
                    }}
                    error={commentsState.list.error}
                    retry={() => {
                      clearComments(true, false);
                      updateComments('initial', { parentId });
                    }}
                  />
                ) : null}

                {account.loggedIn ? (
                  <View>
                    <Divider />
                    <List.Item
                      title="Écrire un message"
                      titleStyle={styles.placeholder}
                      right={() => <List.Icon icon="comment-plus" color={colors.icon} />}
                      onPress={() => {
                        setReplyingToComment(null);
                        setCommentModalVisible(true);
                      }}
                    />

                    <Divider />
                  </View>
                ) : (
                  <View style={styles.contentContainer}>
                    <Text style={styles.disabledText}>Connectez-vous pour écrire un message</Text>
                    <Text>
                      <Text
                        onPress={() =>
                          navigation.navigate('Auth', {
                            screen: 'Login',
                            params: { goBack: true },
                          })
                        }
                        style={[styles.link, styles.primaryText]}
                      >
                        Se connecter
                      </Text>
                      <Text style={styles.disabledText}> ou </Text>
                      <Text
                        onPress={() =>
                          navigation.navigate('Auth', {
                            screen: 'Create',
                            params: { goBack: true },
                          })
                        }
                        style={[styles.link, styles.primaryText]}
                      >
                        Créer un compte
                      </Text>
                    </Text>
                  </View>
                )}
              </View>
            }
            keyExtractor={(comment: Comment) => comment._id}
            ItemSeparatorComponent={Divider}
            renderItem={({ item: comment }: { item: Comment }) => (
              <CommentInlineCard
                comment={comment}
                report={(commentId) => {
                  setFocusedComment(commentId);
                  setCommentReportModalVisible(true);
                }}
                fetch={() => updateComments('initial', { parentId })}
                isReply={false}
                loggedIn={account.loggedIn}
                navigation={navigation}
                reply={(id: string | null) => {
                  setReplyingToComment(id);
                  setCommentModalVisible(true);
                }}
                authors={[]}
              />
            )}
          />
        ),
      });
    }

    return (
      <View style={{ flex: 1 }}>
        {types.includes('groups') && !!groupsState.search?.error && (
          <ErrorMessage
            type="axios"
            strings={{
              what: 'le chargement des groups',
              contentPlural: 'les groups',
            }}
            error={groupsState.search?.error}
            retry={() => searchGroups('initial', '', searchParams, false)}
          />
        )}
        {types.includes('groups') && (
          <View>
            {groups
              .sort((a, b) => (b.cache?.followers || 0) - (a.cache?.followers || 0))
              .map((g) => (
                <View key={g._id}>
                  <InlineCard
                    avatar={g?.avatar}
                    title={g?.name || g?.displayName}
                    subtitle={`Groupe ${g?.type}`}
                    onPress={() =>
                      navigation.navigate('Root', {
                        screen: 'Main',
                        params: {
                          screen: 'Display',
                          params: {
                            screen: 'Group',
                            params: {
                              screen: 'Display',
                              params: { id: g?._id, title: g?.displayName },
                            },
                          },
                        },
                      })
                    }
                    badge={
                      account.loggedIn &&
                      account.accountInfo?.user &&
                      account.accountInfo?.user?.data?.following?.groups.some(
                        (gr) => gr._id === gr?._id,
                      )
                        ? 'heart'
                        : g?.official
                        ? 'check-decagram'
                        : undefined
                    }
                    badgeColor={colors.primary}
                  />
                </View>
              ))}
          </View>
        )}
        {showHeader && !!(articles.length || events.length) && (
          <View style={styles.container}>
            <CategoryTitle>
              {header || (pages.length === 1 ? `Derniers ${pages[0].title}` : 'Derniers contenus')}
            </CategoryTitle>
          </View>
        )}
        {(articlesState.search?.loading?.initial || eventsState.search?.loading?.initial) &&
        showSpinner ? (
          <View style={styles.container}>
            <ActivityIndicator size="large" color={colors.primary} />
          </View>
        ) : pages.length ? (
          <View style={{ flex: 1 }}>
            <CustomTabView hideTabBar={pages.length < 2} scrollEnabled={false} pages={pages} />
            <View style={{ height: 20 }} />
          </View>
        ) : articlesState.search?.loading.initial ||
          eventsState.search?.loading.initial ||
          commentsState.list?.loading.initial ? null : (
          <View style={[styles.contentContainer, { marginTop: 20 }]}>
            <View style={styles.centerIllustrationContainer}>
              <Illustration name="empty" height={200} width={200} />
              <Text>Aucun contenu pour le moment</Text>
            </View>
          </View>
        )}

        {types.includes('comments') && parentId ? (
          <>
            <AddCommentModal
              visible={isCommentModalVisible}
              setVisible={setCommentModalVisible}
              replyingToComment={replyingToComment}
              setReplyingToComment={setReplyingToComment}
              replyingToUsername={
                replyingToPublisher?.type === 'group'
                  ? replyingToPublisher?.group?.shortName || replyingToPublisher?.group?.name
                  : replyingToPublisher?.user?.info?.username
              }
              id={parentId}
              group={searchParams.group}
              reqState={{ comments: commentsState }}
              add={(
                publisher: { type: 'user' | 'group'; user?: string | null; group?: string | null },
                content: ContentType,
                parent: string,
                isReplying: boolean = false,
              ) =>
                commentAdd(publisher, content, parent, isReplying ? 'comment' : 'group').then(
                  () => {
                    updateComments('initial', { parentId });
                  },
                )
              }
            />
            <ReportModal
              visible={isCommentReportModalVisible}
              setVisible={setCommentReportModalVisible}
              contentId={focusedComment || ''}
              report={commentReport}
              state={commentsState.report}
              navigation={navigation}
            />
          </>
        ) : null}
      </View>
    );
  },
  // Function to tell React whether the component should rerender or not
  (prevProps, nextProps) => {
    // Compare all props to see if they've changed
    const prev = {
      params: prevProps.searchParams,
      // Only check ids because checking objects will take forever
      articleIds: prevProps.articles.map((a) => a._id),
      eventIds: prevProps.events.map((e) => e._id),
      groupIds: prevProps.groups.map((g) => g._id),
      commentIds: prevProps.comments.map((c) => c._id),
      articleState: prevProps.articlesState,
      eventState: prevProps.eventsState,
      groupsState: prevProps.groupsState,
      commentsState: prevProps.commentsState,
    };

    const next = {
      params: nextProps.searchParams,
      articleIds: nextProps.articles.map((a) => a._id),
      eventIds: nextProps.events.map((e) => e._id),
      groupIds: nextProps.groups.map((d) => d._id),
      commentIds: nextProps.comments.map((c) => c._id),
      articleState: nextProps.articlesState,
      eventState: nextProps.eventsState,
      groupsState: nextProps.groupsState,
      commentsState: nextProps.commentsState,
    };

    // Lodash performs deep equality check, works with arrays and objects
    // true = equal props = component does not rerender
    // false = props changed = component rerenders
    return _.isEqual(prev, next);
  },
);

const mapStateToProps = (state: State) => {
  const { articles, events, groups, account, comments } = state;
  return {
    articles: articles.search,
    events: events.search,
    groups: groups.search,
    articlesState: articles.state,
    eventsState: events.state,
    groupsState: groups.state,
    account,
    comments: comments.data,
    commentsState: comments.state,
  };
};

export default connect(mapStateToProps)(ContentTabView);
