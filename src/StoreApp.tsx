import { NavigationContainer, NavigationContainerRef } from '@react-navigation/native';
import Color from 'color';
import decode from 'jwt-decode';
import React from 'react';
import { Platform, Appearance, ColorSchemeName, View, Text } from 'react-native';
import RNBootSplash from 'react-native-bootsplash';
import changeNavigationBarColor from 'react-native-navigation-bar-color';
import { Provider as PaperProvider } from 'react-native-paper';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';

import { fetchGroups, fetchWaitingGroups, fetchAccount, logout } from '@redux/actions/data/account';
import { fetchLocationData } from '@redux/actions/data/location';
import updatePrefs from '@redux/actions/data/prefs';
import themes from '@styles/helpers/theme';
import { Preferences, State } from '@ts/types';
import { logger, Alert, setUpMessagingLoaded } from '@utils';
import { migrateReduxDB } from '@utils/compat/migrate';
import { trackPageview } from '@utils/plausible';

import AppNavigator from './index';
import screens from './screens';

type Props = {
  useSystemTheme: boolean;
  theme: Preferences['theme'];
  loggedIn: boolean;
  accountToken?: string;
  fontFamily: Preferences['fontFamily'];
  useDevServer: boolean;
  appOpens: number;
};

const StoreApp: React.FC<Props> = ({
  useSystemTheme,
  theme: themeName,
  loggedIn,
  accountToken,
  useDevServer,
  appOpens,
  fontFamily,
}) => {
  const [colorScheme, setColorScheme] = React.useState<ColorSchemeName>(
    useSystemTheme ? Appearance.getColorScheme() : 'light',
  );

  const handleAppearanceChange = (prefs: Appearance.AppearancePreferences) => {
    setColorScheme(Platform.OS === 'ios' ? Appearance.getColorScheme() : prefs.colorScheme);
    if (Platform.OS === 'android' && Platform.Version >= 28) {
      // This only works on android 9 and above
      changeNavigationBarColor(theme.colors.tabBackground, !theme.dark, true);
    }
  };

  React.useEffect(() => {
    Appearance.addChangeListener(handleAppearanceChange);
    return () => {
      Appearance.removeChangeListener(handleAppearanceChange);
    };
  }, [useSystemTheme]);

  const routeNameRef = React.useRef<string | undefined>();
  const navigationRef = React.useRef<NavigationContainerRef>(null);

  React.useEffect(() => {
    migrateReduxDB();

    setUpMessagingLoaded();

    // Increase app opens
    updatePrefs({ appOpens: appOpens + 1 });

    if (loggedIn && accountToken) {
      let decoded;
      try {
        decoded = decode(accountToken) as { exp?: number } | null;
      } catch (error) {
        logger.warn('JWT decode failed');
      }
      if (!decoded?.exp) {
        logger.warn('Unable to find token expiry');
      } else if (decoded.exp - 7200 < new Date().valueOf() / 1000) {
        // Si le token va expirer dans moins d'une heure
        logger.warn('Token expired, logging out');
        logout();
        Alert.alert(
          'Votre session a expiré',
          'Veuillez vous reconnecter',
          [
            { text: 'Fermer' },
            {
              text: 'Se connecter',
              onPress: () =>
                navigationRef.current?.navigate('Auth', {
                  screen: 'Login',
                  params: { goBack: true },
                }),
            },
          ],
          { cancelable: true },
        );
      }
    }

    fetchLocationData().catch((e) => logger.warn(`fetchLocationData err ${e}`));
    fetchGroups().catch((e) => logger.warn(`fetchGroups err ${e}`));
    fetchWaitingGroups().catch((e) => logger.warn(`fetchWaitingGroups err ${e}`));
    fetchAccount().catch((e) => logger.warn(`fetchAccount err ${e}`));

    onStateChange();
  }, [null]);

  const theme = useSystemTheme
    ? themes[colorScheme === 'dark' ? 'dark' : 'light']
    : themes[themeName];

  const linking = {
    prefixes: [
      'https://topicapp.fr',
      'https://go.topicapp.fr',
      'https://www.topicapp.fr',
      'topic://',
    ],
    config: {
      screens,
    },
  };

  const { colors } = theme;

  const navTheme = {
    dark: theme.dark,
    colors: {
      primary: colors.appBarText,
      background: colors.background,
      card: colors.appBar,
      text: colors.appBarText,
      border: colors.primary,
      notification: colors.primary,
    },
  };

  const insets = useSafeAreaInsets();

  const onStateChange = async () => {
    try {
      const previousRouteName = routeNameRef.current;
      const currentRouteName = navigationRef.current?.getCurrentRoute()?.name;

      if (previousRouteName !== currentRouteName) {
        const state = navigationRef?.current?.getRootState();

        const parseRootState = (navState: any, name?: string) => {
          let url = '';
          if (navState && navState.routes && navState.routes.length) {
            const next =
              navState.type === 'tab'
                ? navState.routes[navState.index]
                : navState.routes[navState.routes.length - 1];
            url = parseRootState(next.state, next.name);
          }
          url = `${name || navState?.name || ''}/${url}`;
          return url;
        };

        const stateUrl = parseRootState(state);
        logger.debug(`Navigating to ${stateUrl}`);

        const parameters = [];
        if (Platform.OS === 'web') {
          const params = new URLSearchParams(window.location.search) as {
            s?: string;
            c?: string;
            m?: string;
          };
          if (params.s) {
            parameters.push(`utm_source=${params.s}`);
          }
          if (params.c) {
            parameters.push(`utm_campaign=${params.c}`);
          }
          if (params.m) {
            parameters.push(`utm_medium=${params.m}`);
          }
        }

        trackPageview({
          url: `${
            Platform.OS === 'web' ? 'https://topicapp.fr' : 'https://app.topicapp.fr'
          }${stateUrl}?${parameters.join('&')}`,
        });
      }

      routeNameRef.current = currentRouteName;
    } catch (e) {
      logger.warn('Error during pageview track');
    }
  };

  return (
    <PaperProvider theme={theme}>
      <>
        <NavigationContainer
          linking={linking}
          ref={navigationRef}
          fallback={null}
          onReady={Platform.OS === 'web' ? undefined : () => RNBootSplash.hide({ fade: true })}
          theme={navTheme}
          onStateChange={onStateChange}
          documentTitle={{
            formatter: (options, _route) => {
              return options?.title ? `${options.title} · Topic` : 'Topic';
            },
          }}
        >
          {useDevServer ? (
            <View style={{ flex: 1 }}>
              <View
                style={{
                  position: 'absolute',
                  right: 5,
                  zIndex: 10000,
                  marginTop: insets.top,
                  paddingHorizontal: 2,
                  borderRadius: 3,
                  backgroundColor: Color('#d11111').alpha(0.6).rgb().string(),
                  flexDirection: 'row',
                  alignItems: 'center',
                }}
              >
                <Icon name="wrench" color="white" size={12} />
                <Text style={{ color: 'white', fontSize: 12, paddingLeft: 5 }}>
                  Serveur de développement
                </Text>
              </View>
              <AppNavigator />
            </View>
          ) : (
            <AppNavigator />
          )}
        </NavigationContainer>
      </>
    </PaperProvider>
  );
};

const mapStateToProps = (state: State) => {
  const { useSystemTheme, theme, useDevServer, appOpens, fontFamily } = state.preferences;
  const { accountInfo, loggedIn } = state.account;
  const { accountToken } = accountInfo || {};
  return { useSystemTheme, theme, useDevServer, appOpens, fontFamily, accountToken, loggedIn };
};

export default connect(mapStateToProps)(StoreApp);
