import { Platform } from 'react-native';

import { Config } from '@constants';
import Store from '@redux/store';
import { UPDATE_UPLOAD_STATE } from '@ts/redux';
import { AppThunk, RequestState } from '@ts/types';
import { request, logger } from '@utils';

function updateUploadStateCreator(state: { permission?: RequestState; upload?: RequestState }) {
  return {
    type: UPDATE_UPLOAD_STATE,
    data: state,
  };
}

async function upload(
  groupId: string,
  resizeMode?: 'content-primary' | 'content-inline' | 'avatar',
  avatar?: boolean,
  camera?: boolean | 'back' | 'front',
) {
  logger.warn('Upload was called from web');
  return null;
}

async function updateUploadState(state: { permission?: RequestState; upload?: RequestState }) {
  return Store.dispatch(updateUploadStateCreator(state));
}

export default upload;
export { upload, updateUploadState };
