import { Platform } from 'react-native';
import LocationService from 'react-native-geolocation-service';
import * as Permissions from 'react-native-permissions';

import logger from './logger';
import { trackEvent } from './plausible';

export namespace Location {
  /**
   * Get status on location permission
   * @returns String indicating whether location can be requested or not
   */
  export async function getStatus(): Promise<'yes' | 'no' | 'never' | 'error'> {
    return 'never';
  }

  /**
   * Get status on location permission
   * @returns String indicating whether location can be requested or not
   */
  export async function request(): Promise<'yes' | 'no' | 'never' | 'error'> {
    return 'never';
  }

  /**
   * Get device location, assumes location permission has already been requested
   * @returns Object with latitude and longitude information
   */
  export async function getCoordinates() {
    return null;
  }
}
