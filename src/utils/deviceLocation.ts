import { Platform } from 'react-native';
import LocationService from 'react-native-geolocation-service';
import * as Permissions from 'react-native-permissions';

import logger from './logger';
import { trackEvent } from './plausible';

export namespace Location {
  /**
   * Get status on location permission
   * @returns String indicating whether location can be requested or not
   */
  export async function getStatus(): Promise<'yes' | 'no' | 'never' | 'error'> {
    if (Platform.OS === 'web') return 'no';
    let result: Permissions.PermissionStatus;
    try {
      result = await Permissions.check(
        Platform.OS === 'android'
          ? Permissions.PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION
          : Permissions.PERMISSIONS.IOS.LOCATION_WHEN_IN_USE,
      );
    } catch (e) {
      logger.warn('deviceLocation: error encountered while getting location permission');
      return 'error';
    }
    // User previously granted permission
    if (result === 'granted' || result === 'limited') {
      return 'yes';
    } else if (result === 'denied') {
      return 'no';
    } else {
      return 'never';
    }
  }

  /**
   * Get status on location permission
   * @returns String indicating whether location can be requested or not
   */
  export async function request(): Promise<'yes' | 'no' | 'never' | 'error'> {
    if (Platform.OS === 'web') return 'no';
    let result: Permissions.PermissionStatus;
    try {
      result = await Permissions.request(
        Platform.OS === 'android'
          ? Permissions.PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION
          : Permissions.PERMISSIONS.IOS.LOCATION_WHEN_IN_USE,
      );
    } catch (e) {
      logger.warn('deviceLocation: error encountered while asking for location permission');
      return 'error';
    }
    // User previously granted permission
    if (result === 'granted' || result === 'limited') {
      trackEvent('landing:locate-accept-permission');
      return 'yes';
    } else if (result === 'denied') {
      trackEvent('landing:locate-reject-permission');
      return 'no';
    } else {
      trackEvent('landing:locate-reject-permission');
      return 'never';
    }
  }

  export type Coordinates = LocationService.GeoCoordinates;

  /**
   * Get device location, assumes location permission has already been requested
   * @returns Object with latitude and longitude information
   */
  export async function getCoordinates(): Promise<Coordinates> {
    const info = await new Promise<LocationService.GeoPosition>((resolve, reject) =>
      LocationService.getCurrentPosition(resolve, reject, {
        enableHighAccuracy: false,
        timeout: 15000,
      }),
    );
    return info.coords;
  }
}
