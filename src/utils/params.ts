import Store from '@redux/store';
import { ReduxLocation } from '@ts/types';

function getContentParams(): ReduxLocation {
  const { location } = Store.getState();
  // The filter is normally not necessary but we seem to frequently have problems with the data
  return {
    schools: location.schools.filter((s) => !!s),
    departments: location.departments.filter((d) => !!d),
    global: location.global || false,
  };
}

export { getContentParams };
export default getContentParams;
