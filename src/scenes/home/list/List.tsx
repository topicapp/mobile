import React from 'react';
import { View, ScrollView, Alert } from 'react-native';
import { ProgressBar, Title, List, Divider, Subheading, useTheme } from 'react-native-paper';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';

import { Avatar, ErrorMessage, Illustration, MainFeedback, CollapsibleView } from '@components';
import { fetchLocationData } from '@redux/actions/data/location';
import { Account, LocationList, Preferences, State } from '@ts/types';
import { logger, Format, checkPermission, Permissions, quickDevServer } from '@utils';

import { HomeTwoScreenNavigationProp } from '../HomeTwo';
import getStyles from './styles';

type LocationItemAccordionProps = React.ComponentProps<typeof List.Item> & {
  chevronColor: string;
};

const LocationItemAccordion: React.FC<LocationItemAccordionProps> = ({
  children,
  onPress,
  chevronColor,
  ...rest
}) => {
  const [expanded, setExpanded] = React.useState(false);
  return (
    <View>
      <List.Item
        {...rest}
        onPress={() => {
          setExpanded(!expanded);
          onPress?.();
        }}
      />
      <Icon
        name={expanded ? 'chevron-up' : 'chevron-down'}
        color={chevronColor}
        size={23}
        style={{
          position: 'absolute',
          right: 16,
          top: 14,
        }}
      />
      <CollapsibleView collapsed={!expanded}>{children}</CollapsibleView>
    </View>
  );
};

type MoreListProps = {
  navigation: HomeTwoScreenNavigationProp<'List'>;
  location: LocationList;
  account: Account;
  preferences: Preferences;
};

const MoreList: React.FC<MoreListProps> = ({ navigation, location, account, preferences }) => {
  const theme = useTheme();
  const styles = getStyles(theme);
  const { colors } = theme;

  const insets = useSafeAreaInsets();

  const [feedbackVisible, setFeedbackVisible] = React.useState(false);

  const locationAccordionItems: React.ReactElement[] = [];
  location.schoolData.forEach((school) => {
    locationAccordionItems.push(
      <List.Item
        key={school._id}
        title={school?.shortName || school?.name}
        left={() => <List.Icon icon="school" />}
        onPress={() =>
          navigation.navigate('Main', {
            screen: 'Display',
            params: {
              screen: 'School',
              params: { screen: 'Display', params: { id: school._id } },
            },
          })
        }
      />,
    );
  });
  location.departmentData.forEach((department) => {
    locationAccordionItems.push(
      <List.Item
        key={department._id}
        title={department.shortName || department.name}
        left={() => <List.Icon icon="map-outline" />}
        onPress={() =>
          navigation.navigate('Main', {
            screen: 'Display',
            params: {
              screen: 'Department',
              params: { screen: 'Display', params: { id: department._id } },
            },
          })
        }
      />,
    );
  });
  if (location.global) {
    locationAccordionItems.push(
      <List.Item key="global" left={() => <List.Icon icon="flag" />} title="France entière" />,
    );
  }
  locationAccordionItems.push(
    <List.Item
      key="settings"
      left={() => <List.Icon icon="cog-outline" />}
      title="Changer"
      onPress={() => {
        navigation.navigate('Main', {
          screen: 'More',
          params: { screen: 'Settings', params: { screen: 'SelectLocation' } },
        });
      }}
    />,
  );

  return (
    <View style={styles.page}>
      <ScrollView>
        <View style={[styles.profileBackground, { paddingTop: insets.top }]}>
          {(location.state.fetch.loading ||
            account.state.fetchAccount.loading ||
            account.state.fetchGroups.loading) && <ProgressBar indeterminate />}
          {location.state.fetch.error && (
            <ErrorMessage
              type="axios"
              strings={{
                what: 'la mise à jour du profil',
                contentPlural: 'des informations de profil',
                contentSingular: 'Le profil',
              }}
              error={[
                location.state.fetch.error,
                account.state.fetchAccount.error,
                account.state.fetchGroups.error,
              ]}
              retry={fetchLocationData}
            />
          )}
          <View style={styles.profileIconContainer}>
            {account.loggedIn ? (
              <View style={{ flexDirection: 'row' }}>
                <View style={{ marginRight: 10 }}>
                  <Avatar
                    avatar={account.accountInfo?.user?.info.avatar}
                    size={60}
                    style={styles.avatar}
                  />
                </View>
                <View style={{ marginTop: -10 }}>
                  <Title style={styles.title} numberOfLines={1}>
                    {Format.fullUserName(account.accountInfo.user)}
                  </Title>
                  <Subheading
                    style={[styles.subtitle, { flex: 1 }]}
                    ellipsizeMode="tail"
                    numberOfLines={1}
                  >
                    @{account.accountInfo.user.info.username}
                  </Subheading>
                </View>
              </View>
            ) : (
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Illustration
                  name="topic-icon"
                  height={60}
                  width={60}
                  style={[styles.avatar, { borderRadius: 30 }]}
                />
                <Title style={styles.topic}>Topic</Title>
              </View>
            )}
          </View>
        </View>
        <Divider />
        <List.Section>
          <LocationItemAccordion
            title="Mes lieux"
            left={() => <List.Icon icon="map-marker" />}
            chevronColor={colors.icon}
          >
            {locationAccordionItems}
          </LocationItemAccordion>
        </List.Section>
        <Divider />
        {account.loggedIn ? (
          <List.Section>
            <List.Item
              title="Mon profil"
              left={() => <List.Icon icon="account-outline" />}
              onPress={() => {
                navigation.navigate('Main', {
                  screen: 'Display',
                  params: {
                    screen: 'User',
                    params: {
                      screen: 'Display',
                      params: {
                        id: account.accountInfo?.accountId,
                        title: account.accountInfo.user?.displayName,
                      },
                    },
                  },
                });
              }}
            />
            <List.Item
              title="Mes groupes"
              left={() => <List.Icon icon="account-group-outline" />}
              onPress={() => {
                navigation.navigate('Main', {
                  screen: 'More',
                  params: { screen: 'MyGroups', params: { screen: 'List' } },
                });
              }}
            />
            <List.Item
              title="Notifications"
              left={() => <List.Icon icon="bell-outline" />}
              onPress={() => {
                navigation.navigate('Main', {
                  screen: 'More',
                  params: { screen: 'Notifications', params: { screen: 'Notifications' } },
                });
              }}
            />
            {(checkPermission(account, {
              permission: Permissions.ARTICLE_VERIFICATION_VIEW,
              scope: {},
            }) ||
              checkPermission(account, {
                permission: Permissions.EVENT_VERIFICATION_VIEW,
                scope: {},
              }) ||
              checkPermission(account, {
                permission: Permissions.GROUP_VERIFICATION_VIEW,
                scope: {},
              }) ||
              checkPermission(account, {
                permission: Permissions.PLACE_VERIFICATION_VIEW,
                scope: {},
              })) && (
              <List.Item
                title="Modération"
                left={() => <List.Icon icon="shield-check-outline" />}
                onPress={() => {
                  navigation.navigate('Main', {
                    screen: 'More',
                    params: { screen: 'Moderation', params: { screen: 'List' } },
                  });
                }}
              />
            )}
          </List.Section>
        ) : (
          <List.Section>
            <List.Item
              title="Se connecter"
              left={() => <List.Icon icon="account-outline" />}
              onPress={() => {
                navigation.navigate('Auth', {
                  screen: 'Login',
                });
              }}
            />
            <List.Item
              title="Créer un compte"
              left={() => <List.Icon icon="account-plus-outline" />}
              onPress={() => {
                navigation.navigate('Auth', {
                  screen: 'Create',
                });
              }}
            />
          </List.Section>
        )}
        <Divider />
        <List.Section>
          <List.Item
            title="Paramètres"
            left={() => <List.Icon icon="cog-outline" />}
            onPress={() => {
              navigation.navigate('Main', {
                screen: 'More',
                params: { screen: 'Settings', params: { screen: 'List' } },
              });
            }}
          />
          <List.Item
            title="Feedback"
            left={() => <List.Icon icon="comment-outline" />}
            onPress={() => {
              setFeedbackVisible(true);
            }}
          />
          <List.Item
            title="À propos"
            left={() => <List.Icon icon="information-outline" />}
            onPress={() => {
              navigation.navigate('Main', {
                screen: 'More',
                params: { screen: 'About', params: { screen: 'List' } },
              });
            }}
          />
          {preferences.quickDevServer ? (
            <List.Item
              title="Serveur de dev"
              left={() => <List.Icon icon="wrench-outline" />}
              onPress={() => {
                if (preferences.useDevServer) {
                  Alert.alert("Redémarrez l'application pour retourner");
                } else {
                  quickDevServer();
                }
              }}
            />
          ) : null}
        </List.Section>
      </ScrollView>
      <MainFeedback visible={feedbackVisible} setVisible={setFeedbackVisible} />
    </View>
  );
};

const mapStateToProps = (state: State) => {
  const { account, location, preferences } = state;
  return {
    account,
    location,
    preferences,
  };
};

export default connect(mapStateToProps)(MoreList);
