import moment from 'moment';
import React from 'react';
import { View, Dimensions } from 'react-native';
import {
  Caption,
  Card,
  Divider,
  Paragraph,
  Subheading,
  Text,
  Title,
  useTheme,
} from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { CardBase, Content } from '@components';
import { Event, Image } from '@ts/types';
import { Format, Alert } from '@utils';

function getLayout() {
  const { height, width } = Dimensions.get('window');
  return {
    height,
    width,
  };
}

const EventDisplayProgram: React.FC<{ event: Event }> = ({ event }) => {
  const { program } = event;

  const theme = useTheme();
  const { colors } = theme;

  if (Array.isArray(program) && program.length > 0) {
    return (
      <View>
        {program
          .sort((a, b) => (a?.duration?.start as any) - (b?.duration?.start as any))
          .map((p) => (
            <View style={{ marginHorizontal: 5, marginBottom: 3 }} key={p._id}>
              <CardBase contentContainerStyle={{ paddingTop: 0, paddingBottom: 0 }}>
                <Card.Content>
                  <View
                    style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 5 }}
                  >
                    <View style={{ flex: 1 }}>
                      <Title>{p?.title}</Title>
                      <Caption>
                        <Icon name="clock" color={colors.subtext} size={12} />{' '}
                        {Format.eventProgramDuration(p?.duration, event.duration)}
                      </Caption>
                      {p?.address?.shortName ? (
                        <Caption>
                          <Icon name="map-marker" color={colors.subtext} size={12} />{' '}
                          {p?.address?.shortName}
                        </Caption>
                      ) : null}
                    </View>
                  </View>
                </Card.Content>
                {p?.description?.data ? (
                  <View style={{ marginTop: 10 }}>
                    <Divider />
                    <Card.Content style={{ marginTop: 5, marginBottom: 10 }}>
                      <View
                        style={{
                          flex: 1,
                        }}
                      >
                        <Content data={p.description.data} parser={p.description.parser} />
                      </View>
                    </Card.Content>
                  </View>
                ) : (
                  <View style={{ height: 10 }} />
                )}
              </CardBase>
            </View>
          ))}
      </View>
    );
  }
  return (
    <View style={{ alignSelf: 'center', margin: 40 }}>
      <Subheading>Pas de programme</Subheading>
    </View>
  );
};

export default EventDisplayProgram;
