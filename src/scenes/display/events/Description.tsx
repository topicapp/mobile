import moment from 'moment';
import React from 'react';
import { View, FlatList, ActivityIndicator, Platform, Clipboard } from 'react-native';
import { Text, Divider, List, Button, useTheme } from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';
import shortid from 'shortid';

import {
  CategoryTitle,
  Content,
  InlineCard,
  Illustration,
  ErrorMessage,
  PlatformTouchable,
  CommentInlineCard,
} from '@components';
import { updateComments } from '@redux/actions/api/comments';
import { fetchEventMy } from '@redux/actions/api/events';
import { eventLike } from '@redux/actions/apiActions/events';
import {
  State,
  Account,
  Event,
  EventPlace,
  Duration,
  EventRequestState,
  CommentRequestState,
  Comment,
  EventMyInfo,
} from '@ts/types';
import {
  logger,
  checkPermission,
  Errors,
  shareContent,
  handleUrl,
  Permissions,
  Format,
  Alert,
} from '@utils';

import CommentsHeader from '../components/CommentsHeader';
import ContentLikeShare from '../components/ContentLikeShare';
import PublisherInfo from '../components/PublisherInfo';
import EventBaseInfo from './components/EventBaseInfo';
import MessageInlineCard from './components/Message';
import MessageList from './components/MessageList';
import getStyles from './styles';

function getPlaceLabels(place: EventPlace) {
  switch (place.type) {
    case 'standalone': {
      const { address } = place;
      if (!address?.address) {
        return { title: '', description: '' };
      }
      const { number, street, extra, city } = address.address;

      let title = '';
      if (address.shortName) {
        title = address.shortName;
      } else {
        if (number) {
          title += `${number}, `;
        }
        if (street) {
          title += `${street} `;
        }
        if (extra) {
          title += `(${extra})`;
        }
      }

      return {
        title,
        description: city,
      };
    }
    case 'school': {
      const { associatedSchool } = place;
      if (!associatedSchool?.address?.address) {
        return { title: '', description: '' };
      }
      return {
        title: associatedSchool.name,
        description: Format.address(associatedSchool.address),
      };
    }
    case 'place': {
      const { associatedPlace } = place;
      if (!associatedPlace?.address?.address) {
        return { title: '', description: '' };
      }
      const { number, street, extra, city } = associatedPlace.address.address;
      return {
        title: associatedPlace.displayName,
        description: Format.address(associatedPlace.address),
      };
    }
    case 'online':
      return {
        title: 'Évènement en ligne',
        description: place.link
          ?.replace('http://', '')
          ?.replace('https://', '')
          ?.split(/[/?#]/)?.[0],
        icon: 'link',
        onPress: () => handleUrl(place.link),
        onCopy: () => Clipboard.setString(place.link),
      };
    default:
      return {
        title: 'Inconnu',
        description: 'Endroit non spécifié',
      };
  }
}

function getTimeLabels(timeData: Duration, startTime: number | null, endTime: number | null) {
  if (timeData?.start && timeData?.end) {
    return {
      dateString: moment(timeData.start).isSame(timeData.end, 'day')
        ? `Le ${moment(timeData.start).format('DD/MM/YYYY')}`
        : `Du ${moment(timeData.start).format('DD/MM/YYYY')} au ${moment(timeData.end).format(
            'DD/MM/YYYY',
          )}`,
      timeString:
        startTime && endTime
          ? `De ${startTime}h à ${endTime}h`
          : `De ${moment(timeData.start).hour()}h à ${moment(timeData.end).hour()}h`,
    };
  }
  return {
    dateString: 'Aucune date spécifiée',
    timeString: null,
  };
}

type CombinedReqState = {
  events: EventRequestState;
  comments: CommentRequestState;
};

type EventDisplayHeaderProps = {
  event: Event;
  eventMy: EventMyInfo | null;
  navigation: any;
  account: Account;
  verification: boolean;
  commentsDisplayed: boolean;
  setCommentModalVisible: (state: boolean) => void;
  reqState: CombinedReqState;
  setMessageModalVisible: (state: boolean) => void;
  setReplyingToComment: (id: string | null) => void;
};

function EventDisplayDescriptionHeader({
  event,
  eventMy,
  navigation,
  account,
  verification,
  reqState,
  setCommentModalVisible,
  setMessageModalVisible,
  setReplyingToComment,
  commentsDisplayed,
}: EventDisplayHeaderProps) {
  const theme = useTheme();
  const styles = getStyles(theme);
  const { colors } = theme;

  if (!event) {
    return null;
  }

  return (
    <View>
      <EventBaseInfo places={event.places} duration={event.duration} />
      <Divider />
      <View style={[styles.description, { marginBottom: 20 }]}>
        <Content parser={event.description?.parser || 'plaintext'} data={event.description?.data} />
      </View>
      <ContentLikeShare
        navigation={navigation}
        type="event"
        id={event._id}
        title={event.title}
        contentMy={eventMy}
      />
      <Divider />
      <MessageList
        group={event.group}
        messages={event.messages}
        setMessageModalVisible={setMessageModalVisible}
      />
      <Divider />
      <PublisherInfo navigation={navigation} authors={event.authors} group={event.group} />
      {!verification && commentsDisplayed && (
        <CommentsHeader
          navigation={navigation}
          id={event._id}
          type="event"
          setReplyingToComment={setReplyingToComment}
          setCommentModalVisible={setCommentModalVisible}
        />
      )}
    </View>
  );
}

type EventDisplayDescriptionProps = {
  event: Event;
  my: EventMyInfo | null;
  navigation: any;
  account: Account;
  verification: boolean;
  reqState: { events: EventRequestState; comments: CommentRequestState };
  commentsDisplayed: boolean;
  setCommentModalVisible: (state: boolean) => any;
  setFocusedComment: (id: string) => any;
  setCommentReportModalVisible: (state: boolean) => any;
  setMessageModalVisible: (state: boolean) => any;
  comments: Comment[];
  setReplyingToComment: (id: string | null) => any;
  id: string;
};

function EventDisplayDescription({
  event,
  my,
  verification,
  account,
  navigation,
  comments,
  commentsDisplayed,
  reqState,
  setFocusedComment,
  setCommentReportModalVisible,
  setMessageModalVisible,
  setCommentModalVisible,
  setReplyingToComment,
  id,
}: EventDisplayDescriptionProps) {
  const theme = useTheme();
  const styles = getStyles(theme);
  const { colors } = theme;

  const eventMy: EventMyInfo | null = my?._id === id ? my : null;

  React.useEffect(() => {
    updateComments('initial', { parentId: id });
  }, [null]);

  const eventComments = comments.filter(
    (c) =>
      c.parent === id &&
      (c.publisher?.type !== 'user' || c.publisher?.user?._id !== account.accountInfo?.accountId),
  );

  return (
    <FlatList
      ListHeaderComponent={() =>
        event ? (
          <EventDisplayDescriptionHeader
            setMessageModalVisible={setMessageModalVisible}
            event={event}
            eventMy={eventMy}
            account={account}
            navigation={navigation}
            verification={verification}
            setCommentModalVisible={setCommentModalVisible}
            setReplyingToComment={setReplyingToComment}
            reqState={reqState}
            commentsDisplayed={commentsDisplayed}
          />
        ) : null
      }
      data={
        commentsDisplayed && reqState.events.info.success && !verification
          ? [...(eventMy?.comments || []), ...eventComments]
          : []
      }
      // onEndReached={() => {
      //   console.log('comment end reached');
      //   updateComments('next', { parentId: id });
      // }}
      // onEndReachedThreshold={0.5}
      keyExtractor={(comment: Comment) => comment._id}
      ItemSeparatorComponent={Divider}
      ListFooterComponent={
        reqState.events.info.success ? (
          <View>
            <Divider />
            <View style={[styles.container, { height: 50 }]}>
              {reqState.comments.list.loading.next ?? (
                <ActivityIndicator size="large" color={colors.primary} />
              )}
            </View>
          </View>
        ) : undefined
      }
      ListEmptyComponent={() =>
        reqState.comments.list.success &&
        reqState.events.info.success &&
        !verification &&
        commentsDisplayed ? (
          <View style={styles.contentContainer}>
            <View style={styles.centerIllustrationContainer}>
              <Illustration name="comment-empty" height={200} width={200} />
              <Text>Aucun commentaire</Text>
            </View>
          </View>
        ) : null
      }
      renderItem={({ item: comment }: { item: Comment }) => (
        <CommentInlineCard
          comment={comment}
          fetch={() => updateComments('initial', { parentId: id })}
          isReply={false}
          report={(commentId) => {
            setFocusedComment(commentId);
            setCommentReportModalVisible(true);
          }}
          reply={(commentId) => {
            setReplyingToComment(commentId);
            setCommentModalVisible(true);
          }}
          authors={[...(event.authors?.map((a) => a._id) || []), event.group?._id || '']}
          loggedIn={account.loggedIn}
          navigation={navigation}
        />
      )}
    />
  );
}

const mapStateToProps = (state: State) => {
  const { account, comments, events } = state;
  return {
    account,
    my: events.my,
    comments: comments.data,
    reqState: { events: events.state, comments: comments.state },
  };
};

export default connect(mapStateToProps)(EventDisplayDescription);
