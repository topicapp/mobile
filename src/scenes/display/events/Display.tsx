import { RouteProp, useIsFocused } from '@react-navigation/native';
import moment from 'moment';
import React from 'react';
import {
  View,
  ActivityIndicator,
  Animated,
  Platform,
  ScrollView,
  useWindowDimensions,
} from 'react-native';
import { Text, Title, Card, Button, useTheme } from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';

import {
  ErrorMessage,
  TagList,
  AnimatingHeader,
  ReportModal,
  CustomTabView,
  PlatformTouchable,
  AutoHeightImage,
  AddCommentModal,
  ContentImage,
} from '@components';
import { updateComments } from '@redux/actions/api/comments';
import { fetchEvent, fetchEventMy, fetchEventVerification } from '@redux/actions/api/events';
import { commentAdd, commentReport } from '@redux/actions/apiActions/comments';
import {
  eventReport,
  eventVerificationApprove,
  eventDelete,
  eventMessagesAdd,
  eventDeverify,
} from '@redux/actions/apiActions/events';
import { addEventRead, updateEventCreationData } from '@redux/actions/contentData/events';
import {
  State,
  CommentRequestState,
  Account,
  EventRequestState,
  Event,
  Preferences,
  EventPreload,
  Content,
  EventVerification,
  EventPlace,
  EventCreationDataPlace,
} from '@ts/types';
import {
  getImageUrl,
  handleUrl,
  checkPermission,
  Alert,
  Errors,
  shareContent,
  Permissions,
  trackEvent,
} from '@utils';

import type { EventDisplayScreenNavigationProp, EventDisplayStackParams } from '.';
import ContentVerification from '../components/ContentVerification';
import EventDisplayContact from './Contact';
import EventDisplayDescription from './Description';
import EventDisplayProgram from './Program';
import AddMessageModal from './components/AddMessageModal';
import getHeaderActions from './getHeaderActions';
import getStyles from './styles';

// Common types
type Navigation = EventDisplayScreenNavigationProp<'Display'>;
type Route = RouteProp<EventDisplayStackParams, 'Display'>;
type CombinedReqState = {
  events: EventRequestState;
  comments: CommentRequestState;
};

type EventDisplayProps = {
  route: Route;
  navigation: Navigation;
  item: Event | null;
  dataUpcoming: EventPreload[];
  dataPassed: EventPreload[];
  search: EventPreload[];
  reqState: CombinedReqState;
  account: Account;
  preferences: Preferences;
  dual?: boolean;
};

const EventDisplay: React.FC<EventDisplayProps> = ({
  route,
  navigation,
  dataUpcoming,
  dataPassed,
  search,
  item,
  reqState,
  account,
  preferences,
  dual = false,
}) => {
  // Pour changer le type de route.params, voir ../index.tsx
  const { id, verification = false } = route.params;

  const theme = useTheme();
  const styles = getStyles(theme);
  const { colors } = theme;

  const isFocused = useIsFocused();

  const event: Event | EventPreload | undefined | null =
    item?._id === id
      ? item
      : dataUpcoming.find((a) => a._id === id) ||
        dataPassed.find((a) => a._id === id) ||
        search.find((a) => a._id === id) ||
        null;

  const fetch = () => {
    if (verification) {
      fetchEventVerification(id);
    } else {
      fetchEvent(id).then(() => {
        if (preferences.history && event) {
          addEventRead(id, event.title);
        }
        setCommentsDisplayed(true);
      });
      if (account.loggedIn) {
        fetchEventMy(id);
      }
    }
  };

  React.useEffect(() => {
    if (isFocused) {
      fetch();
      updateComments('initial', { parentId: id });
    }
  }, [id, isFocused]);

  const scrollViewRef = React.createRef<ScrollView>();

  const [commentsDisplayed, setCommentsDisplayed] = React.useState(false);

  const [isCommentModalVisible, setCommentModalVisible] = React.useState(false);
  const [isEventReportModalVisible, setEventReportModalVisible] = React.useState(false);
  const [isMessageModalVisible, setMessageModalVisible] = React.useState(false);

  const [isCommentReportModalVisible, setCommentReportModalVisible] = React.useState(false);
  const [focusedComment, setFocusedComment] = React.useState<string | null>(null);

  const [replyingToComment, setReplyingToComment] = React.useState<string | null>(null);

  const scrollY = new Animated.Value(0);

  if (!event) {
    // This is when event has not been loaded in list, so we have absolutely no info
    return (
      <View style={styles.page}>
        <View style={styles.centeredPage}>
          <AnimatingHeader
            hideBack={dual}
            value={scrollY}
            title={Platform.OS === 'ios' ? '' : 'Évènement'}
            subtitle={Platform.OS === 'ios' ? 'Évènements' : ''}
          />
          {reqState.events.info.error && (
            <ErrorMessage
              type="axios"
              strings={{
                what: 'la récupération de cet évènement',
                contentSingular: "L'évènement",
              }}
              error={reqState.events.info.error}
              retry={fetch}
            />
          )}
          {reqState.events.info.loading && (
            <View style={styles.container}>
              <ActivityIndicator size="large" color={colors.primary} />
            </View>
          )}
        </View>
      </View>
    );
  }

  const { title } = event;
  const subtitle = verification ? 'Évènement · Modération' : 'Évènement';

  navigation.setOptions({ title });

  const headerActions = getHeaderActions(
    id,
    event,
    account,
    navigation,
    setEventReportModalVisible,
  );

  return (
    <View style={styles.page}>
      <AnimatingHeader
        hideBack={dual}
        value={scrollY}
        title={Platform.OS === 'ios' ? '' : title}
        subtitle={Platform.OS === 'ios' ? 'Évènements' : subtitle}
        actions={headerActions.actions}
        overflow={headerActions.overflow}
      >
        {reqState.events.info.error && (
          <ErrorMessage
            type="axios"
            strings={{
              what: 'la récupération de cet évènement',
              contentSingular: "L'évènement",
            }}
            error={reqState.events.info.error}
            retry={() => fetchEvent(id)}
          />
        )}
      </AnimatingHeader>
      <View style={styles.centeredPage}>
        <Animated.ScrollView
          ref={scrollViewRef}
          onScroll={Animated.event([{ nativeEvent: { contentOffset: { y: scrollY } } }], {
            useNativeDriver: true,
          })}
        >
          <View>
            {event.image?.image ? (
              <ContentImage navigation={navigation} image={event.image} />
            ) : null}
            <View style={styles.contentContainer}>
              <Title style={styles.title}>{event.title}</Title>
              <Text style={styles.subtitle}>
                {event.duration?.start && event?.duration?.end
                  ? `Du ${moment(event.duration?.start).format('DD/MM/YYYY')} au ${moment(
                      event.duration?.end,
                    ).format('DD/MM/YYYY')}`
                  : 'Aucune date spécifiée'}{' '}
                · <Icon name="eye" color={colors.subtext} size={12} />{' '}
                {typeof event.cache?.views === 'number' ? event.cache.views : '?'} ·{' '}
                <Icon name="thumb-up" color={colors.subtext} size={12} />{' '}
                {typeof event.cache?.likes === 'number' ? event.cache.likes : '?'}
              </Text>
            </View>
            <TagList item={event} scrollable />
            {(reqState.events.info.loading ||
              reqState.events.delete?.loading ||
              reqState.events.verification_deverify?.loading) && (
              <ActivityIndicator size="large" color={colors.primary} />
            )}
            {!event.preload && reqState.events.info.success && (
              <View>
                <CustomTabView
                  scrollEnabled={false}
                  pages={[
                    {
                      key: 'description',
                      title: 'Description',
                      component: (
                        <EventDisplayDescription
                          id={id}
                          event={event}
                          navigation={navigation}
                          setCommentModalVisible={setCommentModalVisible}
                          setMessageModalVisible={setMessageModalVisible}
                          setReplyingToComment={setReplyingToComment}
                          setFocusedComment={setFocusedComment}
                          verification={verification}
                          commentsDisplayed={commentsDisplayed}
                          setCommentReportModalVisible={setCommentReportModalVisible}
                        />
                      ),
                    },
                    ...(Array.isArray(event.program) && event.program.length
                      ? [
                          {
                            key: 'program',
                            title: 'Programme',
                            component: <EventDisplayProgram event={event} />,
                            onVisible: () => scrollViewRef.current?.scrollToEnd({ animated: true }),
                          },
                        ]
                      : []),
                    ...(event.contact?.phone ||
                    event.contact?.email ||
                    event.members?.length ||
                    event.contact?.other?.length
                      ? [
                          {
                            key: 'contact',
                            title: 'Contact',
                            component: (
                              <EventDisplayContact event={event} navigation={navigation} />
                            ),
                          },
                        ]
                      : []),
                  ]}
                />
                {verification ? (
                  <ContentVerification
                    navigation={navigation}
                    type="event"
                    id={event._id}
                    content={event.description}
                    verification={(event as EventVerification).verification}
                    setReportModalVisible={setEventReportModalVisible}
                  />
                ) : null}
              </View>
            )}
          </View>
        </Animated.ScrollView>
        <AddCommentModal
          visible={isCommentModalVisible}
          setVisible={setCommentModalVisible}
          replyingToComment={replyingToComment}
          setReplyingToComment={setReplyingToComment}
          id={id}
          group={event?.group?._id}
          reqState={reqState}
          add={(
            publisher: { type: 'user' | 'group'; user?: string | null; group?: string | null },
            content: Content,
            parent: string,
            isReplying: boolean,
          ) =>
            commentAdd(publisher, content, parent, isReplying ? 'comment' : 'event').then(() => {
              updateComments('initial', { parentId: id });
              if (account.loggedIn) {
                fetchEventMy(id);
              }
            })
          }
        />
        <AddMessageModal
          visible={isMessageModalVisible}
          setVisible={setMessageModalVisible}
          id={id}
          state={reqState.events}
          defaultGroup={event?.group?._id}
          key={event?.group?._id || 'unk'}
          add={(group: string, content: Content, type: 'high' | 'medium' | 'low') =>
            eventMessagesAdd(id, group, content, type).then(fetch)
          }
        />
        <ReportModal
          visible={isEventReportModalVisible}
          setVisible={setEventReportModalVisible}
          contentId={id}
          report={eventReport}
          state={reqState.events.report}
          navigation={navigation}
        />
        <ReportModal
          visible={isCommentReportModalVisible}
          setVisible={setCommentReportModalVisible}
          contentId={focusedComment || ''}
          report={commentReport}
          state={reqState.comments.report}
          navigation={navigation}
        />
      </View>
    </View>
  );
};

const mapStateToProps = (state: State) => {
  const { events, eventData, comments, account, preferences } = state;
  return {
    dataUpcoming: events.dataUpcoming,
    dataPassed: events.dataPassed,
    search: events.search,
    item: events.item,
    reqState: { events: events.state, comments: comments.state },
    preferences,
    account,
  };
};

export default connect(mapStateToProps)(EventDisplay);
