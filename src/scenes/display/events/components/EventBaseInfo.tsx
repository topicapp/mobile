import moment from 'moment';
import React from 'react';
import { View, Clipboard } from 'react-native';
import { Text, useTheme } from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import shortid from 'shortid';

import { Avatar, Content, InlineCard, PlatformTouchable } from '@components';
import { EventMessage, EventPlace, Duration } from '@ts/types';
import { Format, handleUrl } from '@utils';

import getStyles from '../styles';

type EventBaseInfoProps = {
  places: EventPlace[];
  duration: Duration;
};

const EventBaseInfo: React.FC<EventBaseInfoProps> = ({ places, duration }) => {
  const theme = useTheme();
  const styles = getStyles(theme);
  const { colors } = theme;

  const getPlaceLabels = (place: EventPlace) => {
    switch (place.type) {
      case 'standalone': {
        const { address } = place;
        if (!address?.address) {
          return { title: '', description: '' };
        }
        const { number, street, extra, city } = address.address;

        let title = '';
        if (address.shortName) {
          title = address.shortName;
        } else {
          if (number) {
            title += `${number}, `;
          }
          if (street) {
            title += `${street} `;
          }
          if (extra) {
            title += `(${extra})`;
          }
        }

        return {
          title,
          description: city,
        };
      }
      case 'school': {
        const { associatedSchool } = place;
        if (!associatedSchool?.address?.address) {
          return { title: '', description: '' };
        }
        return {
          title: associatedSchool.name,
          description: Format.address(associatedSchool.address),
        };
      }
      case 'place': {
        const { associatedPlace } = place;
        if (!associatedPlace?.address?.address) {
          return { title: '', description: '' };
        }
        const { number, street, extra, city } = associatedPlace.address.address;
        return {
          title: associatedPlace.displayName,
          description: Format.address(associatedPlace.address),
        };
      }
      case 'online':
        return {
          title: 'Évènement en ligne',
          description: place.link
            ?.replace('http://', '')
            ?.replace('https://', '')
            ?.split(/[/?#]/)?.[0],
          icon: 'link',
          onPress: () => handleUrl(place.link),
          onCopy: () => Clipboard.setString(place.link),
        };
      default:
        return {
          title: 'Inconnu',
          description: 'Endroit non spécifié',
        };
    }
  };

  // Note: using optional chaining is very risky with moment, if a property is undefined the whole
  // equality becomes undefined and moment then refers to current time, which is not at all what we want
  let dateString = null;
  let timeString = null;
  if (duration?.start && duration?.end) {
    dateString = moment(duration.start).isSame(duration.end, 'day')
      ? `Le ${moment(duration.start).format('DD/MM/YYYY')}`
      : `Du ${moment(duration.start).format('DD/MM/YYYY')} au ${moment(duration.end).format(
          'DD/MM/YYYY',
        )}`;
    timeString = `De ${moment(duration.start).hour()}h à ${moment(duration.end).hour()}h`;
  } else {
    dateString = 'Aucune date spécifiée';
    timeString = null;
  }

  return (
    <View>
      {Array.isArray(places) &&
        places.map((place) => {
          const {
            title,
            description,
            icon = 'map-marker',
            onPress,
            onCopy,
          } = getPlaceLabels(place);
          return (
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}
            >
              <View style={{ flex: 1 }}>
                <InlineCard
                  key={shortid()}
                  icon={icon}
                  title={title}
                  subtitle={description}
                  onPress={onPress}
                />
              </View>
              {onCopy && (
                <View style={{ alignSelf: 'center', marginRight: 20 }}>
                  <PlatformTouchable onPress={onCopy}>
                    <Icon name="content-copy" size={24} color={colors.text} />
                  </PlatformTouchable>
                </View>
              )}
            </View>
          );
        })}
      <InlineCard icon="calendar" title={dateString} subtitle={timeString} />
    </View>
  );
};

export default EventBaseInfo;
