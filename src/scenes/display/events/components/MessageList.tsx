import moment from 'moment';
import React from 'react';
import { Platform, View } from 'react-native';
import { Button, Text, useTheme } from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';

import { Avatar, CategoryTitle, Content, PlatformTouchable } from '@components';
import { Account, EventMessage, Group, GroupPreload, State } from '@ts/types';
import { checkPermission, Format, Permissions } from '@utils';

import getStyles from '../styles';
import MessageInlineCard from './Message';

type MessageListProps = {
  messages?: EventMessage[];
  group?: Group | GroupPreload;
  setMessageModalVisible: (val: boolean) => void;
  account: Account;
};

const MessageList: React.FC<MessageListProps> = ({
  account,
  messages,
  group,
  setMessageModalVisible,
}) => {
  const theme = useTheme();
  const styles = getStyles(theme);
  const { colors } = theme;

  const [messagesShown, setMessagesShown] = React.useState(false);

  if (!messages || !group || messages.length <= 0) {
    return null;
  }

  return (
    <View>
      <View>
        <View style={styles.container}>
          <CategoryTitle>Messages</CategoryTitle>
        </View>
        {(messagesShown ? messages : messages.slice(0, 3)).map((m) => (
          <View key={m._id}>
            <MessageInlineCard message={m} isPublisher={m.group?._id === group._id} />
          </View>
        ))}
        {messages.length > 3 && (
          <View>
            <PlatformTouchable onPress={() => setMessagesShown(!messagesShown)}>
              <View style={{ flexDirection: 'row', margin: 10, alignSelf: 'center' }}>
                <Text style={{ color: colors.disabled, alignSelf: 'center' }}>
                  Voir {messagesShown ? 'moins' : 'plus'} de messages
                </Text>
                <Icon
                  name={messagesShown ? 'chevron-up' : 'chevron-down'}
                  color={colors.disabled}
                  size={23}
                />
              </View>
            </PlatformTouchable>
          </View>
        )}
      </View>
      {checkPermission(account, {
        permission: Permissions.EVENT_MODIFY,
        scope: { groups: [group._id] },
      }) && (
        <View style={styles.container}>
          <Button
            icon="message-processing"
            mode={Platform.OS === 'ios' ? 'text' : 'outlined'}
            uppercase={Platform.OS !== 'ios'}
            onPress={() => setMessageModalVisible(true)}
          >
            Envoyer un message
          </Button>
        </View>
      )}
    </View>
  );
};

const mapStateToProps = (state: State) => {
  const { account } = state;
  return { account };
};
export default connect(mapStateToProps)(MessageList);
