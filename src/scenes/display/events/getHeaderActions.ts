import { articleDelete, articleDeverify } from '@redux/actions/apiActions/articles';
import { eventDelete, eventDeverify } from '@redux/actions/apiActions/events';
import { updateEventCreationData } from '@redux/actions/contentData/events';
import { Account, Event, EventCreationDataPlace, EventPreload } from '@ts/types';
import { Alert, checkPermission, Errors, Permissions, shareContent, trackEvent } from '@utils';

import { EventDisplayScreenNavigationProp } from './index';

type Navigation = EventDisplayScreenNavigationProp<'Display'>;

const getHeaderActions = (
  id: string,
  event: Event | EventPreload,
  account: Account,
  navigation: Navigation,
  setReportModalVisible: (val: boolean) => void,
) => {
  const hasPermissionInGroup = (permission: string) => {
    return checkPermission(account, {
      permission,
      scope: { groups: [event!.group._id] },
    });
  };

  const deleteEvent = () =>
    eventDelete(id)
      .then(() => {
        navigation.goBack();
        Alert.alert(
          'Évènement supprimé',
          "Vous pouvez contacter l'équipe Topic au plus tard après deux semaines pour éviter la suppression définitive.",
          [{ text: 'Fermer' }],
          {
            cancelable: true,
          },
        );
      })
      .catch((error) =>
        Errors.showPopup({
          type: 'axios',
          what: "la suppression de l'évènement",
          error,
          retry: deleteEvent,
        }),
      );

  const deverifyEvent = () =>
    eventDeverify(id)
      .then(() => {
        navigation.goBack();
        Alert.alert(
          'Évènement remis en modération',
          "Vous pouvez le voir dans l'onglet modération",
          [{ text: 'Fermer' }],
          {
            cancelable: true,
          },
        );
      })
      .catch((error) =>
        Errors.showPopup({
          type: 'axios',
          what: "la dévérification de l'évènement",
          error,
          retry: deverifyEvent,
        }),
      );

  const modifyEvent = () => {
    if (!event || !event.duration || event.preload) return;
    updateEventCreationData({
      editing: true,
      id: event._id,
      title: event.title,
      summary: event.summary,
      data: event.description.data,
      parser: event.description.parser,
      phone: event.contact?.phone,
      email: event.contact?.email,
      contact: event.contact?.other || [],
      members: event.members.map((m) => m._id) || [],
      start: event.duration.start,
      end: event.duration.end,
      location: {
        schools: event.location.schools.map((s) => s._id),
        departments: event.location.departments.map((d) => d._id),
        global: event.location.global,
      },
      group: event.group._id,
      places: event.places
        .map((p) => {
          if (p.type === 'school') {
            return {
              type: 'school',
              associatedSchool: p.associatedSchool._id,
              tempName: p.associatedSchool.displayName || p.associatedSchool.name,
            };
          } else if (p.type === 'place') {
            return {
              type: 'place',
              associatedPlace: p.associatedPlace._id,
              tempName: p.associatedPlace.displayName || p.associatedPlace.name,
            };
          } else if (p.type === 'standalone') {
            return {
              type: 'standalone',
              address: p.address,
            };
          } else if (p.type === 'online') {
            return {
              type: 'online',
              link: p.link,
            };
          }
          return null;
        })
        .filter((p) => !!p) as EventCreationDataPlace[],
      image: event.image,
      tags: event.tags.map((t) => t._id),
      program: event.program,
    });
    navigation.navigate('Main', {
      screen: 'Add',
      params: { screen: 'Event', params: { screen: 'Add' } },
    });
  };

  return {
    actions: [
      {
        icon: 'share-variant',
        onPress: () => {
          if (!event) return;
          shareContent({
            title: event.title,
            group: event.group?.displayName,
            type: 'evenements',
            id: event._id,
          });
          trackEvent('eventdisplay:share', { props: { button: 'header' } });
        },
        label: 'Partager',
      },
    ],
    overflow: [
      {
        title: 'Signaler',
        onPress: () => setReportModalVisible(true),
      },
      ...(hasPermissionInGroup(Permissions.EVENT_MODIFY)
        ? [
            {
              title: 'Modifier',
              onPress: () => {
                trackEvent('eventdisplay:modify', { props: { button: 'header' } });
                modifyEvent();
              },
            },
          ]
        : []),
      ...(hasPermissionInGroup(Permissions.EVENT_DELETE)
        ? [
            {
              title: 'Supprimer',
              onPress: () =>
                Alert.alert(
                  'Supprimer cette évènement ?',
                  'Les autres administrateurs du groupe seront notifiés.',
                  [
                    { text: 'Annuler' },
                    {
                      text: 'Supprimer',
                      onPress: () => {
                        deleteEvent();
                        trackEvent('eventdisplay:delete', { props: { button: 'header' } });
                      },
                    },
                  ],
                  { cancelable: true },
                ),
            },
          ]
        : []),
      ...(hasPermissionInGroup(Permissions.EVENT_VERIFICATION_DEVERIFY)
        ? [
            {
              title: 'Dévérifier',
              onPress: () =>
                Alert.alert(
                  'Remettre cet article en modération ?',
                  'Les autres administrateurs du groupe seront notifiés.',
                  [
                    { text: 'Annuler' },
                    {
                      text: 'Dévérifier',
                      onPress: () => {
                        deverifyEvent();
                        trackEvent('eventdisplay:deverify', { props: { button: 'header' } });
                      },
                    },
                  ],
                  { cancelable: true },
                ),
            },
          ]
        : []),
    ],
  };
};

export default getHeaderActions;
