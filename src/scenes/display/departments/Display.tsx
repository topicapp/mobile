import { RouteProp, useIsFocused } from '@react-navigation/native';
import React from 'react';
import { ScrollView, View, Platform, ActivityIndicator, StatusBar } from 'react-native';
import { Text, Divider, useTheme, IconButton, Title, Subheading } from 'react-native-paper';
import { SafeAreaView } from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';

import {
  PlatformBackButton,
  TranslucentStatusBar,
  Content,
  ErrorMessage,
  ContentTabView,
  Avatar,
} from '@components';
import { fetchDepartment } from '@redux/actions/api/departments';
import {
  DepartmentPreload,
  Department,
  Account,
  DepartmentRequestState,
  DepartmentsState,
  State,
  Avatar as AvatarType,
  PreferencesState,
} from '@ts/types';
import {
  logger,
  Format,
  checkPermission,
  Alert,
  Errors,
  shareContent,
  trackEvent,
  Permissions,
} from '@utils';

import type { DepartmentDisplayStackParams, DepartmentDisplayScreenNavigationProp } from '.';
import getStyles from './styles';

// import ChangeDepartmentLocationModal from '../components/ChangeDepartmentLocationModal';

type DepartmentDisplayProps = {
  navigation: DepartmentDisplayScreenNavigationProp<'Display'>;
  route: RouteProp<DepartmentDisplayStackParams, 'Display'>;
  departments: DepartmentsState;
  account: Account;
  state: DepartmentRequestState;
  preferences: PreferencesState;
};

const DepartmentDisplay: React.FC<DepartmentDisplayProps> = ({
  route,
  navigation,
  departments,
  account,
  state,
  preferences,
}) => {
  const theme = useTheme();
  const styles = getStyles(theme);
  const { colors } = theme;

  const isFocused = useIsFocused();

  // Pour changer le type de route.params, voir ../index.tsx
  const { id } = route.params;

  const fetch = () => fetchDepartment(id);

  React.useEffect(() => {
    if (isFocused) {
      fetch();
    }
  }, [id, isFocused]);

  const department: Department | DepartmentPreload | null =
    departments.item?._id === id
      ? departments.item
      : departments.data.find((g) => g._id === id) ||
        departments.search.find((g) => g._id === id) ||
        null;

  if (!department) {
    // This is when article has not been loaded in list, so we have absolutely no info
    return (
      <View style={styles.page}>
        <SafeAreaView style={{ flex: 1 }}>
          <PlatformBackButton onPress={navigation.goBack} />

          {state.info.error && (
            <ErrorMessage
              type="axios"
              strings={{
                what: 'la récupération de ce département',
                contentSingular: 'Le département',
              }}
              error={state.info.error}
              retry={fetch}
            />
          )}
          {!state.info.error && (
            <View style={styles.container}>
              <ActivityIndicator size="large" color={colors.primary} />
            </View>
          )}
        </SafeAreaView>
      </View>
    );
  }

  const departmentType =
    department.type === 'departement'
      ? 'Département'
      : department.type === 'region'
      ? 'Région'
      : 'Zone';

  navigation.setOptions({ title: department.shortName });

  return (
    <View style={styles.page}>
      <SafeAreaView style={{ flex: 1 }}>
        <TranslucentStatusBar />
        {state.info.error && (
          <ErrorMessage
            type="axios"
            strings={{
              what: 'la récupération de ce département',
              contentSingular: 'Le département',
            }}
            error={state.info.error}
            retry={fetch}
          />
        )}
        <ScrollView>
          <View style={styles.centeredPage}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <PlatformBackButton onPress={navigation.goBack} />
              <View style={{ flexDirection: 'row', marginRight: 10 }}>
                <IconButton
                  key="share"
                  icon="share-variant"
                  onPress={() => {
                    trackEvent('departmentdisplay:share', { props: { button: 'header' } });
                    shareContent({
                      title: `${departmentType} ${department.displayName}`,
                      type: 'departements',
                      id: department._id,
                    });
                  }}
                />
              </View>
            </View>

            <View style={[styles.contentContainer, { marginTop: 20 }]}>
              <View style={[styles.centerIllustrationContainer, { marginBottom: 10 }]}>
                <Avatar size={120} icon="map-outline" imageSize="large" />
              </View>
              <View style={[styles.centerIllustrationContainer, { flexDirection: 'row' }]}>
                <View style={{ alignItems: 'center' }}>
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Title style={{ textAlign: 'center' }}>{department.name}</Title>
                  </View>
                  {!!department.shortName && (
                    <Subheading
                      style={{ textAlign: 'center', marginTop: -10, color: colors.disabled }}
                    >
                      {department.code}
                    </Subheading>
                  )}
                  <Subheading
                    style={{ textAlign: 'center', marginTop: -10, color: colors.disabled }}
                  >
                    {departmentType}
                  </Subheading>
                </View>
              </View>
            </View>
            {state.info.loading && (
              <View style={styles.container}>
                <ActivityIndicator size="large" color={colors.primary} />
              </View>
            )}
            {state.info.success && !department.preload && (
              <View>
                <Divider style={{ marginVertical: 10 }} />
                <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                  <View style={{ alignItems: 'center' }}>
                    <Text style={{ fontSize: 40 }}>
                      {typeof department.cache?.users === 'number' ? department.cache.users : ''}
                    </Text>
                    <Text>Utilisateurs</Text>
                  </View>
                  <View style={{ alignItems: 'center' }}>
                    <Text style={{ fontSize: 40 }}>
                      {typeof department.cache?.groups === 'number' ? department.cache.groups : ''}
                    </Text>
                    <Text>Groupes</Text>
                  </View>
                </View>
                <Divider style={{ marginVertical: 10 }} />
              </View>
            )}
            {!department.preload && (
              <ContentTabView
                showSpinner={state.info.success || false}
                searchParams={{ departments: [department._id] }}
                parentId={department._id}
                types={['articles', 'events', 'groups', 'comments']}
              />
            )}
          </View>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
};

const mapStateToProps = (state: State) => {
  const { departments, account, preferences } = state;
  return {
    departments,
    account,
    state: departments.state,
    preferences,
  };
};

export default connect(mapStateToProps)(DepartmentDisplay);
