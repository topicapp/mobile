import { CompositeNavigationProp } from '@react-navigation/core';
import React from 'react';

import { createNativeStackNavigator, NativeStackNavigationProp } from '@utils/compat/stack';

import { DisplayScreenNavigationProp } from '..';
import DepartmentDisplay from './Display';

export type DepartmentDisplayStackParams = {
  Display: { id: string };
};

export type DepartmentDisplayScreenNavigationProp<K extends keyof DepartmentDisplayStackParams> =
  CompositeNavigationProp<
    NativeStackNavigationProp<DepartmentDisplayStackParams, K>,
    DisplayScreenNavigationProp<'Department'>
  >;

const Stack = createNativeStackNavigator<DepartmentDisplayStackParams>();

function DepartmentDisplayStackNavigator() {
  return (
    <Stack.Navigator initialRouteName="Display" screenOptions={{ headerShown: false }}>
      <Stack.Screen name="Display" component={DepartmentDisplay} options={{ title: 'École' }} />
    </Stack.Navigator>
  );
}

export default DepartmentDisplayStackNavigator;
