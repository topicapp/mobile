import { CompositeNavigationProp } from '@react-navigation/core';
import React from 'react';

import { createNativeStackNavigator, NativeStackNavigationProp } from '@utils/compat/stack';

import { DisplayScreenNavigationProp } from '..';
import SchoolDisplay from './Display';

export type SchoolDisplayStackParams = {
  Display: { id: string };
};

export type SchoolDisplayScreenNavigationProp<K extends keyof SchoolDisplayStackParams> =
  CompositeNavigationProp<
    NativeStackNavigationProp<SchoolDisplayStackParams, K>,
    DisplayScreenNavigationProp<'School'>
  >;

const Stack = createNativeStackNavigator<SchoolDisplayStackParams>();

function SchoolDisplayStackNavigator() {
  return (
    <Stack.Navigator initialRouteName="Display" screenOptions={{ headerShown: false }}>
      <Stack.Screen name="Display" component={SchoolDisplay} options={{ title: 'École' }} />
    </Stack.Navigator>
  );
}

export default SchoolDisplayStackNavigator;
