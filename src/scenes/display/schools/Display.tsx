import { RouteProp, useIsFocused } from '@react-navigation/native';
import React from 'react';
import { ScrollView, View, Platform, ActivityIndicator, StatusBar } from 'react-native';
import { Text, Divider, useTheme, IconButton, Title, Subheading } from 'react-native-paper';
import { SafeAreaView } from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';

import {
  PlatformBackButton,
  TranslucentStatusBar,
  Content,
  ErrorMessage,
  ContentTabView,
  Avatar,
} from '@components';
import { fetchSchool } from '@redux/actions/api/schools';
import {
  SchoolPreload,
  School,
  Account,
  SchoolRequestState,
  SchoolsState,
  State,
  Avatar as AvatarType,
  PreferencesState,
} from '@ts/types';
import {
  logger,
  Format,
  checkPermission,
  Alert,
  Errors,
  shareContent,
  trackEvent,
  Permissions,
} from '@utils';
import { schoolTypes } from '@utils/format/school';

import type { SchoolDisplayStackParams, SchoolDisplayScreenNavigationProp } from '.';
import getStyles from './styles';

// import ChangeSchoolLocationModal from '../components/ChangeSchoolLocationModal';

type SchoolDisplayProps = {
  navigation: SchoolDisplayScreenNavigationProp<'Display'>;
  route: RouteProp<SchoolDisplayStackParams, 'Display'>;
  schools: SchoolsState;
  account: Account;
  state: SchoolRequestState;
  preferences: PreferencesState;
};

const SchoolDisplay: React.FC<SchoolDisplayProps> = ({
  route,
  navigation,
  schools,
  account,
  state,
  preferences,
}) => {
  const theme = useTheme();
  const styles = getStyles(theme);
  const { colors } = theme;

  const isFocused = useIsFocused();

  // Pour changer le type de route.params, voir ../index.tsx
  const { id } = route.params;

  const fetch = () => fetchSchool(id);

  React.useEffect(() => {
    if (isFocused) {
      fetch();
    }
  }, [id, isFocused]);

  const school: School | SchoolPreload | null =
    schools.item?._id === id
      ? schools.item
      : schools.data.find((g) => g._id === id) || schools.search.find((g) => g._id === id) || null;

  if (!school) {
    // This is when article has not been loaded in list, so we have absolutely no info
    return (
      <View style={styles.page}>
        <SafeAreaView style={{ flex: 1 }}>
          <PlatformBackButton onPress={navigation.goBack} />

          {state.info.error && (
            <ErrorMessage
              type="axios"
              strings={{
                what: 'la récupération de cette école',
                contentSingular: "L'école",
              }}
              error={state.info.error}
              retry={fetch}
            />
          )}
          {!state.info.error && (
            <View style={styles.container}>
              <ActivityIndicator size="large" color={colors.primary} />
            </View>
          )}
        </SafeAreaView>
      </View>
    );
  }

  navigation.setOptions({ title: school.shortName });

  return (
    <View style={styles.page}>
      <SafeAreaView style={{ flex: 1 }}>
        <TranslucentStatusBar />
        {state.info.error && (
          <ErrorMessage
            type="axios"
            strings={{
              what: 'la récupération de cette école',
              contentSingular: "L'école",
            }}
            error={state.info.error}
            retry={fetch}
          />
        )}
        <ScrollView>
          <View style={styles.centeredPage}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <PlatformBackButton onPress={navigation.goBack} />
              <View style={{ flexDirection: 'row', marginRight: 10 }}>
                <IconButton
                  key="share"
                  icon="share-variant"
                  onPress={() => {
                    trackEvent('schooldisplay:share', { props: { button: 'header' } });
                    shareContent({
                      title: school.displayName || 'École',
                      type: 'ecoles',
                      id: school._id,
                    });
                  }}
                />
              </View>
            </View>

            <View style={[styles.contentContainer, { marginTop: 20 }]}>
              <View style={[styles.centerIllustrationContainer, { marginBottom: 10 }]}>
                <Avatar
                  size={120}
                  avatar={school.image?.image ? { type: 'image', image: school.image } : undefined}
                  icon="school"
                  imageSize="large"
                />
              </View>
              <View style={[styles.centerIllustrationContainer, { flexDirection: 'row' }]}>
                <View style={{ alignItems: 'center' }}>
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Title style={{ textAlign: 'center' }}>{school.name}</Title>
                  </View>
                  {!!school.shortName && (
                    <Subheading
                      style={{ textAlign: 'center', marginTop: -10, color: colors.disabled }}
                    >
                      {school.shortName}
                    </Subheading>
                  )}
                  <Subheading
                    style={{ textAlign: 'center', marginTop: -10, color: colors.disabled }}
                  >
                    {schoolTypes(school.types)}
                  </Subheading>
                </View>
              </View>
            </View>
            {state.info.loading && (
              <View style={styles.container}>
                <ActivityIndicator size="large" color={colors.primary} />
              </View>
            )}
            {state.info.success && !school.preload && (
              <View>
                <Divider style={{ marginVertical: 10 }} />
                <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                  <View style={{ alignItems: 'center' }}>
                    <Text style={{ fontSize: 40 }}>
                      {typeof school.cache?.users === 'number' ? school.cache.users : ''}
                    </Text>
                    <Text>Utilisateurs</Text>
                  </View>
                  <View style={{ alignItems: 'center' }}>
                    <Text style={{ fontSize: 40 }}>
                      {typeof school.cache?.groups === 'number' ? school.cache.groups : ''}
                    </Text>
                    <Text>Groupes</Text>
                  </View>
                </View>
                <Divider style={{ marginVertical: 10 }} />
              </View>
            )}
            {!school.preload && !!school.description && (
              <View style={[styles.container, { marginTop: 0 }]}>
                <Content parser={school.description?.parser} data={school.description?.data} />
              </View>
            )}
            {!school.preload && (
              <ContentTabView
                showSpinner={state.info.success || false}
                searchParams={{ schools: [school._id] }}
                parentId={school._id}
                types={['articles', 'events', 'groups', 'comments']}
              />
            )}
          </View>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
};

const mapStateToProps = (state: State) => {
  const { schools, account, preferences } = state;
  return {
    schools,
    account,
    state: schools.state,
    preferences,
  };
};

export default connect(mapStateToProps)(SchoolDisplay);
