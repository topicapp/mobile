import { RouteProp } from '@react-navigation/native';
import moment from 'moment';
import React from 'react';
import { View, ActivityIndicator, Platform } from 'react-native';
import { Text, Title, Divider, useTheme } from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';
import shortid from 'shortid';

import { Content, TagList, ContentImage } from '@components';
import {
  Article,
  ArticlePreload,
  CommentRequestState,
  Account,
  ArticleRequestState,
  ArticleMyInfo,
  ArticleVerification,
  GroupRequestState,
  UserRequestState,
} from '@ts/types';

import type { ArticleDisplayScreenNavigationProp, ArticleDisplayStackParams } from '..';
import CommentsHeader from '../../components/CommentsHeader';
import ContentLikeShare from '../../components/ContentLikeShare';
import ContentVerification from '../../components/ContentVerification';
import DoubleTapLike from '../../components/DoubleTapLike';
import PublisherInfo from '../../components/PublisherInfo';
import getStyles from '../styles';

// Common types
type Navigation = ArticleDisplayScreenNavigationProp<'Display'>;
type Route = RouteProp<ArticleDisplayStackParams, 'Display'>;

type CombinedReqState = {
  articles: ArticleRequestState;
  comments: CommentRequestState;
  groups: GroupRequestState;
  users: UserRequestState;
};

type ArticleDisplayHeaderProps = {
  article: Article | ArticlePreload;
  navigation: Navigation;
  reqState: CombinedReqState;
  account: Account;
  commentsDisplayed: boolean;
  verification: boolean;
  setReplyingToComment: (id: string | null) => any;
  setCommentModalVisible: (visible: boolean) => void;
  setArticleReportModalVisible: (visible: boolean) => void;
  articleMy: ArticleMyInfo | null;
};

const ArticleDisplayHeader: React.FC<ArticleDisplayHeaderProps> = ({
  article,
  articleMy,
  navigation,
  reqState,
  account,
  commentsDisplayed,
  setCommentModalVisible,
  setArticleReportModalVisible,
  setReplyingToComment,
  verification,
}) => {
  const theme = useTheme();
  const styles = getStyles(theme);
  const { colors } = theme;

  const loading =
    reqState.articles.info.loading ||
    reqState.articles.delete?.loading ||
    reqState.articles.verification_deverify?.loading;
  const loadSuccess = !article.preload && reqState.articles.info.success && article.content;

  return (
    <View style={styles.page}>
      {article.image?.image ? <ContentImage image={article.image} navigation={navigation} /> : null}
      <View style={styles.contentContainer}>
        <Title style={styles.title}>{article.title}</Title>
        <Text style={styles.subtitle}>
          Le {moment(article.date).format('LL')} à {moment(article.date).format('LT')} ·{' '}
          <Icon name="eye" color={colors.subtext} size={12} /> {article.cache?.views ?? '?'} ·{' '}
          <Icon name="thumb-up" color={colors.subtext} size={12} /> {article.cache?.likes ?? '?'}
        </Text>
      </View>
      <TagList item={article} scrollable />
      {loading ? <ActivityIndicator size="large" color={colors.primary} /> : null}
      {loadSuccess ? (
        <View>
          <DoubleTapLike type="article" enabled={Platform.OS !== 'web'} id={article._id}>
            <View style={styles.contentContainer}>
              <Content data={article.content?.data} parser={article.content?.parser} />
            </View>
          </DoubleTapLike>
          <ContentLikeShare
            navigation={navigation}
            type="article"
            id={article._id}
            group={article.group}
            title={article.title}
            contentMy={articleMy}
          />
          <Divider />
          <PublisherInfo navigation={navigation} authors={article.authors} group={article.group} />
          {!verification && commentsDisplayed ? (
            <CommentsHeader
              navigation={navigation}
              type="article"
              id={article._id}
              setReplyingToComment={setReplyingToComment}
              setCommentModalVisible={setCommentModalVisible}
            />
          ) : null}
          {verification ? (
            <ContentVerification
              navigation={navigation}
              id={article._id}
              type="article"
              content={article.content}
              verification={(article as ArticleVerification).verification}
              setReportModalVisible={setArticleReportModalVisible}
            />
          ) : null}
        </View>
      ) : null}
    </View>
  );
};

export default ArticleDisplayHeader;
