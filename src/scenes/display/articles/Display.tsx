import { RouteProp, useIsFocused } from '@react-navigation/native';
import React from 'react';
import { View, ActivityIndicator, FlatList, Platform } from 'react-native';
import { Text, Divider, useTheme } from 'react-native-paper';
import { connect } from 'react-redux';

import {
  Illustration,
  ReportModal,
  PageContainer,
  CommentInlineCard,
  AddCommentModal,
} from '@components';
import {
  fetchArticle,
  fetchArticleMy,
  fetchArticleVerification,
} from '@redux/actions/api/articles';
import { updateComments } from '@redux/actions/api/comments';
import { articleReport } from '@redux/actions/apiActions/articles';
import { commentAdd, commentReport } from '@redux/actions/apiActions/comments';
import { addArticleRead } from '@redux/actions/contentData/articles';
import {
  Article,
  ArticlePreload,
  State,
  CommentRequestState,
  Account,
  ArticleRequestState,
  Comment,
  Preferences,
  Content as ContentType,
  ArticleMyInfo,
  GroupRequestState,
  UserRequestState,
} from '@ts/types';

import type { ArticleDisplayScreenNavigationProp, ArticleDisplayStackParams } from '.';
import ArticleDisplayHeader from './components/DisplayHeader';
import getHeaderActions from './getHeaderActions';
import getStyles from './styles';

// Common types
type Navigation = ArticleDisplayScreenNavigationProp<'Display'>;
type Route = RouteProp<ArticleDisplayStackParams, 'Display'>;

type CombinedReqState = {
  articles: ArticleRequestState;
  comments: CommentRequestState;
  groups: GroupRequestState;
  users: UserRequestState;
};

type ArticleDisplayProps = {
  route: Route;
  navigation: Navigation;
  item: Article | null;
  my: ArticleMyInfo | null;
  data: ArticlePreload[];
  search: ArticlePreload[];
  comments: Comment[];
  reqState: CombinedReqState;
  account: Account;
  preferences: Preferences;
  dual?: boolean;
};

const ArticleDisplay: React.FC<ArticleDisplayProps> = ({
  route,
  navigation,
  data,
  search,
  item,
  my,
  comments,
  reqState,
  account,
  preferences,
  dual = false,
}) => {
  // Pour changer le type de route.params, voir ../index.tsx
  const { id, verification = false } = route.params;

  const [commentsDisplayed, setCommentsDisplayed] = React.useState(false);

  const theme = useTheme();
  const styles = getStyles(theme);
  const { colors } = theme;

  const isFocused = useIsFocused();

  const article: Article | ArticlePreload | undefined | null =
    item?._id === id
      ? item
      : data.find((a) => a._id === id) || search.find((a) => a._id === id) || null;
  const articleMy: ArticleMyInfo | null = my?._id === id ? my : null;

  const articleComments = comments.filter(
    (c) =>
      c.parent === id &&
      (c.publisher?.type !== 'user' || c.publisher?.user?._id !== account.accountInfo?.accountId),
  );

  const fetch = () => {
    if (verification) {
      fetchArticleVerification(id).then(() => setCommentsDisplayed(true));
    } else {
      fetchArticle(id).then(() => {
        if (preferences.history) {
          addArticleRead(id, article?.title || 'Article inconnu');
        }
        setCommentsDisplayed(true);
      });
      if (account.loggedIn) {
        fetchArticleMy(id);
      }
    }
  };

  React.useEffect(() => {
    if (isFocused) {
      fetch();
      updateComments('initial', { parentId: id });
    }
  }, [id, isFocused]);

  const [isCommentModalVisible, setCommentModalVisible] = React.useState(false);
  const [isArticleReportModalVisible, setArticleReportModalVisible] = React.useState(false);

  const [isCommentReportModalVisible, setCommentReportModalVisible] = React.useState(false);
  const [focusedComment, setFocusedComment] = React.useState<string | null>(null);

  const [replyingToComment, setReplyingToComment] = React.useState<string | null>(null);
  const replyingToPublisher = articleComments.find((c) => c._id === replyingToComment)?.publisher;

  if (!article) {
    // This is when article has not been loaded in list, so we have absolutely no info
    return (
      <PageContainer
        centered
        headerOptions={
          Platform.OS === 'ios' ? { subtitle: 'Actus', title: '' } : { title: 'Actus' }
        }
        loading={reqState.articles.info.loading}
        showError={reqState.articles.info.error}
        errorOptions={{
          type: 'axios',
          strings: {
            what: 'la récupération de cet article',
            contentSingular: "L'article",
          },
          error: reqState.articles.info.error,
          retry: fetch,
        }}
      />
    );
  }

  const { title } = article;
  const subtitle = verification ? 'Actus · Modération' : 'Actus';

  const headerActions = getHeaderActions(
    id,
    article,
    account,
    navigation,
    setArticleReportModalVisible,
  );

  navigation.setOptions({ title });

  return (
    <PageContainer
      centered
      headerOptions={{
        hideBack: dual,
        title: Platform.OS === 'ios' ? '' : title,
        subtitle,
        actions: headerActions.actions,
        overflow: headerActions.overflow,
      }}
      showError={reqState.articles.info.error}
      errorOptions={{
        type: 'axios',
        strings: {
          what: 'la récupération de cet article',
          contentSingular: "L'article",
        },
        error: reqState.articles.info.error,
        retry: fetch,
      }}
    >
      <FlatList
        ListHeaderComponent={() =>
          article ? (
            <ArticleDisplayHeader
              article={article}
              commentsDisplayed={commentsDisplayed}
              verification={verification}
              reqState={reqState}
              account={account}
              navigation={navigation}
              setReplyingToComment={setReplyingToComment}
              setCommentModalVisible={setCommentModalVisible}
              setArticleReportModalVisible={setArticleReportModalVisible}
              articleMy={my}
            />
          ) : null
        }
        data={
          commentsDisplayed && reqState.articles.info.success && !verification
            ? [...(articleMy?.comments || []), ...articleComments]
            : []
        }
        // onEndReached={() => {
        //   console.log('comment end reached');
        //   updateComments('next', { parentId: id });
        // }}
        // onEndReachedThreshold={0.5}
        keyExtractor={(comment: Comment) => comment._id}
        ItemSeparatorComponent={Divider}
        ListFooterComponent={
          reqState.articles.info.success ? (
            <View>
              <Divider />
              <View style={[styles.container, { height: 50 }]}>
                {reqState.comments.list.loading.next ?? (
                  <ActivityIndicator size="large" color={colors.primary} />
                )}
              </View>
            </View>
          ) : undefined
        }
        ListEmptyComponent={() =>
          reqState.comments.list.success &&
          reqState.articles.info.success &&
          !verification &&
          commentsDisplayed ? (
            <View style={styles.contentContainer}>
              <View style={styles.centerIllustrationContainer}>
                <Illustration name="comment-empty" height={200} width={200} />
                <Text>Aucun commentaire</Text>
              </View>
            </View>
          ) : null
        }
        renderItem={({ item: comment }: { item: Comment }) => (
          <CommentInlineCard
            comment={comment}
            report={(commentId) => {
              setFocusedComment(commentId);
              setCommentReportModalVisible(true);
            }}
            fetch={() => updateComments('initial', { parentId: id })}
            isReply={false}
            loggedIn={account.loggedIn}
            navigation={navigation}
            reply={(id: string | null) => {
              setReplyingToComment(id);
              setCommentModalVisible(true);
            }}
            authors={[...(article?.authors?.map((a) => a._id) || []), article?.group?._id || '']}
          />
        )}
      />
      <AddCommentModal
        visible={isCommentModalVisible}
        setVisible={setCommentModalVisible}
        replyingToComment={replyingToComment}
        setReplyingToComment={setReplyingToComment}
        replyingToUsername={
          replyingToPublisher?.type === 'group'
            ? replyingToPublisher?.group?.shortName || replyingToPublisher?.group?.name
            : replyingToPublisher?.user?.info?.username
        }
        id={id}
        group={article?.group?._id}
        reqState={reqState}
        add={(
          publisher: { type: 'user' | 'group'; user?: string | null; group?: string | null },
          content: ContentType,
          parent: string,
          isReplying: boolean = false,
        ) =>
          commentAdd(publisher, content, parent, isReplying ? 'comment' : 'article').then(() => {
            updateComments('initial', { parentId: id });
            if (account.loggedIn) {
              fetchArticleMy(id);
            }
          })
        }
      />
      <ReportModal
        visible={isArticleReportModalVisible}
        setVisible={setArticleReportModalVisible}
        contentId={id}
        report={articleReport}
        state={reqState.articles.report}
        navigation={navigation}
      />
      <ReportModal
        visible={isCommentReportModalVisible}
        setVisible={setCommentReportModalVisible}
        contentId={focusedComment || ''}
        report={commentReport}
        state={reqState.comments.report}
        navigation={navigation}
      />
    </PageContainer>
  );
};

const mapStateToProps = (state: State) => {
  const { articles, comments, account, preferences, users, groups } = state;
  return {
    data: articles.data,
    search: articles.search,
    item: articles.item,
    my: articles.my,
    comments: comments.data,
    reqState: {
      articles: articles.state,
      comments: comments.state,
      users: users.state,
      groups: groups.state,
    },
    preferences,
    account,
  };
};

export default connect(mapStateToProps)(ArticleDisplay);
