import { articleDelete, articleDeverify } from '@redux/actions/apiActions/articles';
import { updateArticleCreationData } from '@redux/actions/contentData/articles';
import { Account, Article, ArticlePreload } from '@ts/types';
import { Alert, checkPermission, Errors, Permissions, shareContent, trackEvent } from '@utils';

import { ArticleDisplayScreenNavigationProp } from './index';

type Navigation = ArticleDisplayScreenNavigationProp<'Display'>;

const getHeaderActions = (
  id: string,
  article: Article | ArticlePreload,
  account: Account,
  navigation: Navigation,
  setReportModalVisible: (val: boolean) => void,
) => {
  const hasPermissionInGroup = (permission: string) => {
    return checkPermission(account, {
      permission,
      scope: { groups: [article!.group._id] },
    });
  };

  const deleteArticle = () =>
    articleDelete(id)
      .then(() => {
        navigation.goBack();
        Alert.alert(
          'Article supprimé',
          "Vous pouvez contacter l'équipe Topic au plus tard après deux semaines pour éviter la suppression définitive.",
          [{ text: 'Fermer' }],
          {
            cancelable: true,
          },
        );
      })
      .catch((error) =>
        Errors.showPopup({
          type: 'axios',
          what: "la suppression de l'article",
          error,
          retry: deleteArticle,
        }),
      );

  const modifyArticle = () => {
    if (!article || article.preload) return;
    updateArticleCreationData({
      editing: true,
      id: article._id,
      group: article.group._id,
      location: {
        schools: article.location.schools.map((s) => s._id),
        departments: article.location.departments.map((d) => d._id),
        global: article.location.global,
      },
      title: article.title,
      summary: article.summary,
      image: article.image,
      opinion: article.opinion,
      tags: article.tags.map((t) => t._id),
      tagData: article.tags,
      parser: article.content?.parser,
      data: article.content?.data,
    });
    navigation.navigate('Main', {
      screen: 'Add',
      params: { screen: 'Article', params: { screen: 'Add' } },
    });
  };

  const deverifyArticle = () =>
    articleDeverify(id)
      .then(() => {
        navigation.goBack();
        Alert.alert(
          'Article remis en modération',
          "Vous pouvez le voir dans l'onglet modération",
          [{ text: 'Fermer' }],
          {
            cancelable: true,
          },
        );
      })
      .catch((error) =>
        Errors.showPopup({
          type: 'axios',
          what: "la dévérification de l'article",
          error,
          retry: deverifyArticle,
        }),
      );

  return {
    actions: [
      {
        icon: 'share-variant',
        onPress: () => {
          if (!article) return;
          shareContent({
            title: article.title,
            group: article.group?.displayName,
            type: 'articles',
            id: article._id,
          });
          trackEvent('articledisplay:share', { props: { button: 'header' } });
        },
        label: 'Partager',
      },
    ],
    overflow: [
      {
        title: 'Signaler',
        onPress: () => {
          setReportModalVisible(true);
        },
      },
      ...(hasPermissionInGroup(Permissions.ARTICLE_MODIFY)
        ? [
            {
              title: 'Modifier',
              onPress: () => {
                trackEvent('articledisplay:modify', { props: { button: 'header' } });
                modifyArticle();
              },
            },
          ]
        : []),
      ...(hasPermissionInGroup(Permissions.ARTICLE_DELETE)
        ? [
            {
              title: 'Supprimer',
              onPress: () =>
                Alert.alert(
                  'Supprimer cette article ?',
                  'Les autres administrateurs du groupe seront notifiés.',
                  [
                    { text: 'Annuler' },
                    {
                      text: 'Supprimer',
                      onPress: () => {
                        deleteArticle();
                        trackEvent('articledisplay:delete', { props: { button: 'header' } });
                      },
                    },
                  ],
                  { cancelable: true },
                ),
            },
          ]
        : []),
      ...(hasPermissionInGroup(Permissions.ARTICLE_VERIFICATION_DEVERIFY)
        ? [
            {
              title: 'Dévérifier',
              onPress: () =>
                Alert.alert(
                  'Remettre cet article en modération ?',
                  'Les autres administrateurs du groupe seront notifiés.',
                  [
                    { text: 'Annuler' },
                    {
                      text: 'Dévérifier',
                      onPress: () => {
                        deverifyArticle();
                        trackEvent('articledisplay:deverify', { props: { button: 'header' } });
                      },
                    },
                  ],
                  { cancelable: true },
                ),
            },
          ]
        : []),
    ],
  };
};

export default getHeaderActions;
