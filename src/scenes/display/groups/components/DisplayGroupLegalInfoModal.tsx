import { Formik } from 'formik';
import React from 'react';
import { View, Platform, ActivityIndicator, ScrollView, Clipboard } from 'react-native';
import {
  Divider,
  Button,
  Title,
  Text,
  useTheme,
  Card,
  List,
  IconButton,
  Subheading,
} from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';
import * as Yup from 'yup';

import { Modal } from '@components';
import { ModalProps, State, Group, GroupRequestState, GroupPreload, Account } from '@ts/types';
import { checkPermission, Permissions } from '@utils';

import getStyles from '../styles';

type DisplayGroupLegalInfoModalProps = ModalProps & {
  group: Group | GroupPreload | null;
  account: Account;
  setEditGroupLegalModalVisible: (val: boolean) => void;
};

const DisplayGroupLegalInfoModal: React.FC<DisplayGroupLegalInfoModalProps> = ({
  visible,
  setVisible,
  group,
  account,
  setEditGroupLegalModalVisible,
}) => {
  const theme = useTheme();
  const styles = getStyles(theme);
  const { colors } = theme;

  if (!group || group.preload) return null;

  return (
    <Modal visible={visible} setVisible={setVisible}>
      <View style={[styles.centerIllustrationContainer, { marginVertical: 20 }]}>
        <Title style={{ textAlign: 'center' }}>Infos légales</Title>
        <Title style={{ textAlign: 'center', color: colors.muted }}>{group.name}</Title>
      </View>
      <View style={styles.contentContainer}>
        <Subheading>Nom complet</Subheading>
        <Text>{group.legal?.name || 'Non spécifié'}</Text>
        <Divider style={{ marginVertical: 20 }} />
        <Subheading>Identifiant</Subheading>
        <Text>{group.legal?.id || 'Non spécifié'}</Text>
        <Divider style={{ marginVertical: 20 }} />
        <Subheading>Responsable légal</Subheading>
        <Text>{group.legal?.admin}</Text>
        <Divider style={{ marginVertical: 20 }} />
        <Subheading>Siège social</Subheading>
        <Text>{group.legal?.address || 'Non spécifié'}</Text>
        <Divider style={{ marginVertical: 20 }} />
        <Subheading>Adresse email</Subheading>
        <Text>{group.legal?.email}</Text>
        <Divider style={{ marginVertical: 20 }} />
        {group.legal?.extra ? (
          <View>
            <Subheading>Données supplémentaires</Subheading>
            <Text>{group.legal?.extra}</Text>
            <Divider style={{ marginVertical: 20 }} />
          </View>
        ) : null}
      </View>
      {checkPermission(account, {
        permission: Permissions.GROUP_MODIFY,
        scope: { groups: [group._id] },
      }) ? (
        <View style={styles.container}>
          <Button
            mode="outlined"
            onPress={() => {
              setVisible(false);
              setEditGroupLegalModalVisible(true);
            }}
          >
            Modifier
          </Button>
        </View>
      ) : null}
    </Modal>
  );
};

const mapStateToProps = (state: State) => {
  const { account } = state;
  return {
    account,
  };
};

export default connect(mapStateToProps)(DisplayGroupLegalInfoModal);
