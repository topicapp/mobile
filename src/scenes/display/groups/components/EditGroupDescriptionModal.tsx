import { Formik } from 'formik';
import React from 'react';
import { View, Platform, ActivityIndicator, ScrollView, Clipboard } from 'react-native';
import { Divider, Button, Title, Text, useTheme, Card, List, IconButton } from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';
import * as Yup from 'yup';

import { Modal, Avatar, FileUpload, FormTextInput, ErrorMessage, InlineCard } from '@components';
import { fetchGroup, fetchGroupInviteLink } from '@redux/actions/api/groups';
import { groupModify, groupUpdateLink } from '@redux/actions/apiActions/groups';
import ContactAddModal from '@src/scenes/add/groups/components/ContactAddModal';
import {
  ModalProps,
  State,
  Group,
  GroupRequestState,
  GroupPreload,
  Avatar as AvatarType,
  UploadRequestState,
  GroupInvite,
  Account,
} from '@ts/types';
import {
  Errors,
  Alert,
  trackEvent,
  shareContent,
  shareMessage,
  checkPermission,
  Permissions,
} from '@utils';

import getStyles from '../styles';

type EditGroupDescriptionModalProps = ModalProps & {
  group: Group | GroupPreload | null;
  editingGroup: {
    id?: string;
    name?: string;
    shortName?: string;
    summary?: string;
    description?: string;
    avatar?: AvatarType;
    contacts?: CustomContactType[];
  } | null;
  state: GroupRequestState;
  uploadState: UploadRequestState;
  itemInvite: GroupInvite | null;
  account: Account;
};
type CustomContactType = {
  _id: string;
  key: string;
  value: string;
  link: string;
};

const EditGroupDescriptionModal: React.FC<EditGroupDescriptionModalProps> = ({
  visible,
  setVisible,
  group,
  editingGroup,
  state,
  uploadState,
  itemInvite,
  account,
}) => {
  const theme = useTheme();
  const styles = getStyles(theme);
  const { colors } = theme;

  const [errorVisible, setErrorVisible] = React.useState(false);
  const [isContactAddModalVisible, setContactAddModalVisible] = React.useState(false);
  const [customContact, setCustomContact] = React.useState<CustomContactType[]>(
    editingGroup?.contacts || [],
  );
  const addCustomContact = (contact: CustomContactType) => {
    setCustomContact([...customContact, contact]);
  };

  React.useEffect(() => {
    if (
      editingGroup?.id &&
      checkPermission(account, {
        permission: Permissions.GROUP_MEMBERS_ADD,
        scope: { groups: [editingGroup.id] },
      })
    ) {
      fetchGroupInviteLink(editingGroup.id);
    }
  }, [group]);

  const submit = ({
    shortName,
    summary,
    description,
    file,
  }: {
    shortName?: string;
    summary: string;
    description?: string;
    file?: string;
  }) => {
    if (group) {
      groupModify(group?._id, {
        ...(shortName ? { shortName } : {}),
        ...(summary ? { summary } : {}),
        ...(description ? { description: { parser: 'markdown', data: description } } : {}),
        avatar: editingGroup?.avatar,
        ...(file
          ? {
              avatar: {
                type: 'image',
                image: {
                  image: file,
                  thumbnails: { small: true, medium: false, large: true },
                },
              },
            }
          : {}),
        ...(customContact.length ? { contacts: customContact } : {}),
      })
        .then(() => {
          fetchGroup(group?._id);
          setVisible(false);
        })
        .catch((error) =>
          Errors.showPopup({
            type: 'axios',
            what: 'la modification du groupe',
            error,
            retry: () => submit({ shortName, summary, description, file }),
          }),
        );
    }
  };

  const GroupSchema = Yup.object().shape({
    shortName: Yup.string()
      .min(2, "L'acronyme doit contenir au moins 2 caractères")
      .max(20, "L'acronyme doit contenir moins de 20 caractères"),
    description: Yup.string(),
    summary: Yup.string()
      .max(200, 'La description courte doit contenir moins de 200 caractères.')
      .required('Description courte requise'),
    file: Yup.mixed(),
  });

  if (!editingGroup) return null;

  return (
    <Modal visible={visible} setVisible={setVisible}>
      <Formik
        initialValues={{
          shortName: editingGroup.shortName || '',
          summary: editingGroup.summary || '',
          description: editingGroup.description,
          file: editingGroup.avatar?.type === 'image' ? editingGroup.avatar.image.image || '' : '',
        }}
        validationSchema={GroupSchema}
        onSubmit={submit}
      >
        {({ handleChange, handleBlur, handleSubmit, values, errors, touched }) => (
          <View>
            <View
              style={[
                styles.centerIllustrationContainer,
                styles.container,
                { marginTop: 30, marginBottom: 30 },
              ]}
            >
              <Title style={{ fontSize: 24 }}>{editingGroup.name}</Title>
            </View>
            <Divider />
            <View style={styles.container}>
              <View
                style={[styles.centerIllustrationContainer, { marginTop: 10, marginBottom: 30 }]}
              >
                <FileUpload
                  file={values.file || null}
                  setFile={(file) => {
                    handleChange('file')(file || '');
                  }}
                  title={undefined}
                  allowDelete={false}
                  group={editingGroup.id || ''}
                  resizeMode="avatar"
                  type="avatar"
                />
              </View>
              <View style={{ marginBottom: 20 }}>
                <FormTextInput
                  label="Acronyme ou abréviation (facultatif)"
                  info="Acronyme ou abréviation reconnaissable, qui sera affichée en priorité sur vos contenus"
                  value={values.shortName}
                  touched={touched.shortName}
                  error={errors.shortName}
                  onChangeText={handleChange('shortName')}
                  onBlur={handleBlur('shortName')}
                  style={styles.textInput}
                />
              </View>
              <View style={{ marginBottom: 20 }}>
                <FormTextInput
                  label="Description courte"
                  info="Décrivez votre groupe en une ou deux phrases"
                  multiline
                  numberOfLines={3}
                  value={values.summary}
                  touched={touched.summary}
                  error={errors.summary}
                  onChangeText={handleChange('summary')}
                  onBlur={handleBlur('summary')}
                  style={styles.textInput}
                />
              </View>
              <View style={{ marginBottom: 20 }}>
                <FormTextInput
                  label="Description longue (facultatif)"
                  info="Cette description sera affichée sur votre page de groupe"
                  multiline
                  numberOfLines={6}
                  value={values.description}
                  touched={touched.description}
                  error={errors.description}
                  onChangeText={handleChange('description')}
                  onBlur={handleBlur('description')}
                  onSubmitEditing={() => handleSubmit()}
                  style={styles.textInput}
                />
              </View>
            </View>
            <View>
              <List.Item
                title="Contacts"
                description="Donnez vos réseaux sociaux, email, téléphone..."
              />

              {customContact?.map((contact) => (
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                  }}
                >
                  <View style={{ flex: 1, marginHorizontal: 5 }}>
                    <InlineCard key={contact._id} title={contact.value} subtitle={contact.key} />
                  </View>
                  <IconButton
                    accessibilityLabel="Supprimer cet élément"
                    icon="delete"
                    size={30}
                    style={{ marginRight: 20 }}
                    onPress={() => {
                      setCustomContact(customContact.filter((s) => s !== contact));
                    }}
                  />
                </View>
              ))}
            </View>
            <View style={[styles.container, { marginBottom: 20 }]}>
              <Button
                mode="outlined"
                uppercase={Platform.OS !== 'ios'}
                onPress={() => {
                  setContactAddModalVisible(true);
                }}
              >
                Ajouter
              </Button>
            </View>
            {editingGroup.id &&
            checkPermission(account, {
              permission: Permissions.GROUP_MEMBERS_ADD,
              scope: { groups: [editingGroup.id || ''] },
            }) ? (
              <View style={[styles.container, { marginTop: 20 }]}>
                {state.invite_get.error && (
                  <ErrorMessage
                    type="axios"
                    strings={{
                      what: "la récupération du lien d'invitation",
                      contentSingular: "Le lien d'invitation",
                    }}
                    error={state.invite_get.error}
                    retry={() => fetchGroupInviteLink(editingGroup.id || '')}
                  />
                )}
                {state.invite_update.error && (
                  <ErrorMessage
                    type="axios"
                    strings={{
                      what: "la mise à jour du lien d'invitation",
                      contentSingular: "Le lien d'invitation",
                    }}
                    error={state.invite_update.error}
                  />
                )}
                <Text style={{ color: colors.disabled, marginBottom: 5, marginLeft: 10 }}>
                  Lien d&apos;invitation
                </Text>
                <Card
                  elevation={0}
                  style={{ borderColor: colors.primary, borderWidth: 1, borderRadius: 5 }}
                >
                  <View style={[styles.container, { flexDirection: 'row' }]}>
                    <ScrollView horizontal>
                      <Text style={{ color: colors.text, fontSize: 20 }} numberOfLines={1}>
                        {!state.invite_get.success || state.invite_update.loading
                          ? 'Chargement...'
                          : itemInvite?.enabled
                          ? itemInvite?.link
                          : 'Désactivé'}
                      </Text>
                    </ScrollView>
                    {state.invite_get.success && itemInvite?.enabled ? (
                      <>
                        <Icon
                          name="content-copy"
                          style={{ alignSelf: 'center', marginLeft: 10 }}
                          size={24}
                          color={colors.text}
                          onPress={() => {
                            Clipboard.setString(itemInvite?.link || '');
                            trackEvent('groupinvite:copylink');
                            Alert.alert(
                              'Lien copié',
                              'Toutes les personnes qui possèdent ce lien pourront rejoindre le groupe',
                              [{ text: 'Fermer' }],
                              { cancelable: true },
                            );
                          }}
                        />
                        <Icon
                          name="share-variant"
                          style={{ alignSelf: 'center', marginLeft: 10 }}
                          size={24}
                          color={colors.text}
                          onPress={() => {
                            Alert.alert(
                              'Partager le lien ?',
                              'Toutes les personnes ayant accès au lien pourront rejoindre le groupe',
                              [
                                { text: 'Annuler' },
                                {
                                  text: 'Partager',
                                  onPress: () => {
                                    trackEvent('groupinvite:share');
                                    shareMessage({
                                      message: `Invitation à rejoindre le groupe ${group?.name} sur Topic`,
                                      url: itemInvite?.link || '',
                                    });
                                  },
                                },
                              ],
                              { cancelable: true },
                            );
                          }}
                        />
                      </>
                    ) : null}
                  </View>
                </Card>
                {itemInvite && (
                  <View style={[styles.container, { flexDirection: 'row' }]}>
                    <Button
                      mode={Platform.OS !== 'ios' ? 'outlined' : 'text'}
                      uppercase={Platform.OS !== 'ios'}
                      loading={state.invite_update.loading || state.invite_get.loading}
                      onPress={() =>
                        groupUpdateLink(editingGroup.id!, {
                          enabled: !itemInvite.enabled,
                        }).then(() => fetchGroupInviteLink(editingGroup.id || ''))
                      }
                      style={{ flex: 1, marginRight: 5 }}
                    >
                      {itemInvite?.enabled ? 'Désactiver' : 'Activer'}
                    </Button>
                    {itemInvite.enabled && (
                      <Button
                        mode={Platform.OS !== 'ios' ? 'outlined' : 'text'}
                        uppercase={Platform.OS !== 'ios'}
                        onPress={() =>
                          groupUpdateLink(editingGroup.id!, {
                            enabled: true,
                            cycle: (itemInvite.cycle || 1) + 1,
                          }).then(() => fetchGroupInviteLink(editingGroup.id || ''))
                        }
                        style={{ flex: 1, marginLeft: 5 }}
                      >
                        Changer
                      </Button>
                    )}
                  </View>
                )}
              </View>
            ) : null}
            <Divider style={{ marginTop: 10 }} />
            <View style={styles.contentContainer}>
              <Button
                mode={Platform.OS === 'ios' ? 'outlined' : 'contained'}
                color={colors.primary}
                loading={state.modify?.loading}
                disabled={
                  uploadState.upload.loading ||
                  uploadState.permission.loading ||
                  uploadState.process.loading
                }
                uppercase={Platform.OS !== 'ios'}
                onPress={() => handleSubmit()}
              >
                Mettre à jour
              </Button>
            </View>
          </View>
        )}
      </Formik>
      <ContactAddModal
        visible={isContactAddModalVisible}
        setVisible={setContactAddModalVisible}
        add={(contact) => {
          addCustomContact(contact as CustomContactType);
        }}
      />
    </Modal>
  );
};

const mapStateToProps = (state: State) => {
  const { groups, upload, account } = state;
  return {
    state: groups.state,
    uploadState: upload.state,
    itemInvite: groups.itemInvite,
    account,
  };
};

export default connect(mapStateToProps)(EditGroupDescriptionModal);
