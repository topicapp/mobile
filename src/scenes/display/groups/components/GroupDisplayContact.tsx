import React from 'react';
import { View, ActivityIndicator } from 'react-native';
import { useTheme } from 'react-native-paper';
import shortid from 'shortid';

import { InlineCard } from '@components/Cards';
import { Group, GroupPreload } from '@ts/types';
import { handleUrl } from '@utils';

type GroupDisplayContactProps = {
  group: Group | GroupPreload;
  navigation: any;
};

const GroupDisplayContact: React.FC<GroupDisplayContactProps> = ({ group, navigation }) => {
  const theme = useTheme();
  const { colors } = theme;

  if (!group) {
    return <ActivityIndicator size="large" color={colors.primary} />;
  }

  const contacts = group?.contacts;

  const icons: { [key: string]: string } = {
    email: 'email',
    telephone: 'phone',
    twitter: 'twitter',
    instagram: 'instagram',
    facebook: 'facebook',
    reddit: 'reddit',
  };

  return (
    <View>
      {contacts
        ? contacts.map(({ value, key, link }: { value: string; key: string; link?: string }) => (
            <InlineCard
              key={shortid()}
              icon={icons[key.toLowerCase()] || 'at'}
              title={value}
              subtitle={key}
              onPress={link ? () => handleUrl(link) : undefined}
            />
          ))
        : null}
    </View>
  );
};

export default GroupDisplayContact;
