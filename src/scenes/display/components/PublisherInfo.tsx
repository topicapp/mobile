import { useNavigation } from '@react-navigation/core';
import React from 'react';
import { View } from 'react-native';
import { Button, useTheme } from 'react-native-paper';
import { connect } from 'react-redux';
import shortid from 'shortid';

import { CategoryTitle } from '@components';
import { InlineCard } from '@components/Cards';
import { groupFollow } from '@redux/actions/apiActions/groups';
import { userFollow } from '@redux/actions/apiActions/users';
import { fetchAccount } from '@redux/actions/data/account';
import {
  Account,
  Group,
  GroupPreload,
  GroupRequestState,
  State,
  User,
  UserPreload,
  UserRequestState,
} from '@ts/types';
import { Errors, handleUrl } from '@utils';

import { ArticleDisplayScreenNavigationProp } from '../articles';
import { EventDisplayScreenNavigationProp } from '../events';
import getStyles from './styles';

type Navigation =
  | ArticleDisplayScreenNavigationProp<'Display'>
  | EventDisplayScreenNavigationProp<'Display'>;
type CombinedReqState = {
  groups: GroupRequestState;
  users: UserRequestState;
};
type PublisherInfoProps = {
  authors?: (User | UserPreload)[];
  group?: Group | GroupPreload;
  account: Account;
  reqState: CombinedReqState;
  navigation: Navigation;
};

const PublisherInfo: React.FC<PublisherInfoProps> = ({
  group,
  authors,
  account,
  reqState,
  navigation,
}) => {
  const theme = useTheme();
  const { colors } = theme;
  const styles = getStyles(theme);

  const following = account.accountInfo?.user?.data.following;

  const followGroup = (id: string) => {
    groupFollow(id)
      .then(fetchAccount)
      .catch((error) =>
        Errors.showPopup({
          type: 'axios',
          what: 'la modification du suivi du groupe',
          error,
          retry: () => followGroup(id),
        }),
      );
  };
  const followAuthor = (id: string) => {
    userFollow(id)
      .then(fetchAccount)
      .catch((error) =>
        Errors.showPopup({
          type: 'axios',
          what: 'la modification du suivi du groupe',
          error,
          retry: () => followGroup(id),
        }),
      );
  };

  return (
    <View>
      {authors ? (
        <View>
          <View style={styles.container}>
            <CategoryTitle>Auteur{authors?.length > 1 ? 's' : ''}</CategoryTitle>
          </View>
          {authors?.map((author) => (
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}
            >
              <View style={{ flex: 1 }}>
                <InlineCard
                  key={author._id}
                  avatar={author.info?.avatar}
                  title={author.displayName}
                  subtitle={
                    author.displayName === author.info?.username
                      ? undefined
                      : `@${author.info?.username}`
                  }
                  onPress={() =>
                    navigation.push('Root', {
                      screen: 'Main',
                      params: {
                        screen: 'Display',
                        params: {
                          screen: 'User',
                          params: {
                            screen: 'Display',
                            params: { id: author._id || '' /* title: author.displayName */ },
                          },
                        },
                      },
                    })
                  }
                  badge={
                    account.loggedIn &&
                    account.accountInfo?.user &&
                    following?.users.some((u) => u._id === author._id)
                      ? 'heart'
                      : author.info?.official
                      ? 'check-decagram'
                      : undefined
                  }
                  badgeColor={colors.primary}
                />
              </View>
              {account.loggedIn &&
              account.accountInfo?.user &&
              !following?.users.some((u) => u._id === author._id) ? (
                <Button
                  mode="contained"
                  loading={reqState.users.follow?.loading}
                  disabled={reqState.users.follow?.loading}
                  style={{ marginRight: 10 }}
                  onPress={() => followAuthor(author._id)}
                >
                  S&apos;abonner
                </Button>
              ) : null}
            </View>
          ))}
        </View>
      ) : null}
      {group ? (
        <View>
          <View style={styles.container}>
            <CategoryTitle>Groupe</CategoryTitle>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}
          >
            <View style={{ flex: 1 }}>
              <InlineCard
                avatar={group.avatar}
                title={group.name || group.displayName}
                subtitle={`Groupe ${group.type}`}
                onPress={() =>
                  navigation.push('Root', {
                    screen: 'Main',
                    params: {
                      screen: 'Display',
                      params: {
                        screen: 'Group',
                        params: {
                          screen: 'Display',
                          params: { id: group._id, title: group.displayName },
                        },
                      },
                    },
                  })
                }
                badge={
                  account.loggedIn &&
                  account.accountInfo?.user &&
                  following?.groups.some((g) => g._id === group._id)
                    ? 'heart'
                    : group.official
                    ? 'check-decagram'
                    : undefined
                }
                badgeColor={colors.primary}
              />
            </View>
            {account.loggedIn &&
            account.accountInfo?.user &&
            !following?.groups.some((u) => u._id === group._id) ? (
              <Button
                mode="contained"
                loading={reqState.groups.follow?.loading}
                disabled={reqState.groups.follow?.loading}
                style={{ marginRight: 10 }}
                onPress={() => followGroup(group._id)}
              >
                S&apos;abonner
              </Button>
            ) : null}
          </View>
        </View>
      ) : null}
    </View>
  );
};

const mapStateToProps = (state: State) => {
  const { account, users, groups } = state;
  const reqState = { users: users.state, groups: groups.state };
  return { account, reqState };
};

export default connect(mapStateToProps)(PublisherInfo);
