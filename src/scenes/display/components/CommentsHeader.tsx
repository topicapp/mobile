import { useNavigation } from '@react-navigation/core';
import React from 'react';
import { Alert, View } from 'react-native';
import { Button, Divider, List, Text, useTheme } from 'react-native-paper';
import { connect } from 'react-redux';
import shortid from 'shortid';

import { CategoryTitle, ErrorMessage } from '@components';
import { fetchArticleMy } from '@redux/actions/api/articles';
import { updateComments } from '@redux/actions/api/comments';
import { fetchEventMy } from '@redux/actions/api/events';
import {
  Account,
  ArticleMyInfo,
  ArticleRequestState,
  CommentRequestState,
  EventMyInfo,
  EventRequestState,
  Group,
  GroupPreload,
  State,
} from '@ts/types';
import { Errors, handleUrl, shareContent, trackEvent } from '@utils';

import { ArticleDisplayScreenNavigationProp } from '../articles';
import { EventDisplayScreenNavigationProp } from '../events';
import getStyles from './styles';

type Navigation =
  | ArticleDisplayScreenNavigationProp<'Display'>
  | EventDisplayScreenNavigationProp<'Display'>;
type CombinedReqState = {
  articles: ArticleRequestState;
  events: EventRequestState;
  comments: CommentRequestState;
};
type CommentsHeaderProps = {
  type: 'article' | 'event';
  id: string;
  reqState: CombinedReqState;
  account: Account;
  navigation: Navigation;
  setReplyingToComment: (c: string | null) => void;
  setCommentModalVisible: (val: boolean) => void;
};

const CommentsHeader: React.FC<CommentsHeaderProps> = ({
  account,
  reqState,
  navigation,
  type,
  id,
  setReplyingToComment,
  setCommentModalVisible,
}) => {
  const theme = useTheme();
  const { colors } = theme;
  const styles = getStyles(theme);

  return (
    <View>
      <View style={styles.container}>
        <CategoryTitle>Commentaires</CategoryTitle>
      </View>
      <Divider />
      {account.loggedIn ? (
        <View>
          <List.Item
            title="Écrire un commentaire"
            titleStyle={styles.placeholder}
            right={() => <List.Icon icon="comment-plus" color={colors.icon} />}
            onPress={() => {
              setReplyingToComment(null);
              setCommentModalVisible(true);
            }}
          />
        </View>
      ) : (
        <View style={styles.contentContainer}>
          <Text style={styles.disabledText}>Connectez-vous pour écrire un commentaire</Text>
          <Text>
            <Text
              onPress={() =>
                navigation.navigate('Auth', {
                  screen: 'Login',
                  params: {
                    goBack: true,
                  },
                })
              }
              style={[styles.link, styles.primaryText]}
            >
              Se connecter
            </Text>
            <Text style={styles.disabledText}> ou </Text>
            <Text
              onPress={() =>
                navigation.navigate('Auth', {
                  screen: 'Create',
                  params: {
                    goBack: true,
                  },
                })
              }
              style={[styles.link, styles.primaryText]}
            >
              Créer un compte
            </Text>
          </Text>
        </View>
      )}
      <Divider />
      <View>
        {(reqState.comments.list.error || reqState.articles.my?.error) && (
          <ErrorMessage
            type="axios"
            strings={{
              what: 'la récupération des commentaires et des likes',
              contentPlural: 'des commentaires et des likes',
            }}
            error={[reqState.comments.list.error, reqState.articles.my?.error]}
            retry={() => {
              updateComments('initial', { parentId: id });
              if (type === 'article') {
                fetchArticleMy(id);
              } else if (type === 'event') {
                fetchEventMy(id);
              }
            }}
          />
        )}
      </View>
    </View>
  );
};

const mapStateToProps = (state: State) => {
  const { account, comments, articles, events } = state;
  const reqState = { comments: comments.state, events: events.state, articles: articles.state };
  return { account, reqState };
};

export default connect(mapStateToProps)(CommentsHeader);
