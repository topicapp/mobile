import { useNavigation } from '@react-navigation/core';
import React from 'react';
import { Alert, View } from 'react-native';
import { Button, Card, Text, useTheme } from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';
import shortid from 'shortid';

import { articleVerificationApprove } from '@redux/actions/apiActions/articles';
import { eventVerificationApprove } from '@redux/actions/apiActions/events';
import {
  Account,
  ArticleMyInfo,
  ArticleRequestState,
  ArticleVerification,
  Content,
  EventMyInfo,
  EventRequestState,
  EventVerification,
  Group,
  GroupPreload,
  State,
  Verification,
} from '@ts/types';
import { Errors, handleUrl, shareContent, trackEvent } from '@utils';

import { ArticleDisplayScreenNavigationProp } from '../articles';
import { EventDisplayScreenNavigationProp } from '../events';
import getStyles from './styles';

type Navigation =
  | ArticleDisplayScreenNavigationProp<'Display'>
  | EventDisplayScreenNavigationProp<'Display'>;
type CombinedReqState = {
  articles: ArticleRequestState;
  events: EventRequestState;
};
type ContentVerification = ArticleVerification | EventVerification;
type ContentVerificationProps = {
  type: 'article' | 'event';
  id: string;
  reqState: CombinedReqState;
  content: Content;
  verification?: Verification;
  setReportModalVisible: (val: boolean) => void;
  navigation: Navigation;
};

const ContentVerification: React.FC<ContentVerificationProps> = ({
  type,
  id,
  reqState,
  content,
  verification,
  setReportModalVisible,
  navigation,
}) => {
  const theme = useTheme();
  const { colors } = theme;
  const styles = getStyles(theme);

  const state = type === 'article' ? reqState.articles : reqState.events;

  const approve = () => {
    trackEvent(`${type}display:approve`, { props: { method: 'moderation' } });
    if (type === 'article') {
      articleVerificationApprove(id)
        .then(() => navigation.goBack())
        .catch((error) =>
          Errors.showPopup({
            type: 'axios',
            what: "l'approbation de l'article",
            error,
            retry: approve,
          }),
        );
    } else if (type === 'event') {
      eventVerificationApprove(id)
        .then(() => navigation.goBack())
        .catch((error) =>
          Errors.showPopup({
            type: 'axios',
            what: "l'approbation de l'article",
            error,
            retry: approve,
          }),
        );
    }
  };

  return (
    <View>
      <View style={[styles.container, { marginTop: 40 }]}>
        <Card
          elevation={0}
          style={{ borderColor: colors.primary, borderWidth: 1, borderRadius: 5 }}
        >
          <View style={[styles.container, { flexDirection: 'row' }]}>
            <Icon
              name="shield-alert-outline"
              style={{ alignSelf: 'flex-start', marginRight: 10 }}
              size={24}
              color={colors.primary}
            />
            <Text style={{ color: colors.text, flex: 1 }}>
              Pour vérifier cet article:{'\n'}- Vérifiez que le contenu est bien conforme aux
              conditions générales d&apos;utilisation{'\n'}- Vérifiez que tous les médias sont
              conformes et que vous avez bien le droit d&apos;utiliser ceux-ci
              {'\n'}- Visitez chacun des liens afin de vous assurer que tous les sites sont
              conformes{'\n'}
              {'\n'}
              Nous vous rappelons que les contenus suivants ne sont pas autorisés : {'\n'}- Tout
              contenu illégal{'\n'}- Tout contenu haineux ou discriminatoire{'\n'}- Tout contenu à
              caractère pornographique ou qui ne convient pas aux enfants
              {'\n'}- Toute atteinte à la propriété intellectuelle{'\n'}- Tout contenu trompeur
              {'\n'}- Toute atteinte à la vie privée{'\n'}- Tout contenu publié de façon automatisée
              {'\n'}- Tout contenu qui pointe vers un site web, logiciel ou autre média qui ne
              respecte pas les présentes règles{'\n'}
              {'\n'}
              En tant qu&apos;administrateur, vous êtes en partie responsable des contenus publiés,
              comme détaillé dans les Conditions Générales d&apos;Utilisation.
            </Text>
          </View>
        </Card>
      </View>
      {content?.data?.match(/(https?:\/\/[^\s]+)/g)?.length && (
        <View style={[styles.container, { marginTop: 20 }]}>
          <Card
            elevation={0}
            style={{ borderColor: colors.disabled, borderWidth: 1, borderRadius: 5 }}
          >
            <View style={[styles.container, { flexDirection: 'row' }]}>
              <Icon
                name="link"
                style={{ alignSelf: 'flex-start', marginRight: 10 }}
                size={24}
                color={colors.disabled}
              />
              <Text style={{ color: colors.text }}>
                Liens contenus dans l&apos;article:{'\n'}
                {content?.data?.match(/(https?:\/\/[^\s]+)/g)?.map((u: string) => (
                  <Text
                    key={shortid()}
                    style={{ textDecorationLine: 'underline' }}
                    onPress={() => handleUrl(u)}
                  >
                    {u}
                    {'\n'}
                  </Text>
                ))}
              </Text>
            </View>
          </Card>
        </View>
      )}
      <View style={[styles.container, { marginTop: 20 }]}>
        {verification?.bot?.flags?.length !== 0 && (
          <View style={{ flexDirection: 'row' }}>
            <Icon
              name="tag"
              color={colors.invalid}
              size={16}
              style={{ alignSelf: 'center', marginRight: 5 }}
            />
            <Text>Classifié comme {verification?.bot?.flags?.join(', ')}</Text>
          </View>
        )}
        {verification?.reports?.length !== 0 && (
          <View style={{ flexDirection: 'row' }}>
            <Icon
              name="message-alert"
              color={colors.invalid}
              size={16}
              style={{ alignSelf: 'center', marginRight: 5 }}
            />
            <Text>Reporté {verification?.reports?.length} fois</Text>
          </View>
        )}
        {verification?.users?.length !== 0 && (
          <View style={{ flexDirection: 'row' }}>
            <Icon
              name="shield"
              color={colors.invalid}
              size={16}
              style={{ alignSelf: 'center', marginRight: 5 }}
            />
            <Text>Remis en modération</Text>
          </View>
        )}
        {verification?.extraVerification && (
          <View style={{ flexDirection: 'row' }}>
            <Icon
              name="alert-decagram"
              color={colors.invalid}
              size={16}
              style={{ alignSelf: 'center', marginRight: 5 }}
            />
            <Text>Vérification d&apos;un administrateur Topic requise</Text>
          </View>
        )}
      </View>
      <View style={{ flexDirection: 'row', justifyContent: 'space-evenly' }}>
        <View style={[styles.container]}>
          <Button
            mode="outlined"
            color={colors.invalid}
            contentStyle={{
              height: 50,
              alignSelf: 'stretch',
              justifyContent: 'center',
            }}
            onPress={() => setReportModalVisible(true)}
          >
            Signaler
          </Button>
        </View>
        <View style={styles.container}>
          <Button
            mode="contained"
            loading={state.verification_approve?.loading}
            color={colors.valid}
            contentStyle={{
              height: 50,
              justifyContent: 'center',
            }}
            onPress={approve}
          >
            Publier
          </Button>
        </View>
      </View>
    </View>
  );
};

const mapStateToProps = (state: State) => {
  const { articles, events } = state;
  const reqState = { articles: articles.state, events: events.state };
  return { reqState };
};

export default connect(mapStateToProps)(ContentVerification);
