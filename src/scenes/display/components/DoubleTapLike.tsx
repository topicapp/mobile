import { useNavigation } from '@react-navigation/core';
import React, { ReactElement } from 'react';
import { Alert, TouchableWithoutFeedback, View } from 'react-native';
import ModalComponent from 'react-native-modal';
import { Button, Text, useTheme } from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';
import shortid from 'shortid';

import { fetchArticleMy } from '@redux/actions/api/articles';
import { fetchEventMy } from '@redux/actions/api/events';
import { articleLike } from '@redux/actions/apiActions/articles';
import { eventLike } from '@redux/actions/apiActions/events';
import {
  Account,
  ArticleMyInfo,
  ArticleRequestState,
  EventMyInfo,
  EventRequestState,
  Group,
  GroupPreload,
  State,
} from '@ts/types';
import { Errors, handleUrl, shareContent, trackEvent } from '@utils';

import { ArticleDisplayScreenNavigationProp } from '../articles';
import { EventDisplayScreenNavigationProp } from '../events';
import getStyles from './styles';

type CombinedReqState = {
  articles: ArticleRequestState;
  events: EventRequestState;
};
type DoubleTapLikeProps = {
  type: 'article' | 'event';
  id: string;
  contentMy?: ArticleMyInfo | EventMyInfo;
  reqState: CombinedReqState;
  account: Account;
  enabled: boolean;
  children: ReactElement;
};

const DoubleTapLike: React.FC<DoubleTapLikeProps> = ({
  type,
  contentMy,
  reqState,
  account,
  id,
  enabled,
  children,
}) => {
  const theme = useTheme();
  const { colors } = theme;
  const styles = getStyles(theme);

  const like = () => {
    trackEvent(`${type}display:${contentMy?.liked ? 'unlike' : 'like'}`, {
      props: { button: 'doubletap' },
    });
    if (type === 'article') {
      articleLike(id, !contentMy?.liked)
        .then(() => {
          fetchArticleMy(id);
        })
        .catch((error) =>
          Errors.showPopup({
            type: 'axios',
            what: 'la prise en compte du like',
            error,
            retry: like,
          }),
        );
    } else if (type === 'event') {
      eventLike(id, !contentMy?.liked)
        .then(() => {
          fetchEventMy(id);
        })
        .catch((error) =>
          Errors.showPopup({
            type: 'axios',
            what: 'la prise en compte du like',
            error,
            retry: like,
          }),
        );
    }
  };

  const [visible, setVisible] = React.useState(false);

  let lastContentPressTime: number | undefined = undefined;
  const handleContentPress = () => {
    if (!contentMy?.liked) {
      const now = new Date().valueOf();
      if (lastContentPressTime && now - lastContentPressTime < 500) {
        setVisible(true);
        like();
        lastContentPressTime = undefined;
      } else {
        lastContentPressTime = now;
      }
    }
  };

  if (!enabled || !account.loggedIn) return children;

  return (
    <>
      <TouchableWithoutFeedback onPress={handleContentPress}>{children}</TouchableWithoutFeedback>
      <ModalComponent
        isVisible={visible}
        animationIn="bounceIn"
        animationInTiming={1000}
        animationOut="fadeOut"
        useNativeDriver
        onBackdropPress={() => setVisible(false)}
      >
        <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
          <Icon name="thumb-up" size={120} color={colors.primary} />
          <Text style={{ color: colors.primary, fontSize: 30 }}>Article liké</Text>
        </View>
      </ModalComponent>
    </>
  );
};

const mapStateToProps = (state: State) => {
  const { account, articles, events } = state;
  const reqState = { articles: articles.state, events: events.state };
  return { account, reqState };
};

export default connect(mapStateToProps)(DoubleTapLike);
