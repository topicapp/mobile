import { useNavigation } from '@react-navigation/core';
import React from 'react';
import { View } from 'react-native';
import { Button, useTheme } from 'react-native-paper';
import { connect } from 'react-redux';
import shortid from 'shortid';

import { fetchArticleMy } from '@redux/actions/api/articles';
import { fetchEventMy } from '@redux/actions/api/events';
import { articleLike } from '@redux/actions/apiActions/articles';
import { eventLike } from '@redux/actions/apiActions/events';
import {
  Account,
  ArticleMyInfo,
  ArticleRequestState,
  EventMyInfo,
  EventRequestState,
  Group,
  GroupPreload,
  State,
} from '@ts/types';
import { Errors, Alert, handleUrl, shareContent, trackEvent } from '@utils';

import { ArticleDisplayScreenNavigationProp } from '../articles';
import { EventDisplayScreenNavigationProp } from '../events';
import getStyles from './styles';

type Navigation =
  | ArticleDisplayScreenNavigationProp<'Display'>
  | EventDisplayScreenNavigationProp<'Display'>;
type CombinedReqState = {
  articles: ArticleRequestState;
  events: EventRequestState;
};
type ContentLikeShareProps = {
  type: 'article' | 'event';
  id: string;
  title: string;
  group?: Group | GroupPreload;
  contentMy?: ArticleMyInfo | EventMyInfo | null;
  reqState: CombinedReqState;
  account: Account;
  navigation: Navigation;
};

const ContentLikeShare: React.FC<ContentLikeShareProps> = ({
  type,
  contentMy,
  reqState,
  account,
  navigation,
  id,
  title,
  group,
}) => {
  const theme = useTheme();
  const { colors } = theme;
  const styles = getStyles(theme);

  const showLikeLoginAlert = () =>
    Alert.alert(
      'Connectez vous pour liker cet article',
      'Avec un compte Topic, vous pourrez liker les articles, suivre vos groupes préférés et en rejoindre.',
      [
        { text: 'Se connecter', onPress: () => navigation.navigate('Auth', { screen: 'Login' }) },
        {
          text: 'Créer un compte',
          onPress: () => navigation.navigate('Auth', { screen: 'Create' }),
        },
        { text: 'Annuler' },
      ],
      { cancelable: true },
    );

  const like = (button = 'bottom') => {
    trackEvent(`${type}display:${contentMy?.liked ? 'unlike' : 'like'}`, {
      props: { button },
    });
    if (type === 'article') {
      articleLike(id, !contentMy?.liked)
        .then(() => {
          fetchArticleMy(id);
        })
        .catch((error) =>
          Errors.showPopup({
            type: 'axios',
            what: 'la prise en compte du like',
            error,
            retry: like,
          }),
        );
    } else if (type === 'event') {
      eventLike(id, !contentMy?.liked)
        .then(() => {
          fetchEventMy(id);
        })
        .catch((error) =>
          Errors.showPopup({
            type: 'axios',
            what: 'la prise en compte du like',
            error,
            retry: like,
          }),
        );
    }
  };

  const share = () => {
    shareContent({
      title: type === 'event' ? `Évènement ${title}` : title,
      group: group?.displayName,
      type: type === 'article' ? 'articles' : 'evenements',
      id,
    });
    trackEvent(`${type}display:share`, { props: { button: 'bottom' } });
  };

  const loading =
    type === 'article' ? reqState.articles.like?.loading : reqState.events.like?.loading;

  return (
    <View
      style={{
        flexDirection: 'row',
        marginVertical: 10,
        marginHorizontal: 10,
        justifyContent: 'space-around',
      }}
    >
      <Button
        mode={contentMy?.liked ? 'outlined' : 'contained'}
        icon={contentMy?.liked ? 'thumb-up' : 'thumb-up-outline'}
        loading={loading}
        disabled={loading}
        style={{ flex: 1, marginHorizontal: 5, borderRadius: 20 }}
        onPress={account.loggedIn ? () => like() : () => showLikeLoginAlert()}
      >
        {contentMy?.liked ? 'Liké' : 'Liker'}
      </Button>
      <Button
        mode="contained"
        icon="share-variant"
        style={{ flex: 1, marginHorizontal: 5, borderRadius: 20 }}
        color={colors.primary}
        onPress={share}
      >
        Partager
      </Button>
    </View>
  );
};

const mapStateToProps = (state: State) => {
  const { account, articles, events } = state;
  const reqState = { articles: articles.state, events: events.state };
  return { account, reqState };
};

export default connect(mapStateToProps)(ContentLikeShare);
