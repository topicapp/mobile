import React from 'react';
import { View, Platform } from 'react-native';
import { List, Checkbox, useTheme } from 'react-native-paper';

import { LocationItem } from '../utils/getNewLocations';

type LocationListItemProps = LocationItem & {
  selected: boolean;
  onAdd: (loc: LocationItem) => void;
  onRemove: (locId: LocationItem) => void;
  disabled?: boolean;
};

const LocationListItem: React.FC<LocationListItemProps> = React.memo(
  ({ id, name, type, description, departments, selected, onAdd, onRemove, disabled }) => {
    const { colors } = useTheme();
    return (
      <List.Item
        title={name}
        description={description}
        descriptionNumberOfLines={1}
        disabled={disabled}
        onPress={
          selected
            ? () => onRemove({ id, name, type, description, departments })
            : () => onAdd({ id, name, type, description, departments })
        }
        left={() =>
          Platform.OS !== 'ios' ? (
            <View style={{ alignSelf: 'center' }}>
              <Checkbox
                status={selected ? 'checked' : 'unchecked'}
                color={colors.primary}
                disabled={disabled}
              />
            </View>
          ) : null
        }
        right={() =>
          Platform.OS === 'ios' ? (
            <View style={{ alignSelf: 'center' }}>
              <Checkbox
                status={selected ? 'checked' : 'unchecked'}
                color={colors.primary}
                disabled={disabled}
              />
            </View>
          ) : null
        }
      />
    );
  },
);

export default LocationListItem;
