import { RouteProp } from '@react-navigation/native';
import React from 'react';
import { Platform, View, ScrollView, Clipboard } from 'react-native';
import { Text, Button, Divider, Card, useTheme } from 'react-native-paper';
import { SafeAreaView } from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';

import { Illustration, ErrorMessage, FeedbackCard } from '@components';
import config from '@constants/config';
import { eventVerificationApprove } from '@redux/actions/apiActions/events';
import { State, EventRequestState, Account } from '@ts/types';
import { checkPermission, Alert, shareContent, trackEvent, Permissions } from '@utils';

import type { EventAddScreenNavigationProp, EventAddStackParams } from '.';
import getStyles from './styles';

type EventAddSuccessProps = {
  navigation: EventAddScreenNavigationProp<'Success'>;
  route: RouteProp<EventAddStackParams, 'Success'>;
  reqState: EventRequestState;
  account: Account;
};

const EventAddSuccess: React.FC<EventAddSuccessProps> = ({
  navigation,
  reqState,
  account,
  route,
}) => {
  const theme = useTheme();
  const styles = getStyles(theme);
  const { colors } = theme;

  const [approved, setApproved] = React.useState(false);

  const { id, creationData, editing } = route?.params || {};

  const groupName = account?.groups?.find((g) => g._id === creationData?.group)?.name;

  const approve = () => {
    trackEvent('eventdisplay:approve', { props: { method: 'add-success' } });
    if (id) eventVerificationApprove(id).then(() => setApproved(true));
  };

  // TODO: Consider using IconButton instead of Icon here

  return (
    <View style={styles.page}>
      {reqState.verification_approve?.success === false && (
        <SafeAreaView style={{ flex: 1 }}>
          <ErrorMessage
            error={reqState.verification_approve?.error}
            strings={{
              what: "l'approbation de l'évènement",
              contentSingular: "L'évènement",
            }}
            type="axios"
            retry={approve}
          />
        </SafeAreaView>
      )}
      <ScrollView>
        <View
          style={[styles.centerIllustrationContainer, styles.contentContainer, { marginTop: 40 }]}
        >
          <Illustration name="auth-register-success" height={200} width={200} />
          <Text style={[styles.title, { textAlign: 'center' }]}>
            Évènement {editing ? 'Modifié' : approved ? 'publié' : 'en attente de modération'}
          </Text>
          {!approved && !editing && (
            <View>
              <Text style={{ marginTop: 40, textAlign: 'center' }}>
                Votre évènement doit être approuvé par un administrateur de {groupName}.
              </Text>
              <Text style={{ textAlign: 'center' }}>
                Vous serez notifié dès que l&apos;évènement sera approuvé.
              </Text>
            </View>
          )}
          {checkPermission(account, {
            permission: Permissions.ARTICLE_VERIFICATION_APPROVE,
            scope: { groups: [creationData?.group || ''] },
          }) &&
            !editing &&
            (approved ? (
              <Text>Évènement publié par @{account?.accountInfo?.user?.info?.username}</Text>
            ) : (
              <View>
                <Text style={{ marginTop: 30, textAlign: 'center' }}>
                  Vous pouvez publier vous-même cet évènement.
                </Text>
                <Button
                  uppercase={Platform.OS !== 'ios'}
                  loading={reqState.verification_approve?.loading}
                  mode="outlined"
                  style={{ marginTop: 10 }}
                  onPress={() => {
                    Alert.alert(
                      "Publier l'évènement ?",
                      "L'évènement doit être conforme aux conditions d'utilisation.\nVous êtes responsable si l'évènement ne les respecte pas, et nous pouvons désactiver votre compte si c'est le cas.\n\nDe plus, nous vous conseillons d'attendre l'approbation d'un autre membre, afin d'éviter les erreurs.",
                      [
                        {
                          text: 'Annuler',
                        },
                        {
                          text: 'Publier',
                          onPress: approve,
                        },
                      ],
                      { cancelable: true },
                    );
                  }}
                >
                  {reqState.verification_approve?.loading ? '' : 'Publier'}
                </Button>
              </View>
            ))}
        </View>
        <View style={{ marginBottom: 20 }}>
          <View style={[styles.container, { marginTop: 20 }]}>
            <Text style={{ color: colors.disabled, marginBottom: 5, marginLeft: 10 }}>
              Lien de partage
            </Text>
            <Card
              elevation={0}
              style={{ borderColor: colors.primary, borderWidth: 1, borderRadius: 5 }}
            >
              <View style={[styles.container, { flexDirection: 'row' }]}>
                <ScrollView horizontal>
                  <Text style={{ color: colors.text, fontSize: 20 }} numberOfLines={1}>
                    {config.links.share}/evenements/{id}
                  </Text>
                </ScrollView>
                <Icon
                  name="content-copy"
                  style={{ alignSelf: 'center', marginLeft: 10 }}
                  size={24}
                  color={colors.text}
                  onPress={() => {
                    Clipboard.setString(`${config.links.share}/evenements/${id}`);
                    trackEvent('eventadd:success-copylink');
                    Alert.alert(
                      'Lien copié',
                      "Vous ne pourrez pas utiliser ce lien tant que l'évènement n'aura pas été validé.",
                      [{ text: 'Fermer' }],
                      { cancelable: true },
                    );
                  }}
                />
                <Icon
                  name="share-variant"
                  style={{ alignSelf: 'center', marginLeft: 10 }}
                  size={24}
                  color={colors.text}
                  onPress={() => {
                    Alert.alert(
                      "Partager l'évènement",
                      "L'évènement ne sera pas accessible tant qu'il n'aura pas été approuvé.",
                      [
                        { text: 'Annuler' },
                        {
                          text: 'Partager',
                          onPress: () => {
                            if (!creationData?.title || !id) return;
                            trackEvent('eventadd:success-share');
                            shareContent({
                              title: creationData.title,
                              group: groupName,
                              type: 'evenements',
                              id,
                            });
                          },
                        },
                      ],
                      { cancelable: true },
                    );
                  }}
                />
              </View>
            </Card>
          </View>
        </View>
      </ScrollView>
      <Divider />
      <View style={styles.formContainer}>
        <View style={styles.buttonContainer}>
          <Button
            mode={Platform.OS !== 'ios' ? 'contained' : 'outlined'}
            uppercase={Platform.OS !== 'ios'}
            onPress={() =>
              navigation.navigate('Main', {
                screen: 'Home1',
                params: { screen: 'Home2', params: { screen: 'Event' } },
              })
            }
            style={{ flex: 1 }}
          >
            Continuer
          </Button>
        </View>
      </View>
    </View>
  );
};

const mapStateToProps = (state: State) => {
  const { account, events } = state;
  return { account, reqState: events.state };
};

export default connect(mapStateToProps)(EventAddSuccess);
