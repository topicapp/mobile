import React from 'react';
import { View, ScrollView, KeyboardAvoidingView, Platform } from 'react-native';
import { Text, useTheme } from 'react-native-paper';
import { SafeAreaView } from 'react-native-safe-area-context';
import { connect } from 'react-redux';

import {
  TranslucentStatusBar,
  StepperView,
  PlatformBackButton,
  StepperViewPageProps,
} from '@components';
import { eventAdd } from '@redux/actions/apiActions/events';
import { clearEventCreationData } from '@redux/actions/contentData/events';
import { State, EventRequestState, EventCreationData, ProgramEntry } from '@ts/types';
import { Errors, trackEvent } from '@utils';

import type { EventAddScreenNavigationProp } from '.';
import EventAddPageDetails from './components/AddDetails';
import EventAddPageGroup from './components/AddGroup';
import EventAddPageLocation from './components/AddLocation';
import EventAddPageMeta from './components/AddMeta';
import EventAddPageTags from './components/AddTags';
import getStyles from './styles';

type Props = {
  navigation: EventAddScreenNavigationProp<'Add'>;
  reqState: EventRequestState;
  creationData?: EventCreationData;
};

const EventAdd: React.FC<Props> = ({ navigation, reqState, creationData = {} }) => {
  const theme = useTheme();
  const styles = getStyles(theme);

  const scrollViewRef = React.useRef<ScrollView>(null);

  return (
    <View style={styles.page}>
      <SafeAreaView style={{ flex: 1 }}>
        <TranslucentStatusBar />
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
          style={{ flex: 1 }}
        >
          <ScrollView keyboardShouldPersistTaps="handled" nestedScrollEnabled ref={scrollViewRef}>
            <PlatformBackButton onPress={navigation.goBack} />
            <View style={styles.centerIllustrationContainer}>
              <Text style={styles.title}>
                {creationData.editing ? `Modifier "${creationData.title}"` : 'Créer un évènement'}
              </Text>
            </View>
            <StepperView
              onChange={() => scrollViewRef.current?.scrollTo({ x: 0, y: 0, animated: true })}
              pages={[
                ...(creationData.editing
                  ? []
                  : [
                      {
                        key: 'group',
                        icon: 'account-group',
                        title: 'Groupe',
                        component: (props: StepperViewPageProps) => (
                          <EventAddPageGroup {...props} />
                        ),
                      },
                    ]),
                {
                  key: 'location',
                  icon: 'map-marker',
                  title: 'Loc.',
                  component: (props) => <EventAddPageLocation navigation={navigation} {...props} />,
                },
                {
                  key: 'meta',
                  icon: 'information',
                  title: 'Info',
                  component: (props) => <EventAddPageMeta {...props} />,
                },
                {
                  key: 'contact',
                  icon: 'file-document',
                  title: 'Détails',
                  component: (props) => <EventAddPageDetails {...props} />,
                },
                {
                  key: 'tags',
                  icon: 'tag-multiple',
                  title: 'Tags',
                  component: (props) => (
                    <EventAddPageTags
                      navigate={() => navigation.navigate('AddContent')}
                      {...props}
                    />
                  ),
                },
                {
                  key: 'content',
                  icon: 'pencil',
                  title: 'Contenu',
                  component: () => <View />,
                },
              ]}
            />
          </ScrollView>
        </KeyboardAvoidingView>
      </SafeAreaView>
    </View>
  );
};

const mapStateToProps = (state: State) => {
  const { events, eventData } = state;
  return { creationData: eventData.creationData, reqState: events.state };
};

export default connect(mapStateToProps)(EventAdd);
