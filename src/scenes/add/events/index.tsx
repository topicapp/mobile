import { CompositeNavigationProp } from '@react-navigation/core';
import React from 'react';

import { EventCreationData, ReduxLocation } from '@ts/types';
import { createNativeStackNavigator, NativeStackNavigationProp } from '@utils/compat/stack';

import { AddScreenNavigationProp } from '../index';
import EventAdd from './Add';
import EventAddContent from './AddContent';
import EventAddSuccess from './AddSuccess';

export type EventAddStackParams = {
  Add: undefined;
  AddContent: undefined;
  Success: {
    id?: string;
    creationData?: EventCreationData;
    editing?: boolean;
  };
};

export type EventAddScreenNavigationProp<K extends keyof EventAddStackParams> =
  CompositeNavigationProp<
    NativeStackNavigationProp<EventAddStackParams, K>,
    AddScreenNavigationProp<'Event'>
  >;

const Stack = createNativeStackNavigator<EventAddStackParams>();

function EventAddStackNavigator() {
  return (
    <Stack.Navigator initialRouteName="Add" screenOptions={{ headerShown: false }}>
      <Stack.Screen name="Add" component={EventAdd} options={{ title: 'Créer un évènement' }} />
      <Stack.Screen
        name="AddContent"
        options={{ title: 'Créer un évènement' }}
        component={EventAddContent}
      />
      <Stack.Screen
        name="Success"
        component={EventAddSuccess}
        options={{ title: 'Créer un évènement' }}
      />
    </Stack.Navigator>
  );
}

export default EventAddStackNavigator;
