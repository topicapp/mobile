// @ts-expect-error
import MarkdownIt from 'markdown-it';
import React, { LegacyRef } from 'react';
import { View, ScrollView, Platform, KeyboardAvoidingView, Keyboard } from 'react-native';
import {
  Button,
  HelperText,
  Title,
  Divider,
  IconButton,
  RadioButton,
  List,
  TextInput,
  Card,
  Text,
  useTheme,
} from 'react-native-paper';
import { RichToolbar, RichEditor } from 'react-native-pell-rich-editor';
import { SafeAreaView } from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';
// @ts-expect-error
import TurndownService from 'turndown-rn';

import {
  TranslucentStatusBar,
  PlatformBackButton,
  CollapsibleView,
  Content,
  ContentEditor,
} from '@components';
import { Config } from '@constants';
import { eventAdd, eventModify } from '@redux/actions/apiActions/events';
import { upload } from '@redux/actions/apiActions/upload';
import { clearEventCreationData, updateEventCreationData } from '@redux/actions/contentData/events';
import { State, EventRequestState, EventCreationData, Account, ProgramEntry } from '@ts/types';
import { logger, checkPermission, Alert, Errors, trackEvent, Permissions } from '@utils';

import type { EventAddScreenNavigationProp } from '.';
import getStyles from './styles';

type EventAddContentProps = {
  navigation: EventAddScreenNavigationProp<'AddContent'>;
  reqState: EventRequestState;
  creationData?: EventCreationData;
  account: Account;
};

const EventAddContent: React.FC<EventAddContentProps> = ({
  navigation,
  reqState,
  creationData = {},
  account,
}) => {
  const theme = useTheme();
  const styles = getStyles(theme);

  const { colors } = theme;

  const add = (parser?: 'markdown' | 'plaintext', description?: string) => {
    if (!creationData.editing) {
      trackEvent('eventadd:add-request');
      eventAdd({
        title: creationData.title,
        summary: creationData.summary,
        data: description || creationData.description,
        phone: creationData.phone,
        email: creationData.email,
        contact: creationData.contact,
        members: creationData.members,
        start: creationData.start,
        end: creationData.end,
        date: new Date(),
        location: creationData.location,
        group: creationData.group,
        places: creationData.places,
        parser: parser || creationData.parser,
        image: creationData.image,
        preferences: {
          comments: true,
        },
        tags: creationData.tags,
        program: creationData.program,
      })
        .then(({ _id }) => {
          trackEvent('eventadd:add-success');
          navigation.goBack();
          navigation.replace('Success', { id: _id, creationData });
          clearEventCreationData();
        })
        .catch((error) => {
          Errors.showPopup({
            type: 'axios',
            what: "l'ajout de l'évènement",
            error,
            retry: () => add(parser, description),
          });
        });
    } else {
      trackEvent('eventadd:modify-request');
      eventModify({
        id: creationData.id,
        title: creationData.title,
        summary: creationData.summary,
        data: description || creationData.description,
        phone: creationData.phone,
        email: creationData.email,
        contact: creationData.contact,
        members: creationData.members,
        start: creationData.start,
        end: creationData.end,
        date: new Date(),
        location: creationData.location,
        group: creationData.group,
        places: creationData.places,
        parser: parser || creationData.parser,
        image: creationData.image,
        preferences: {
          comments: true,
        },
        tags: creationData.tags,
        program: creationData.program,
      })
        .then(({ _id }) => {
          trackEvent('eventadd:modify-success');
          navigation.goBack();
          navigation.replace('Success', { id: _id, creationData, editing: true });
          clearEventCreationData();
        })
        .catch((error) => {
          Errors.showPopup({
            type: 'axios',
            what: "la modification de l'évènement",
            error,
            retry: () => add(parser, description),
          });
        });
    }
  };

  const fetchSuggestions = (editor: 'source' | 'rich' | 'plaintext', data: string) => {
    const tempSuggestions: { text: string; icon: string }[] = [];

    if (data.length > 700 && !data.match(/#/g) && editor !== 'plaintext') {
      tempSuggestions.push({
        icon: 'information-outline',
        text: 'Vous pouvez utiliser des titres via le menu "Paragraphe" pour organiser votre contenu',
      });
    }

    return tempSuggestions;
  };

  const fetchWarnings = (editor: 'source' | 'rich' | 'plaintext', data: string) => {
    const tempSuggestions: { text: string; icon: string }[] = [];

    // Length
    if (data.length < 300) {
      tempSuggestions.push({
        icon: 'alert-circle-outline',
        text: "Votre description est plutôt courte. N'hésitez pas à rajouter quelques phrases pour donner plus de détails",
      });
    }

    return tempSuggestions;
  };

  const update = (parser: 'markdown' | 'plaintext', data: string) =>
    updateEventCreationData({ parser, data });

  return (
    <ContentEditor
      add={add}
      update={update}
      editing={creationData.editing}
      placeholder="Présentez votre évènement..."
      title={creationData.title}
      initialContent={{ parser: creationData.parser, data: creationData.data }}
      fetchSuggestions={fetchSuggestions}
      fetchWarnings={fetchWarnings}
      loading={reqState.add?.loading}
      back={() => navigation.goBack()}
      imageUploadGroup={
        checkPermission(account, {
          permission: Permissions.CONTENT_UPLOAD,
          scope: { groups: [creationData.group || ''] },
        }) && creationData.group
          ? creationData.group
          : null
      }
    />
  );
};

const mapStateToProps = (state: State) => {
  const { events, eventData, account } = state;
  return {
    creationData: eventData.creationData,
    reqState: events.state,
    account,
  };
};

export default connect(mapStateToProps)(EventAddContent);
