import { Formik } from 'formik';
import moment from 'moment';
import React, { createRef } from 'react';
import { View, Platform, TextInput as RNTextInput } from 'react-native';
import { Button, HelperText, IconButton, List, Text, useTheme } from 'react-native-paper';
import { DatePickerModal, TimePickerModal } from 'react-native-paper-dates';
import { connect } from 'react-redux';
import * as Yup from 'yup';

import { StepperViewPageProps, InlineCard, FormTextInput } from '@components';
import { fetchMultiDepartment } from '@redux/actions/api/departments';
import { fetchMultiSchool } from '@redux/actions/api/schools';
import { updateEventCreationData } from '@redux/actions/contentData/events';
import {
  Account,
  EventCreationData,
  EventCreationDataPlace,
  ProgramEntry,
  State,
  UserPreload,
} from '@ts/types';
import { Format } from '@utils';

import { CheckboxListItem } from '../../components/ListItems';
import getStyles from '../styles';
import ContactAddModal from './ContactAddModal';
import PlaceAddressModal from './PlaceAddressModal';
import PlaceOnlineModal from './PlaceOnlineModal';
import PlaceSelectModal from './PlaceSelectModal';
import PlaceTypeModal from './PlaceTypeModal';
import ProgramAddModal from './ProgramAddModal';
import UserSelectModal from './UserSelectModal';

type Props = StepperViewPageProps & {
  account: Account;
  creationData: EventCreationData;
};

const EventAddPageDetails: React.FC<Props> = ({ next, prev, account, creationData }) => {
  // Contact
  const [isAddUserModalVisible, setAddUserModalVisible] = React.useState(false);
  const [isContactAddModalVisible, setContactAddModalVisible] = React.useState(false);

  const [eventOrganizers, setEventOrganizers] = React.useState<UserPreload[]>([]);
  const [customContact, setCustomContact] = React.useState<CustomContactType[]>(
    (creationData.contact as CustomContactType[]) || [],
  );
  const [email, setEmail] = React.useState<string | null>(creationData.email || null);
  const [phone, setPhone] = React.useState<string | null>(creationData.phone || null);

  const theme = useTheme();
  const styles = getStyles(theme);

  type CustomContactType = {
    _id: string;
    key: string;
    value: string;
    link: string;
  };

  const addEventOrganizer = (user: UserPreload) => {
    const previousEventIds = eventOrganizers.map((p) => p._id);
    if (!previousEventIds.includes(user._id)) {
      setEventOrganizers([...eventOrganizers, user]);
    }
  };

  const addCustomContact = (contact: CustomContactType) => {
    if (contact.key === 'email') {
      setEmail(contact.value);
    } else if (contact.key === 'phone') {
      setPhone(contact.value);
    } else {
      setCustomContact([...customContact, contact]);
    }
  };

  // Places
  const [isPlaceTypeModalVisible, setPlaceTypeModalVisible] = React.useState(false);
  const [isPlaceSelectModalVisible, setPlaceSelectModalVisible] = React.useState(false);
  const [isPlaceAddressModalVisible, setPlaceAddressModalVisible] = React.useState(false);
  const [isPlaceOnlineModalVisible, setPlaceOnlineModalVisible] = React.useState(false);
  const [placeType, setPlaceType] = React.useState<'school' | 'place' | 'standalone' | 'online'>(
    'school',
  );

  const [eventPlaces, setEventPlaces] = React.useState<EventCreationDataPlace[]>(
    creationData.places || [],
  );

  const toSelectedType = (data: 'school' | 'place' | 'standalone' | 'online') => {
    setPlaceTypeModalVisible(false);
    if (data === 'standalone') {
      setPlaceAddressModalVisible(true);
    } else if (data === 'online') {
      setPlaceOnlineModalVisible(true);
    } else {
      setPlaceType(data);
      setPlaceSelectModalVisible(true);
    }
  };

  const addEventPlace = (place: EventCreationDataPlace) => {
    const previousEventIds = eventPlaces.map((p) => p.id);
    if (!previousEventIds.includes(place.id)) {
      setEventPlaces([...eventPlaces, place]);
    }
  };

  // Program
  const [isProgramAddModalVisible, setProgramAddModalVisible] = React.useState(false);
  const [startDateShow, setStartDateShow] = React.useState(false);
  const [startTimeShow, setStartTimeShow] = React.useState(false);
  const [startDate, setStartDate] = React.useState<moment.Moment | undefined>(
    moment(creationData.start),
  );

  const [eventProgram, setProgram] = React.useState<ProgramEntry[]>(creationData.program || []);

  const dismissStartDateModal = React.useCallback(() => {
    setStartDateShow(false);
  }, [setStartDateShow]);
  const dismissStartTimeModal = React.useCallback(() => {
    setStartTimeShow(false);
  }, [setStartTimeShow]);
  const showStartDateModal = () => {
    if (!moment(creationData.start).isSame(creationData.end, 'day')) {
      setStartDateShow(true);
    } else {
      changeStartDate({ date: moment(creationData.start).toDate() });
    }
  };
  const addProgram = (program: ProgramEntry) => {
    setProgram([...eventProgram, program]);
    setStartDate(moment(creationData.start));
  };

  const changeStartDate = React.useCallback(({ date }: { date?: Date }) => {
    setStartDateShow(false);
    setStartTimeShow(true);
    if (date) setStartDate(moment(date));
  }, []);

  const changeStartTime = ({ hours, minutes }: { hours: number; minutes: number }) => {
    if (startDate) {
      const newDate = moment(startDate)
        .hour(0)
        .minute(0)
        .add(hours, 'hours')
        .add(minutes, 'minutes');
      setStartTimeShow(false);
      setStartDate(newDate);
      setProgramAddModalVisible(true);
    }
  };

  // Submit
  const submit = () => {
    // TODO;
    updateEventCreationData({
      places: eventPlaces,
      program: eventProgram,
      contact: customContact,
      email: email || undefined,
      phone: phone || undefined,
    });
    next();
  };

  if (!account.loggedIn) {
    return (
      <View style={styles.container}>
        <View style={styles.centerIllustrationContainer}>
          <Text>Non autorisé</Text>
        </View>
      </View>
    );
  }

  return (
    <View style={styles.formContainer}>
      <View style={{ marginTop: 30 }}>
        <List.Item
          title="Lieux"
          description="Spéficiez l'adresse ou l'école dans laquelle se déroule l'évènement"
        />
        {eventPlaces.map((place) => (
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}
          >
            <View style={{ flex: 1 }}>
              <InlineCard
                icon={
                  place.type === 'school'
                    ? 'school'
                    : place.type === 'place'
                    ? 'map'
                    : place.type === 'online'
                    ? 'link'
                    : 'map-marker'
                }
                title={
                  place.type === 'standalone'
                    ? Format.address(place.address)
                    : place.type === 'online'
                    ? place.link.replace('http://', '').replace('https://', '').split(/[/?#]/)[0]
                    : place.tempName ?? 'Lieu inconnu'
                }
                onPress={() => {
                  setEventPlaces(eventPlaces.filter((s) => s !== place));
                }}
              />
            </View>
            <IconButton
              accessibilityLabel="Supprimer ce lieu"
              icon="delete"
              size={30}
              style={{ marginRight: 20 }}
              onPress={() => {
                setEventPlaces(eventPlaces.filter((s) => s !== place));
              }}
            />
          </View>
        ))}
      </View>
      <View style={styles.container}>
        <Button
          mode="outlined"
          uppercase={Platform.OS !== 'ios'}
          onPress={() => {
            setPlaceTypeModalVisible(true);
          }}
        >
          Ajouter
        </Button>
      </View>

      <View style={{ marginTop: 30 }}>
        <List.Item title="Contacts" description="Donnez vos réseaux sociaux, email, téléphone..." />
        {email ? (
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}
          >
            <View style={{ flex: 1 }}>
              <InlineCard key="email" icon="email" title="Email" subtitle={email} />
            </View>
            <IconButton
              accessibilityLabel="Supprimer l'email"
              icon="delete"
              size={30}
              style={{ marginRight: 20 }}
              onPress={() => {
                setEmail(null);
              }}
            />
          </View>
        ) : null}
        {phone ? (
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}
          >
            <View style={{ flex: 1 }}>
              <InlineCard key="phone" icon="phone" title="Téléphone" subtitle={phone} />
            </View>
            <IconButton
              accessibilityLabel="Supprimer le téléphone"
              icon="delete"
              size={30}
              style={{ marginRight: 20 }}
              onPress={() => {
                setPhone(null);
              }}
            />
          </View>
        ) : null}
        {customContact?.map((contact) => (
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}
          >
            <View style={{ flex: 1 }}>
              <InlineCard
                key={contact._id}
                icon="at"
                title={contact.value}
                subtitle={contact.key}
              />
            </View>
            <IconButton
              accessibilityLabel="Supprimer cet élément"
              icon="delete"
              size={30}
              style={{ marginRight: 20 }}
              onPress={() => {
                setCustomContact(customContact.filter((s) => s !== contact));
              }}
            />
          </View>
        ))}
      </View>
      <View style={styles.container}>
        <Button
          mode="outlined"
          uppercase={Platform.OS !== 'ios'}
          onPress={() => {
            setContactAddModalVisible(true);
          }}
        >
          Ajouter
        </Button>
      </View>

      <View style={{ marginTop: 30 }}>
        <List.Item
          title="Organisateurs"
          description="Ajoutez les utilisateurs Topic qui aident à organiser l'évènement"
        />
        {eventOrganizers?.map((user) => (
          <View
            key={user._id}
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}
          >
            <View style={{ flex: 1 }}>
              <InlineCard key={user._id} avatar={user.info.avatar} title={user.info.username} />
            </View>
            <IconButton
              accessibilityLabel="Supprimer l'organisateur"
              icon="delete"
              size={30}
              style={{ marginRight: 20 }}
              onPress={() => {
                setEventOrganizers(eventOrganizers.filter((s) => s !== user));
              }}
            />
          </View>
        ))}
      </View>
      <View style={styles.container}>
        <Button
          mode="outlined"
          uppercase={Platform.OS !== 'ios'}
          onPress={() => {
            setAddUserModalVisible(true);
          }}
        >
          Ajouter
        </Button>
      </View>

      <View style={{ marginTop: 30 }}>
        <List.Item title="Programme" description="Donnez l'emploi du temps de votre évènement" />
        {eventProgram?.map((program) => (
          <View
            key={program._id}
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}
          >
            <View style={{ flex: 1 }}>
              <InlineCard
                key={program._id}
                icon="timetable"
                title={program.title}
                subtitle={`${moment(program.duration?.start).format('ddd D H:mm')} à ${moment(
                  program.duration?.end,
                ).format('ddd D H:mm')}`}
              />
            </View>
            <IconButton
              accessibilityLabel="Supprimer cet élément"
              icon="delete"
              size={30}
              style={{ marginRight: 20 }}
              onPress={() => {
                setProgram(eventProgram.filter((s) => s !== program));
              }}
            />
          </View>
        ))}
      </View>
      <View style={styles.container}>
        <Button
          mode="outlined"
          uppercase={Platform.OS !== 'ios'}
          onPress={() => {
            setStartDate(moment(creationData.start));
            setProgramAddModalVisible(true);
          }}
        >
          Ajouter
        </Button>
      </View>
      <View style={{ height: 40 }} />

      <UserSelectModal
        visible={isAddUserModalVisible}
        setVisible={setAddUserModalVisible}
        next={(user) => {
          addEventOrganizer(user);
        }}
      />
      <ContactAddModal
        visible={isContactAddModalVisible}
        setVisible={setContactAddModalVisible}
        add={(contact) => {
          addCustomContact(contact as CustomContactType);
        }}
      />
      <DatePickerModal
        mode="single"
        visible={startDateShow}
        onDismiss={dismissStartDateModal}
        date={moment(startDate).toDate()}
        onConfirm={changeStartDate}
        saveLabel="Enregistrer"
        label="Choisissez une date"
        validRange={{
          startDate: creationData.start as Date,
          endDate: creationData.end as Date,
        }}
      />
      <TimePickerModal
        visible={startTimeShow}
        onDismiss={dismissStartTimeModal}
        onConfirm={changeStartTime}
        label="Choisissez l'heure de début"
        cancelLabel="Annuler"
        confirmLabel="Enregistrer"
      />
      <ProgramAddModal
        visible={isProgramAddModalVisible}
        setVisible={setProgramAddModalVisible}
        date={moment(startDate).toDate()}
        resetDate={() => setStartDate(undefined)}
        setDate={() => showStartDateModal()}
        add={(program) => {
          addProgram(program);
        }}
      />
      <PlaceTypeModal
        visible={isPlaceTypeModalVisible}
        setVisible={setPlaceTypeModalVisible}
        next={toSelectedType}
      />
      <PlaceSelectModal
        visible={isPlaceSelectModalVisible}
        setVisible={setPlaceSelectModalVisible}
        type={placeType}
        add={addEventPlace}
      />
      <PlaceAddressModal
        visible={isPlaceAddressModalVisible}
        setVisible={setPlaceAddressModalVisible}
        add={addEventPlace}
      />
      <PlaceOnlineModal
        visible={isPlaceOnlineModalVisible}
        setVisible={setPlaceOnlineModalVisible}
        add={addEventPlace}
      />
      <View style={{ height: 20 }} />
      <View style={styles.buttonContainer}>
        <Button
          mode={Platform.OS !== 'ios' ? 'outlined' : 'text'}
          uppercase={Platform.OS !== 'ios'}
          onPress={() => prev()}
          style={{ flex: 1, marginRight: 5 }}
        >
          Retour
        </Button>
        <Button
          mode={Platform.OS !== 'ios' ? 'contained' : 'outlined'}
          uppercase={Platform.OS !== 'ios'}
          onPress={() => submit()}
          style={{ flex: 1, marginLeft: 5 }}
        >
          Suivant
        </Button>
      </View>
    </View>
  );
};

const mapStateToProps = (state: State) => {
  const { eventData, account } = state;
  return { creationData: eventData.creationData, account };
};

export default connect(mapStateToProps)(EventAddPageDetails);
