import { Formik } from 'formik';
import moment from 'moment';
import React, { createRef } from 'react';
import { View, Platform, TextInput as RNTextInput, Image } from 'react-native';
import { Button, ProgressBar, Card, Text, useTheme, List, HelperText } from 'react-native-paper';
import { DatePickerModal, TimePickerModal } from 'react-native-paper-dates';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';
import * as Yup from 'yup';

import { StepperViewPageProps, ErrorMessage, FormTextInput, FileUpload } from '@components';
import { updateEventCreationData } from '@redux/actions/contentData/events';
import { State, EventCreationData, UploadRequestState, Account } from '@ts/types';
import { getImageUrl, checkPermission, Permissions } from '@utils';

import getStyles from '../styles';

type EventAddPageMetaProps = StepperViewPageProps & {
  creationData: EventCreationData;
  state: UploadRequestState;
  account: Account;
};

const EventAddPageMeta: React.FC<EventAddPageMetaProps> = ({
  next,
  prev,
  creationData,
  state,
  account,
}) => {
  const titleInput = createRef<RNTextInput>();
  const summaryInput = createRef<RNTextInput>();

  const theme = useTheme();
  const { colors } = theme;
  const styles = getStyles(theme);

  const [showError, setError] = React.useState(false);

  const [startDate, setStartDate] = React.useState<moment.Moment | undefined>(
    creationData.start ? moment(creationData.start) : undefined,
  );
  const [endDate, setEndDate] = React.useState<moment.Moment | undefined>(
    creationData.end ? moment(creationData.end) : undefined,
  );

  const [startDateShow, setStartDateShow] = React.useState(false);
  const [endDateShow, setEndDateShow] = React.useState(false);
  const [startTimeShow, setStartTimeShow] = React.useState(false);
  const [endTimeShow, setEndTimeShow] = React.useState(false);

  const changeStartDate = ({ date }: { date: Date | undefined }) => {
    if (date) {
      setStartDateShow(false);
      setStartTimeShow(true);
      setStartDate(moment(date));
      // if (newDate.isSameOrAfter(endDate)) {
      //   setEndDate(newDate);
      // }
    }
    checkErrors();
  };

  const changeStartTime = ({ hours, minutes }: { hours: number; minutes: number }) => {
    if (startDate) {
      const newDate = moment(startDate).add(hours, 'hours').add(minutes, 'minutes');
      setStartTimeShow(false);
      setStartDate(newDate);
      // if (newDate.isSameOrAfter(endDate)) {
      //   setEndDate(newDate);
      // }
    }
    checkErrors();
  };

  const changeEndDate = ({ date }: { date: Date | undefined }) => {
    if (date) {
      setEndDateShow(false);
      setEndTimeShow(true);
      setEndDate(moment(date));
    }
    checkErrors();
  };

  const changeEndTime = ({ hours, minutes }: { hours: number; minutes: number }) => {
    if (endDate) {
      const newDate = moment(endDate).add(hours, 'hours').add(minutes, 'minutes');
      setEndTimeShow(false);
      setEndDate(newDate);
    }
    checkErrors();
  };

  const checkErrors = () => {
    if (startDate && endDate) {
      setError(false);
      if (moment(startDate).add(1, 'hour').isBefore(endDate)) {
        setError(false);
      }
    }
  };

  if (!account.loggedIn) return null;

  const MetaSchema = Yup.object().shape({
    title: Yup.string()
      .min(10, 'Le titre doit contenir au moins 10 caractères')
      .max(100, 'Le titre doit contenir moins de 100 caractères')
      .required('Titre requis'),
    summary: Yup.string().max(500, 'Le résumé doit contenir moins de 500 caractères'),
    file: Yup.mixed(),
  });

  return (
    <View style={styles.formContainer}>
      <Formik
        initialValues={{
          title: creationData.title || '',
          summary: creationData.summary || '',
          file: creationData.image?.image || null,
        }}
        validationSchema={MetaSchema}
        onSubmit={({ title, summary, file }) => {
          if (startDate && endDate && moment(startDate).add(1, 'hour').isBefore(endDate)) {
            updateEventCreationData({
              start: startDate.toDate(),
              end: endDate.toDate(),
              title,
              summary,
              image: { image: file, thumbnails: { small: false, medium: true, large: true } },
            });
            next();
          } else {
            setError(true);
          }
        }}
      >
        {({ handleChange, handleBlur, handleSubmit, values, errors, touched, setFieldValue }) => (
          <View>
            <FormTextInput
              ref={titleInput}
              label="Titre"
              value={values.title}
              touched={touched.title}
              error={errors.title}
              onChangeText={handleChange('title')}
              onBlur={handleBlur('title')}
              onSubmitEditing={() => summaryInput.current?.focus()}
              style={styles.textInput}
              autoFocus
            />
            <FormTextInput
              ref={summaryInput}
              label="Résumé"
              placeholder="Laissez vide pour sélectionner les premières lignes de la description"
              multiline
              numberOfLines={4}
              value={values.summary}
              touched={touched.summary}
              error={errors.summary}
              onChangeText={handleChange('summary')}
              onBlur={handleBlur('summary')}
              onSubmitEditing={() => summaryInput.current?.focus()}
              style={styles.textInput}
            />
            <FileUpload
              file={values.file}
              setFile={(file) => setFieldValue('file', file)}
              group={creationData.group || ''}
            />
            <View style={{ marginBottom: 40 }}>
              <List.Subheader> Début de l&apos;évènement </List.Subheader>
              <View style={{ marginHorizontal: 10, marginBottom: 10 }}>
                <Button
                  mode={Platform.OS !== 'ios' ? 'outlined' : 'text'}
                  onPress={() => setStartDateShow(true)}
                  uppercase={false}
                >
                  {startDate ? startDate.format('LLL') : 'Appuyez pour sélectionner'}
                </Button>
                <DatePickerModal
                  mode="single"
                  visible={startDateShow}
                  onDismiss={() => setStartDateShow(false)}
                  date={moment(startDate).toDate()}
                  onConfirm={changeStartDate}
                  saveLabel="Suivant"
                  label="Choisissez la date de début"
                />
                <TimePickerModal
                  visible={startTimeShow}
                  onDismiss={() => setStartTimeShow(false)}
                  onConfirm={changeStartTime}
                  label="Choisissez l'heure de début"
                  cancelLabel="Annuler"
                  confirmLabel="Enregistrer"
                />
              </View>
              <List.Subheader> Fin de l&apos;évènement </List.Subheader>
              <View style={{ marginHorizontal: 10 }}>
                <Button
                  mode={Platform.OS !== 'ios' ? 'outlined' : 'text'}
                  uppercase={false}
                  onPress={() => setEndDateShow(true)}
                >
                  {endDate ? endDate.format('LLL') : 'Appuyez pour sélectionner'}
                </Button>
                <DatePickerModal
                  mode="single"
                  visible={endDateShow}
                  onDismiss={() => setEndDateShow(false)}
                  date={moment(endDate).toDate()}
                  onConfirm={changeEndDate}
                  saveLabel="Suivant"
                  label="Choisissez la date de fin"
                />
                <TimePickerModal
                  visible={endTimeShow}
                  onDismiss={() => setEndTimeShow(false)}
                  onConfirm={changeEndTime}
                  label="Choisissez l'heure de fin"
                  cancelLabel="Annuler"
                  confirmLabel="Enregistrer"
                />
              </View>
              <HelperText type="error" visible={showError} style={{ marginVertical: -10 }}>
                Votre évènement doit durer une heure au minimum
              </HelperText>
            </View>
            <View style={styles.buttonContainer}>
              <Button
                mode={Platform.OS !== 'ios' ? 'outlined' : 'text'}
                uppercase={Platform.OS !== 'ios'}
                onPress={() => prev()}
                style={{ flex: 1, marginRight: 5 }}
              >
                Retour
              </Button>
              <Button
                mode={Platform.OS !== 'ios' ? 'contained' : 'outlined'}
                uppercase={Platform.OS !== 'ios'}
                onPress={handleSubmit}
                disabled={state.process.loading || state.upload.loading || state.process.loading}
                style={{ flex: 1, marginLeft: 5 }}
              >
                Suivant
              </Button>
            </View>
          </View>
        )}
      </Formik>
    </View>
  );
};

const mapStateToProps = (state: State) => {
  const { upload, eventData, account } = state;
  return { state: upload.state, creationData: eventData.creationData, account };
};

export default connect(mapStateToProps)(EventAddPageMeta);
