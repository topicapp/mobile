import React from 'react';
import { View, Platform } from 'react-native';
import {
  Button,
  HelperText,
  List,
  Text,
  Checkbox,
  Divider,
  ProgressBar,
  useTheme,
} from 'react-native-paper';
import { connect } from 'react-redux';

import { StepperViewPageProps, ErrorMessage } from '@components';
import { fetchMultiDepartment } from '@redux/actions/api/departments';
import { fetchMultiSchool } from '@redux/actions/api/schools';
import { updateEventCreationData } from '@redux/actions/contentData/events';
import {
  Account,
  State,
  EventCreationData,
  Department,
  School,
  ReduxLocation,
  RequestState,
  GroupRolePermission,
  GroupRole,
} from '@ts/types';
import { checkPermission, trackEvent, Permissions } from '@utils';

import { CheckboxListItem } from '../../components/ListItems';
import getStyles from '../styles';

type EventAddPageLocationProps = StepperViewPageProps & {
  account: Account;
  creationData: EventCreationData;
  navigation: any;
  schoolItems: School[];
  departmentItems: Department[];
  locationStates: {
    schools: {
      info: RequestState;
    };
    departments: {
      info: RequestState;
    };
  };
};

const getListItemCheckbox = (props: React.ComponentProps<typeof Checkbox>) => {
  return {
    left:
      Platform.OS !== 'ios'
        ? () => (
            <View style={{ justifyContent: 'center' }}>
              <Checkbox {...props} />
            </View>
          )
        : undefined,
    right: Platform.OS === 'ios' ? () => <Checkbox {...props} /> : undefined,
  };
};

const EventAddPageLocation: React.FC<EventAddPageLocationProps> = ({
  prev,
  next,
  account,
  creationData,
  navigation,
  schoolItems,
  departmentItems,
  locationStates,
}) => {
  const [schools, setSchools] = React.useState<string[]>(creationData.location?.schools || []);
  const [departments, setDepartments] = React.useState<string[]>(
    creationData.location?.departments || [],
  );
  const [global, setGlobal] = React.useState(creationData.location?.global || false);
  const [showError, setError] = React.useState(false);

  const theme = useTheme();
  const { colors } = theme;
  const styles = getStyles(theme);

  const submit = () => {
    if (schools.length !== 0 || departments.length !== 0 || global) {
      trackEvent('eventadd:page-meta');
      updateEventCreationData({ location: { schools, departments, global } });
      next();
    } else {
      setError(true);
    }
  };

  const selectedGroup = account.groups?.find((g) => g._id === creationData.group);
  const selectedGroupLocation =
    selectedGroup &&
    selectedGroup.roles
      ?.find((r: GroupRole) => r._id === selectedGroup.membership.role)
      ?.permissions.find((p: GroupRolePermission) => p.permission === Permissions.EVENT_ADD)?.scope;

  const userHasModifyEverywhere = checkPermission(account, {
    permission: Permissions.EVENT_MODIFY,
    scope: { everywhere: true },
  });

  const toggle = (i: { _id: string }, func: Function, data: string[]) => {
    if (data.includes(i._id)) {
      func(data.filter((j) => j !== i._id));
    } else {
      setError(false);
      func([...data, i._id]);
    }
  };

  React.useEffect(() => {
    if (selectedGroupLocation?.schools) {
      fetchMultiSchool(selectedGroupLocation.schools || []);
      fetchMultiDepartment(selectedGroupLocation.departments || []);
    }
    fetchMultiSchool(schools);
    fetchMultiDepartment(departments);
  }, [null]);

  React.useEffect(() => {
    if (!creationData.editing) {
      if (selectedGroupLocation?.schools) setSchools(selectedGroupLocation.schools);
      if (selectedGroupLocation?.departments) setDepartments(selectedGroupLocation.departments);
      if (selectedGroupLocation?.global) setGlobal(selectedGroupLocation.global);
    }
  }, [selectedGroupLocation]);

  return (
    <View style={styles.formContainer}>
      <View style={styles.listContainer}>
        {selectedGroupLocation?.schools?.map((sId) => {
          const s = schoolItems.find((t) => t._id === sId);
          if (!s) return null;
          return (
            <CheckboxListItem
              key={s._id}
              title={s.name}
              description={`École · ${s.address?.shortName || s.address?.address?.city}`}
              status={schools.includes(s._id) ? 'checked' : 'unchecked'}
              onPress={() => toggle(s, setSchools, schools)}
            />
          );
        })}
        {selectedGroupLocation?.departments?.map((dId) => {
          const d = departmentItems.find((t) => t._id === dId);
          if (!d) return null;
          return (
            <CheckboxListItem
              key={d._id}
              title={d.name}
              description={`Département ${d.code}`}
              status={departments.includes(d._id) ? 'checked' : 'unchecked'}
              onPress={() => toggle(d, setDepartments, departments)}
            />
          );
        })}
        {(selectedGroupLocation?.global ||
          selectedGroupLocation?.everywhere ||
          userHasModifyEverywhere) && (
          <List.Item
            title="France entière"
            description="Visible pour tous les utilisateurs"
            {...getListItemCheckbox({
              status: global ? 'checked' : 'unchecked',
              color: colors.primary,
              onPress: () => setGlobal(!global),
            })}
            onPress={() => setGlobal(!global)}
          />
        )}
        {selectedGroupLocation?.everywhere || userHasModifyEverywhere ? (
          <View>
            <Divider style={{ marginTop: 20 }} />
            {(locationStates.schools.info.loading || locationStates.departments.info.loading) && (
              <ProgressBar indeterminate />
            )}
            {(locationStates.schools.info.error || locationStates.departments.info.error) && (
              <ErrorMessage
                error={[locationStates.schools.info.error, locationStates.departments.info.error]}
                strings={{
                  what: 'la recherche des localisations',
                  contentSingular: 'La liste de localisations',
                  contentPlural: 'Les localisations',
                }}
                type="axios"
                retry={() => {
                  fetchMultiSchool([...schools, ...(selectedGroupLocation?.schools || [])]);
                  fetchMultiDepartment([
                    ...departments,
                    ...(selectedGroupLocation?.departments || []),
                  ]);
                }}
              />
            )}
            <List.Item
              title="Écoles"
              description={
                schools?.length
                  ? schools
                      .map(
                        (s) =>
                          schoolItems?.find((t) => t._id === s)?.displayName ||
                          schoolItems?.find((t) => t._id === s)?.name,
                      )
                      .join(', ')
                  : undefined
              }
              style={{ marginBottom: -20 }}
              right={() => <List.Icon icon="chevron-right" />}
              onPress={() =>
                navigation.push('Root', {
                  screen: 'Main',
                  params: {
                    screen: 'More',
                    params: {
                      screen: 'Location',
                      params: {
                        type: 'schools',
                        subtitle: 'Créer un évènement',
                        initialData: { schools, departments, global },
                        callback: ({ schools: newSchools }: ReduxLocation) => {
                          fetchMultiSchool(newSchools);
                          setSchools(newSchools);
                        },
                      },
                    },
                  },
                })
              }
            />
            <List.Item
              title="Départements"
              description={
                departments
                  ?.map((d) =>
                    departmentItems
                      .filter((e) => e.type === 'departement')
                      ?.find((e) => e._id === d),
                  )
                  .filter((d) => d)?.length
                  ? departments
                      .map(
                        (d) =>
                          departmentItems
                            .filter((e) => e.type === 'departement')
                            ?.find((e) => e._id === d)?.displayName ||
                          departmentItems
                            .filter((e) => e.type === 'departement')
                            ?.find((e) => e._id === d)?.name,
                      )
                      .filter((d) => d)
                      .join(', ')
                  : undefined
              }
              style={{ marginBottom: -20 }}
              onPress={() =>
                navigation.push('Root', {
                  screen: 'Main',
                  params: {
                    screen: 'More',
                    params: {
                      screen: 'Location',
                      params: {
                        type: 'departements',
                        subtitle: 'Créer un évènement',
                        initialData: { schools, departments, global },
                        callback: ({ departments: newDepartments }: ReduxLocation) => {
                          fetchMultiDepartment(newDepartments);
                          setDepartments(newDepartments);
                        },
                      },
                    },
                  },
                })
              }
              right={() => <List.Icon icon="chevron-right" />}
            />
            <List.Item
              title="Régions"
              description={
                departments
                  ?.map((d) =>
                    departmentItems.filter((e) => e.type === 'region')?.find((e) => e._id === d),
                  )
                  .filter((d) => d)?.length
                  ? departments
                      .map(
                        (d) =>
                          departmentItems
                            .filter((e) => e.type === 'region')
                            ?.find((e) => e._id === d)?.displayName ||
                          departmentItems
                            .filter((e) => e.type === 'region')
                            ?.find((e) => e._id === d)?.name,
                      )
                      .filter((d) => d)
                      .join(', ')
                  : undefined
              }
              onPress={() =>
                navigation.push('Root', {
                  screen: 'Main',
                  params: {
                    screen: 'More',
                    params: {
                      screen: 'Location',
                      params: {
                        type: 'regions',
                        subtitle: 'Créer un évènement',
                        initialData: { schools, departments, global },
                        callback: ({ departments: newDepartments }: ReduxLocation) => {
                          fetchMultiDepartment(newDepartments);
                          setDepartments(newDepartments);
                        },
                      },
                    },
                  },
                })
              }
              right={() => <List.Icon icon="chevron-right" />}
            />
          </View>
        ) : (
          <Text>Vous pouvez publier seulement dans ces localisations</Text>
        )}
        <HelperText visible={showError} type="error">
          Vous devez sélectionner au moins une localisation
        </HelperText>
      </View>
      <View style={styles.buttonContainer}>
        {!creationData.editing && (
          <Button
            mode={Platform.OS !== 'ios' ? 'outlined' : 'text'}
            uppercase={Platform.OS !== 'ios'}
            onPress={() => prev()}
            style={{ flex: 1, marginRight: 5 }}
          >
            Retour
          </Button>
        )}
        <Button
          mode={Platform.OS !== 'ios' ? 'contained' : 'outlined'}
          uppercase={Platform.OS !== 'ios'}
          onPress={submit}
          style={{ flex: 1, marginLeft: 5 }}
        >
          Suivant
        </Button>
      </View>
    </View>
  );
};

const mapStateToProps = (state: State) => {
  const { account, eventData, schools, departments } = state;
  return {
    account,
    creationData: eventData.creationData,
    schoolItems: schools.items,
    departmentItems: departments.items,
    locationStates: {
      schools: schools.state,
      departments: departments.state,
    },
  };
};

export default connect(mapStateToProps)(EventAddPageLocation);
