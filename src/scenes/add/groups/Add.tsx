import React from 'react';
import { View, ScrollView, KeyboardAvoidingView, Platform } from 'react-native';
import { Card, Text, useTheme } from 'react-native-paper';
import { SafeAreaView } from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';

import {
  TranslucentStatusBar,
  StepperView,
  PlatformBackButton,
  HelpModal,
  VerificationBanner,
} from '@components';
import {
  State,
  ArticleRequestState,
  ArticleCreationData,
  GroupTemplate,
  GroupRequestState,
  Account,
} from '@ts/types';

import type { GroupAddScreenNavigationProp } from '.';
import GroupAddPageLocation from './components/AddLocation';
import GroupAddPageMeta from './components/AddMeta';
import GroupAddPageProof from './components/AddProof';
import GroupAddPageReview from './components/AddReview';
import GroupAddPageTemplate from './components/AddTemplate';
import getStyles from './styles';

type Props = {
  navigation: GroupAddScreenNavigationProp<'Add'>;
  reqState: ArticleRequestState;
  creationData?: ArticleCreationData;
  templates: GroupTemplate[];
  groupState: GroupRequestState;
  account: Account;
};

const GroupAdd: React.FC<Props> = ({ navigation, templates, account, groupState }) => {
  const theme = useTheme();
  const { colors } = theme;
  const styles = getStyles(theme);

  const scrollViewRef = React.useRef<ScrollView>(null);

  const [helpModalVisible, setHelpModalVisible] = React.useState(false);

  return (
    <View style={styles.page}>
      <SafeAreaView style={{ flex: 1 }}>
        <TranslucentStatusBar />
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
          style={{ flex: 1 }}
        >
          <ScrollView keyboardShouldPersistTaps="handled" nestedScrollEnabled ref={scrollViewRef}>
            <PlatformBackButton onPress={navigation.goBack} />
            <View style={styles.centerIllustrationContainer}>
              <Text style={styles.title}>Créer un groupe</Text>
            </View>
            {account.accountInfo?.user?.verification?.verified ? (
              <StepperView
                onChange={() => scrollViewRef.current?.scrollTo({ x: 0, y: 0, animated: true })}
                pages={[
                  {
                    key: 'group',
                    icon: 'account-group',
                    title: 'Type',
                    component: (props) => (
                      <GroupAddPageTemplate templates={templates} state={groupState} {...props} />
                    ),
                  },
                  {
                    key: 'location',
                    icon: 'map-marker',
                    title: 'Localisation',
                    component: (props) => (
                      <GroupAddPageLocation navigation={navigation} {...props} />
                    ),
                  },
                  {
                    key: 'meta',
                    icon: 'information',
                    title: 'Info',
                    component: (props) => <GroupAddPageMeta {...props} />,
                  },
                  {
                    key: 'proof',
                    icon: 'script-text',
                    title: 'Légal',
                    component: (props) => <GroupAddPageProof navigation={navigation} {...props} />,
                  },
                  {
                    key: 'review',
                    icon: 'check-bold',
                    title: 'Vérif.',
                    component: (props) => <GroupAddPageReview navigation={navigation} {...props} />,
                  },
                ]}
              />
            ) : (
              <View>
                <View style={[styles.container, styles.formContainer, { marginTop: 20 }]}>
                  <Card
                    elevation={0}
                    style={{ borderColor: colors.primary, borderWidth: 1, borderRadius: 5 }}
                  >
                    <View style={[styles.container, { flexDirection: 'row' }]}>
                      <Icon
                        name="shield-key-outline"
                        style={{ alignSelf: 'center', marginRight: 10 }}
                        size={24}
                        color={colors.primary}
                      />
                      <Text style={{ color: colors.text, flex: 1, fontSize: 18 }}>
                        Vous devez vérifier votre adresse email avant de pouvoir créer un groupe
                      </Text>
                    </View>
                  </Card>
                </View>
                <View style={{ marginTop: 20 }}>
                  <VerificationBanner />
                </View>
              </View>
            )}
            <View style={styles.formContainer}>
              <View style={{ flex: 1, alignItems: 'center' }}>
                <Text style={styles.subtitle} onPress={() => setHelpModalVisible(true)}>
                  Besoin d&apos;aide ? <Text style={styles.link}>Cliquez ici</Text> ou envoyez un
                  email à contact@topicapp.fr
                </Text>
              </View>
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </SafeAreaView>
      <HelpModal visible={helpModalVisible} setVisible={setHelpModalVisible} type="groupe" />
    </View>
  );
};

const mapStateToProps = (state: State) => {
  const { groups, account } = state;
  return { groupState: groups.state, account, templates: groups.templates };
};

export default connect(mapStateToProps)(GroupAdd);
