import { RouteProp } from '@react-navigation/native';
import { Formik } from 'formik';
import jwtDecode from 'jwt-decode';
import React from 'react';
import { ActivityIndicator, Platform, ScrollView, View } from 'react-native';
import { Text, Button, useTheme, Divider } from 'react-native-paper';
import { connect } from 'react-redux';
import * as Yup from 'yup';
import zxcvbn from 'zxcvbn';

import {
  Illustration,
  ErrorMessage,
  StrengthMeter,
  FormTextInput,
  PageContainer,
  GroupCard,
  VerificationBanner,
} from '@components';
import { fetchGroup } from '@redux/actions/api/groups';
import { groupJoinLink } from '@redux/actions/apiActions/groups';
import { fetchAccount, fetchGroups } from '@redux/actions/data/account';
import {
  Account,
  State,
  LinkingRequestState,
  GroupRequestState,
  Group,
  GroupPreload,
  AccountRequestState,
} from '@ts/types';

import type { LinkingScreenNavigationProp, LinkingStackParams } from '.';
import getStyles from './styles';

type Props = {
  navigation: LinkingScreenNavigationProp<'GroupInvite'>;
  route: RouteProp<LinkingStackParams, 'GroupInvite'>;
  state: GroupRequestState;
  group: Group | GroupPreload | null;
  account: Account;
  accountState: AccountRequestState;
};

const Linking: React.FC<Props> = ({ navigation, route, state, group, account, accountState }) => {
  const theme = useTheme();
  const styles = getStyles(theme);
  const { colors } = theme;

  const { token } = route.params;

  const { type, groupId } = jwtDecode(token) as { type: string; groupId: string };

  React.useEffect(() => {
    fetchGroup(groupId);
  }, [groupId]);

  const submit = async () => {
    await groupJoinLink(groupId, token);
    await fetchAccount();
    await fetchGroups();
    navigation.replace('Root', {
      screen: 'Main',
      params: {
        screen: 'More',
        params: { screen: 'MyGroups' },
      },
    });
  };

  const isAlreadyMember = account.groups.some((g) => g._id === groupId);

  return (
    <PageContainer
      headerOptions={{
        hideBack: true,
        title: 'Topic',
        subtitle: 'Rejoindre un groupe',
      }}
    >
      {account.loggedIn ? (
        <View style={{ flex: 1, flexGrow: 1 }}>
          <ScrollView>
            {state.invite_join.error ? (
              <View>
                <ErrorMessage
                  type="axios"
                  strings={{
                    what: "l'ajout au groupe",
                    contentSingular: "L'ajout au groupe",
                  }}
                  error={state.invite_join.error}
                  retry={submit}
                />
              </View>
            ) : null}
            <View
              style={[
                styles.centerIllustrationContainer,
                styles.contentContainer,
                { marginTop: 40 },
              ]}
            >
              <Illustration name="group" height={200} width={200} />
              {state.info.loading ? (
                <ActivityIndicator size="large" color={colors.primary} />
              ) : (
                <Text style={[styles.title, { textAlign: 'center' }]}>
                  {isAlreadyMember
                    ? `Vous êtes déjà dans ${group?.name}`
                    : `Vous êtes invités à rejoindre ${group?.name}`}
                </Text>
              )}
            </View>
            <View style={styles.container}>
              {state.info.success && group ? (
                <GroupCard group={group as Group} navigate={() => {}} />
              ) : null}
            </View>
          </ScrollView>
          <View>
            {!account.accountInfo.user.verification?.verified && (
              <View>
                <VerificationBanner extra="Vous ne pourrez pas rejoindre de groupe tant que votre compte n'est pas vérifié" />
              </View>
            )}
            <Divider />
            <View style={styles.formContainer}>
              <View style={styles.buttonContainer}>
                <Button
                  loading={
                    state.invite_join.loading ||
                    accountState.fetchAccount.loading ||
                    accountState.fetchGroups.loading
                  }
                  disabled={
                    state.invite_join.loading ||
                    accountState.fetchAccount.loading ||
                    accountState.fetchGroups.loading ||
                    isAlreadyMember ||
                    !account.accountInfo.user.verification?.verified
                  }
                  mode={Platform.OS !== 'ios' ? 'contained' : 'outlined'}
                  uppercase={Platform.OS !== 'ios'}
                  onPress={() => submit()}
                  style={{ flex: 1 }}
                >
                  Rejoindre
                </Button>
              </View>
            </View>
          </View>
        </View>
      ) : (
        <View style={[styles.container, { marginTop: 30, flex: 1 }]}>
          <View style={[styles.centerIllustrationContainer, styles.contentContainer, { flex: 1 }]}>
            <Illustration name="group" height={200} width={200} />
          </View>
          <View style={{ marginVertical: 20 }}>
            <Text style={[styles.subtitle, { textAlign: 'center' }]}>
              Vous devez avoir un compte Topic pour rejoindre un groupe
            </Text>
          </View>
          <View style={styles.container}>
            <Button
              mode={Platform.OS !== 'ios' ? 'contained' : 'outlined'}
              uppercase={Platform.OS !== 'ios'}
              onPress={() =>
                navigation.navigate('Auth', { screen: 'Login', params: { goBack: true } })
              }
            >
              Se connecter
            </Button>
          </View>
          <View style={styles.container}>
            <Button
              mode={Platform.OS !== 'ios' ? 'contained' : 'outlined'}
              uppercase={Platform.OS !== 'ios'}
              onPress={() =>
                navigation.navigate('Auth', { screen: 'Create', params: { goBack: true } })
              }
            >
              Créer un compte
            </Button>
          </View>
        </View>
      )}
    </PageContainer>
  );
};

const mapStateToProps = (state: State) => {
  const { groups, account } = state;
  return { state: groups.state, group: groups.item, account, accountState: account.state };
};

export default connect(mapStateToProps)(Linking);
