import { useNavigation } from '@react-navigation/core';
import React from 'react';
import { View, Platform, TouchableOpacity, Text } from 'react-native';
import { List, Checkbox, useTheme, Title, Subheading, Button } from 'react-native-paper';

import getStyles from '@styles/global';
import { handleUrl, trackEvent } from '@utils';

type WelcomeDownloadProps = {
  showWebsite: boolean;
};

const WelcomeDownload: React.FC<WelcomeDownloadProps> = ({ showWebsite = false }) => {
  const theme = useTheme();
  const { colors } = theme;
  const styles = getStyles(theme);

  const navigation = useNavigation();

  const detectOS = () => {
    const userAgent = navigator.userAgent || navigator.vendor || '';

    // Windows Phone must come first because its UA also contains "Android"
    if (/windows phone/i.test(userAgent)) {
      return;
    }

    if (/android/i.test(userAgent)) {
      return 'android';
    }

    // iOS detection from: http://stackoverflow.com/a/9039885/177710
    if (/iPad|iPhone|iPod/.test(userAgent)) {
      return 'ios';
    }
  };

  return (
    <View style={styles.centerIllustrationContainer}>
      <Title style={[styles.title, { textAlign: 'center', fontSize: 24 }]}>
        Téléchargez l&apos;application Topic
      </Title>
      <Subheading
        style={[styles.subtitle, { textAlign: 'center', color: colors.subtext, fontSize: 16 }]}
      >
        Retrouvez l&apos;actualité engagée et découvrez ce qui se passe autour de vous, directement
        sur votre téléphone
      </Subheading>
      <View style={{ marginTop: 20, flexDirection: 'row' }}>
        <Button
          mode={detectOS() === 'android' ? 'contained' : 'outlined'}
          onPress={() => {
            const params = new URLSearchParams(window.location.search) as {
              s?: string;
              c?: string;
              m?: string;
            };
            let parameters = '';
            if (params.s) {
              parameters += `&utm_source=${params.s}`;
            }
            if (params.c) {
              parameters += `&utm_campaign=${params.c}`;
            }
            if (params.m) {
              parameters += `&utm_medium=${params.m}`;
            }
            trackEvent('downloadpage:download-button', {
              props: {
                os: 'android',
                utm_source: params.s || 'None',
                utm_campaign: params.c || 'None',
                utm_medium: params.m || 'None',
              },
            });
            handleUrl(
              `https://play.google.com/store/apps/details?id=fr.topicapp.topic${parameters}`,
              {
                trusted: true,
              },
            );
          }}
          icon="android"
          style={{ marginRight: 10 }}
          uppercase={false}
        >
          Android
        </Button>
        <Button
          mode={detectOS() === 'ios' ? 'contained' : 'outlined'}
          onPress={() => {
            const params = new URLSearchParams(window.location.search) as {
              s?: string;
              c?: string;
              m?: string;
            };
            const parameters = `pt=122464955&ct=${params.s}.${params.m}.${params.c}&mt=8`;
            trackEvent('downloadpage:download-button', {
              props: {
                os: 'ios',
                utm_source: params.s || 'None',
                utm_campaign: params.c || 'None',
                utm_medium: params.m || 'None',
              },
            });
            handleUrl(`https://apps.apple.com/fr/app/topic/id1545178171?${parameters}`, {
              trusted: true,
            });
          }}
          icon="apple"
          style={{ marginLeft: 10 }}
          uppercase={false}
        >
          iOS
        </Button>
      </View>
      {showWebsite && (
        <View style={{ marginTop: 10 }}>
          <TouchableOpacity onPress={() => navigation.navigate('Welcome')}>
            <Text style={{ color: colors.subtext, fontSize: 16 }}>
              Ou <Text style={[styles.link, { color: colors.subtext }]}>utilisez le site web</Text>
            </Text>
          </TouchableOpacity>
        </View>
      )}
    </View>
  );
};

export default WelcomeDownload;
