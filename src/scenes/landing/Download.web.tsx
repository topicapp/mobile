import React from 'react';
import { View, ScrollView, Image, useWindowDimensions, TouchableOpacity } from 'react-native';
import { Divider, Subheading, Title, Button, Text, useTheme } from 'react-native-paper';

import { Illustration } from '@components';
import { updateDepartments } from '@redux/actions/api/departments';
import { handleUrl, trackEvent } from '@utils';

import type { LandingScreenNavigationProp } from '.';
import WelcomeAbout from './components/WelcomeAbout';
import WelcomeDownload from './components/WelcomeDownload';
import WelcomeSlides from './components/WelcomeSlides';
import getStyles from './styles';

type LandingWelcomeProps = {
  navigation: LandingScreenNavigationProp<'Welcome'>;
};

const LandingWelcome: React.FC<LandingWelcomeProps> = ({ navigation }) => {
  const theme = useTheme();
  const { colors } = theme;
  const styles = getStyles(theme);

  const homepage_illustration = require('@assets/images/bigillustrations/homepage.png');

  const { width } = useWindowDimensions();

  React.useEffect(() => {
    // Preload écoles & departments pour utiliser après dans SelectLocation
    updateDepartments('initial');
    trackEvent('firstopen');
  }, []);

  return (
    <View style={styles.page}>
      <ScrollView>
        <View style={{ height: 'calc(100vh - 110px)', width: '100%' }}>
          <View style={styles.centerIllustrationContainer}>
            <View style={styles.container}>
              <TouchableOpacity
                onPress={() => navigation.navigate('Landing', { screen: 'Welcome' })}
              >
                <Illustration name="topic-icon-text" height={50} />
              </TouchableOpacity>
            </View>
          </View>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
            }}
          >
            <View
              style={[
                styles.container,
                { flex: 1, alignSelf: width > 1000 ? 'center' : 'flex-start' },
              ]}
            >
              {width <= 1000 && (
                <View style={{ marginTop: 10, marginBottom: 20 }}>
                  <Image
                    resizeMode="contain"
                    style={{ height: 'calc(65vh - 250px)' }}
                    source={homepage_illustration}
                  />
                </View>
              )}
              <WelcomeDownload showWebsite />
            </View>
            {width > 1000 && <WelcomeSlides />}
          </View>
        </View>
        <WelcomeAbout showDownload={false} showSocial={true} />
      </ScrollView>
    </View>
  );
};

export default LandingWelcome;
