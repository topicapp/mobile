import React from 'react';
import { View, ScrollView, Text, useWindowDimensions } from 'react-native';
import { Divider, useTheme } from 'react-native-paper';

import { updateDepartments } from '@redux/actions/api/departments';
import { trackEvent } from '@utils';

import getStyles from './styles';

const LandingWelcome: React.FC = () => {
  const theme = useTheme();
  const { colors } = theme;
  const styles = getStyles(theme);

  const { width } = useWindowDimensions();

  React.useEffect(() => {}, []);

  return (
    <View style={styles.page}>
      <View style={styles.centerIllustrationContainer}>
        <Text style={{ textAlign: 'center' }}>Vous avez déjà téléchargé l&apos;application :)</Text>
      </View>
    </View>
  );
};

export default LandingWelcome;
